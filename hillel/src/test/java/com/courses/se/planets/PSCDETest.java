package com.courses.se.planets;

import com.courses.se.planets.model.*;
import org.junit.Test;

import java.util.ArrayList;

/** PSCDETest class. */
public class PSCDETest {

    private ArrayList<Planet> planets = new ArrayList<>();

    {
        planets.add(new Planet("Earth",
                new ArrayList<State>() {{
                    add(new State("EarthState",
                            new ArrayList<Company>() {{
                                add(new Company("EarthCompany", new ArrayList<Department>() {{
                                    add(new Department("DeathStar",
                                            new ArrayList<Employee>() {{
                                                add(new Employee("Martha", 18500));
                                                add(new Employee("Batman", 50000));
                                            }}));
                                    add(new Department("Department",
                                            new ArrayList<Employee>() {{
                                                add(new Employee("John", 15000));
                                                add(new Employee("Sasha", 20000));
                                            }}));
                                }}));
                            }}));
                }}));
        planets.add(new Planet("Mars",
                new ArrayList<State>() {{
                    add(new State("MarsState",
                            new ArrayList<Company>() {{
                                add(new Company(new ArrayList<Employee>() {{
                                    add(new Employee("Superman", 2000));
                                    add(new Employee("Lex Luthor", 40000));
                                }}, "MarsCompany"));
                            }}));
                }}));
    }

    @Test
    public void searchTest() {
        System.out.println("Best of the Best = > " + PlanetMethods.employeeSearch(planets));
    }


}
