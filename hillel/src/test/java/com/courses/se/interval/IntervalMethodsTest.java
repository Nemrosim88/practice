package com.courses.se.interval;

import com.courses.se.interval.model.Interval;
import org.junit.Test;

import java.util.HashSet;

import static com.courses.se.interval.IntervalMethods.mergeTwoIntervals;
import static org.junit.Assert.assertTrue;

/**
 * This class has methods for testing IntervalMethods.
 */
public class IntervalMethodsTest {


  @Test
  public void isIntervalsCrossingTest() {
    assertTrue(!IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 8, true), new Interval(12, true, 20, true)));
    assertTrue(!IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 10, true), new Interval(10, true, 20, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 10, false), new Interval(10, false, 20, true)));
    assertTrue(!IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 10, false), new Interval(10, true, 20, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 13, false), new Interval(12, false, 20, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, false, 13, true), new Interval(2, true, 8, false)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, false, 13, true), new Interval(-1, false, 8, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 13, true), new Interval(-1, true, 8, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 2, false), new Interval(-1, true, 2, false)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 2, true), new Interval(-1, true, 2, false)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 2, false), new Interval(-1, true, 2, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(2, false, -1, true), new Interval(2, false, -1, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(2, false, -1, false), new Interval(2, false, -1, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(2, false, -1, true), new Interval(2, false, -2, false)));
    assertTrue(!IntervalMethods.isIntervalsCrossing(new Interval(12, true, 20, true), new Interval(-1, true, 8, true)));
    assertTrue(!IntervalMethods.isIntervalsCrossing(new Interval(10, true, 20, true), new Interval(-1, true, 10, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(10, false, 20, true), new Interval(-1, true, 10, false)));
    assertTrue(!IntervalMethods.isIntervalsCrossing(new Interval(10, true, 20, true), new Interval(-1, true, 10, false)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(12, false, 20, true), new Interval(-1, true, 13, false)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(2, true, 8, false), new Interval(-1, false, 13, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, true, 8, true), new Interval(-1, false, 13, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-1, false, 8, true), new Interval(-1, true, 13, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-20, true, -32, false), new Interval(-20, false, 13, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-20, false, -32, false), new Interval(-20, true, 13, true)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-20, false, -32, false), new Interval(-20, false, 13, false)));
    assertTrue(IntervalMethods.isIntervalsCrossing(new Interval(-20, true, 20, true), new Interval(-20, true, 20, true)));
  }

  @Test
  public void intervalMergeTest() {
    assertTrue((new Interval(-1, true, 5, false)).equals(mergeTwoIntervals(new Interval(-1, true, 3, false), new Interval(3, false, 5, false))));
    assertTrue((new Interval(-1, true, 5, false)).equals(mergeTwoIntervals(new Interval(-1, true, 4, true), new Interval(3, false, 5, false))));
    assertTrue((new Interval(-1, true, 5, false)).equals(mergeTwoIntervals(new Interval(-1, true, 5, true), new Interval(3, false, 5, false))));
    assertTrue((new Interval(-1, true, 7, false)).equals(mergeTwoIntervals(new Interval(3, false, 5, false), new Interval(-1, true, 7, false))));
    assertTrue((new Interval(-1, true, 5, true)).equals(mergeTwoIntervals(new Interval(-1, true, 5, true), new Interval(-1, true, 2, false))));
    assertTrue((new Interval(-1, false, 2, true)).equals(mergeTwoIntervals(new Interval(-1, false, 2, true), new Interval(-1, true, 2, true))));
    assertTrue((new Interval(-1, false, 2, false)).equals(mergeTwoIntervals(new Interval(-1, true, 2, false), new Interval(-1, false, 2, true))));
    assertTrue((new Interval(-1, false, 2, false)).equals(mergeTwoIntervals(new Interval(-1, true, 2, false), new Interval(-1, false, 2, false))));
    assertTrue((new Interval(-1, true, 5, true)).equals(mergeTwoIntervals(new Interval(-1, true, 5, true), new Interval(-1, true, 0, true))));
    assertTrue((new Interval(-1, true, 6, true)).equals(mergeTwoIntervals(new Interval(-1, true, 5, true), new Interval(-1, true, 6, true))));
    assertTrue((new Interval(-3, true, 10, true)).equals(mergeTwoIntervals(new Interval(-3, true, 10, true), new Interval(-1, true, 6, true))));
    assertTrue((new Interval(-3, false, 10, false)).equals(mergeTwoIntervals(new Interval(-3, false, 10, false), new Interval(-1, true, 6, true))));
    assertTrue((new Interval(-1, true, 10, false)).equals(mergeTwoIntervals(new Interval(6, false, 10, false), new Interval(-1, true, 10, true))));
    assertTrue((new Interval(-1, true, 10, false)).equals(mergeTwoIntervals(new Interval(6, false, 10, true), new Interval(-1, true, 10, false))));
    assertTrue((new Interval(-1, true, 10, false)).equals(mergeTwoIntervals(new Interval(6, false, 10, false), new Interval(-1, true, 10, false))));
    assertTrue((new Interval(-1, true, 12, false)).equals(mergeTwoIntervals(new Interval(6, false, 12, false), new Interval(-1, true, 10, false))));
    assertTrue((new Interval(-1, true, 12, true)).equals(mergeTwoIntervals(new Interval(6, false, 12, true), new Interval(-1, true, 10, false))));
    assertTrue((new Interval(6, false, 12, true)).equals(mergeTwoIntervals(new Interval(6, false, 12, true), new Interval(6, true, 10, false))));
    assertTrue((new Interval(6, true, 12, false)).equals(mergeTwoIntervals(new Interval(6, true, 12, false), new Interval(6, true, 10, false))));
    assertTrue((new Interval(6, false, 12, true)).equals(mergeTwoIntervals(new Interval(6, false, 12, true), new Interval(6, true, 12, true))));
    assertTrue((new Interval(6, true, 12, false)).equals(mergeTwoIntervals(new Interval(6, true, 12, false), new Interval(6, true, 12, false))));
    assertTrue((new Interval(6, true, 12, false)).equals(mergeTwoIntervals(new Interval(6, true, 12, false), new Interval(6, true, 12, true))));
    assertTrue(null == mergeTwoIntervals(new Interval(-1, true, 5, true), new Interval(6, true, 8, true)));
  }

  @Test
  public void getArrayOfRandomIntervalsTest() {
    Interval[] arrayOfIntervals = IntervalMethods.getArrayOfRandomIntervals(30);
    assertTrue(arrayOfIntervals.length == 30);

    HashSet<Boolean> setOfIsOpenLeft = new HashSet<>();
    HashSet<Boolean> setOfIsOpenRight = new HashSet<>();
    HashSet<Integer> setOfVariablesA = new HashSet<>();
    HashSet<Integer> setOfVariablesB = new HashSet<>();

    for (Interval interval : arrayOfIntervals) {
      setOfIsOpenLeft.add(interval.getOpenLeft());
      setOfIsOpenRight.add(interval.getOpenRight());
      setOfVariablesA.add(interval.getVariableA());
      setOfVariablesB.add(interval.getVariableB());
    }

    assertTrue(setOfIsOpenLeft.size() == 2);
    assertTrue(setOfIsOpenRight.size() == 2);
    assertTrue(setOfVariablesA.size() > 25);
    assertTrue(setOfVariablesB.size() > 25);
  }

  @Test
  public void getDistanceBetweenTheEndsTest() {
    Interval[] arrayOfIntervals = {
            new Interval(-345, true, 123, true),
            new Interval(-5, false, 8, true),
            new Interval(-35, true, 76, false),
            new Interval(345, false, 12, true),
            new Interval(-5, true, 87645, false),
            new Interval(-5431, true, 456, false),
    };
    Interval actualResult = IntervalMethods.getDistanceBetweenTheEnds(arrayOfIntervals);
    assertTrue(actualResult.getVariableA() == -5431
            && actualResult.getOpenLeft()
            && actualResult.getVariableB() == 87645
            && !actualResult.getOpenRight());

  }

}
