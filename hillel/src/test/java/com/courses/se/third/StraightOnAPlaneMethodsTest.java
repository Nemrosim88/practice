package com.courses.se.third;

import com.courses.se.third.model.StraightOnAPlane;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Class created for testing of StraightOnAPlaneMethods class.
 * http://graph.reshish.ru/ - this site was used for checks.
 */
public class StraightOnAPlaneMethodsTest {

  @Test
  public void getCrossingPointsTest() {
    assertArrayEquals(
            new double[][]{{-1.0, 0.0}, {0.0, 1.0}},
            StraightOnAPlaneMethods.getCrossingPoints(new StraightOnAPlane(2, 2, 2)));
    assertArrayEquals(
            new double[][]{{1.67, 0.0}, {0.0, -0.63}},
            StraightOnAPlaneMethods.getCrossingPoints(new StraightOnAPlane(-3, -8, 5)));
    assertArrayEquals(
            new double[][]{{0.6, 0.0}, {0.0, 0.64}},
            StraightOnAPlaneMethods.getCrossingPoints(new StraightOnAPlane(-15, 14, 9)));
  }

  @Test
  public void getCrossingPointsTestTwo() {
    assertArrayEquals(new double[]{-0.17, 0.83},
            StraightOnAPlaneMethods.getCrossingPoints(
                    new StraightOnAPlane(2, 2, 2),
                    new StraightOnAPlane(-15, 14, 9)), 0.001);

    assertArrayEquals(new double[]{-2.60, -1.60},
            StraightOnAPlaneMethods.getCrossingPoints(
                    new StraightOnAPlane(2, 2, 2),
                    new StraightOnAPlane(-3, -8, 5)), 0.001);
  }

  @Test
  public void arrayOfStraightsTest() {
    ArrayList<StraightOnAPlane> parallelStraights = StraightOnAPlaneMethods.getArrayOfParallelStraights();

    StraightOnAPlane firstStraight = null;
    StraightOnAPlane secondStraight = null;

    for (int i = 2; i < parallelStraights.size(); i++) {
      if (i % 2 == 0) {
        firstStraight = parallelStraights.get(i);
        secondStraight = parallelStraights.get(i + 1);
      }

      assertArrayEquals(StraightOnAPlaneMethods.getCrossingPoints(firstStraight, secondStraight),
              new double[0], 0.001);
    }
  }
}