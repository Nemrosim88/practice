package com.courses.se.recursion;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * RecursionTest class for testing Recursion class with searchList method.
 */
public class RecursionTest {

    @Test
    public void searchListMethodTest() {
        int[] testArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        // number "4" is in the testArray[3];
        assertTrue(new Recursion().searchList(testArray, 5, 4));

        int[] secondTestArray = {87, 45, 23, 1, 123, 321};
        // number "321" is in the secondTestArray[5];
        assertTrue(new Recursion().searchList(secondTestArray, 5, 321));

        int[] thirdTestArray = {123, 234, 345, 456, 567, 678, 789, 890};
        // thirdTestArray does not contain number 5;
        assertTrue(!new Recursion().searchList(thirdTestArray, 5, 5));
    }

    @Test
    public void searchListMethodForNull() {
        int[] testArray = null;
        assertTrue(!new Recursion().searchList(testArray, 5, 4));
    }

    @Test
    public void searchListMethodForNegativeValue() {
        int[] testArray = {123, -234, 345, 456, 567, 678, 789, 890};
        assertTrue(new Recursion().searchList(testArray, 5, -234));
    }
}
