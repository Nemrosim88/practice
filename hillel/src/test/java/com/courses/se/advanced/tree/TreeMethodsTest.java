package com.courses.se.advanced.tree;

import com.courses.se.advanced.tree.model.Leaf;
import com.courses.se.advanced.tree.model.Tree;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** TreeMethodsTest created for TreeMethods class test. */
public class TreeMethodsTest {

    /** Created array of array of leaves. Three branches of leaves.*/
    private final Leaf[][] leafsArray = {
            {new Leaf(false, Leaf.Color.ORANGE), new Leaf(false, Leaf.Color.RED),},
            {new Leaf(false, Leaf.Color.ORANGE), new Leaf(false, Leaf.Color.RED),},
            {new Leaf(false, Leaf.Color.RED), new Leaf(false, Leaf.Color.ORANGE),
                    new Leaf(false, Leaf.Color.GREEN), new Leaf(false, Leaf.Color.ORANGE),
            },
    };

    /** Created new Tree with all new leaves and branches. */
    private Tree treeObject = new Tree(leafsArray);

    /** Created new TreeMethods for Tree class. */
    private TreeMethods treeMethods = new TreeMethods(treeObject);

    @Test
    public void toBlossomTest() {
        treeMethods.toBlossom();
        for (int i = 0; i < treeObject.getBranches().length; i++) {
            for (int j = 0; j < treeObject.getBranches()[i].getLeaves().length; j++) {
                assertTrue(!treeObject.getBranches()[i].getLeaves()[j].isSear());
                assertTrue(treeObject.getBranches()[i].getLeaves()[j].getColor() == Leaf.Color.GREEN);
            }
        }
    }

    @Test
    public void fallInTest() {
        treeMethods.fallIn();
        for (int i = 0; i < treeObject.getBranches().length; i++) {
            assertEquals(treeObject.getBranches()[i].getLeaves(), (new Leaf[0]));
        }
    }

    @Test
    public void frostedTest() {
        treeMethods.frosted();
        assertTrue(treeObject.isFrost());
    }

    @Test
    public void growYellowTest() {
        treeMethods.growYellow();
        for (int i = 0; i < treeObject.getBranches().length; i++) {
            for (int j = 0; j < treeObject.getBranches()[i].getLeaves().length; j++) {
                assertTrue(treeObject.getBranches()[i].getLeaves()[j].getColor() == Leaf.Color.YELLOW);
            }
        }
    }
}