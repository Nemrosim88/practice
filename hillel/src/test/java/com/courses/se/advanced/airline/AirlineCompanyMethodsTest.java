package com.courses.se.advanced.airline;

import com.courses.se.advanced.airline.model.*;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/** Class created for AirlineCompanyMethods test. */
public class AirlineCompanyMethodsTest {
    private static ArrayList<Airplane> airplaneList = new ArrayList<>();
    private AirlineCompanyMethods airlineCompanyMethods = new AirlineCompanyMethods(new AirlineCompany("Best airlines", airplaneList));

    static {
        // manufacture, model, SN, flightRange, fuelConsumption, maximumLiftingWeight, passengerCapacity
        airplaneList.add(new Turboprop("KING AIR", "350", 1, 2_850, 123, 6_849, 9));
        airplaneList.add(new Turboprop("KING AIR", "B200GT", 2, 2_126, 234, 5_670, 7));
        airplaneList.add(new Turboprop("KING AIR", "C90GTI", 3, 1_539, 345, 4_581, 5));
        airplaneList.add(new Turboprop("Piaggio", "P.180 Avanti II", 4, 2_592, 456, 5_239, 9));
        airplaneList.add(new Turboprop("SOCATA", "TBM 850", 5, 2815, 567, 3354, 6));

        airplaneList.add(new LightAirplane("Adam Aircraft", "А700", 6, 2_037, 678, 3_856, 6));
        airplaneList.add(new LightAirplane("Cessna", "Citation Mustang", 7, 2130, 789, 3921, 5));
        airplaneList.add(new LightAirplane("Eclipse Aviation", "Eclipse 500", 8, 2048, 890, 2719, 6));

        airplaneList.add(new SmallAirplane("Cessna", "Citation Bravo", 9, 3232, 1231, 6713, 7));
        airplaneList.add(new SmallAirplane("Cessna", "Citation Encore+", 10, 3262, 1232, 7544, 7));
        airplaneList.add(new SmallAirplane("Cessna", "CJ1+", 11, 2400, 1234, 4853, 5));

        airplaneList.add(new AverageAirplane("Cessna", "Citation Sovereign", 12, 5272, 2345, 13608, 12));
        airplaneList.add(new AverageAirplane("Cessna", "Citation XLS/XLS+", 13, 3440, 2345, 9163, 12));
        airplaneList.add(new AverageAirplane("Cessna", "CITATION X", 14, 5686, 2346, 16375, 11));

        airplaneList.add(new LargeAirplane("CESSNA", "CITATION COLUMBUS", 15, 7408, 2347, 13716, 10));
        airplaneList.add(new LargeAirplane("FALCON", "2000DX", 16, 5955, 2348, 18597, 19));
        airplaneList.add(new LargeAirplane("Bombardier", "Challenger 600", 17, 7491, 2348, 21863, 19));

        airplaneList.add(new CargoVersionOfPassengerAircraft("McDonnell Douglas", "MD-11ER", 22, 13408, 3564, 286_000));
    }

    @Test
    public void getTotalPassengersCapacityOfAirlinersTest() {
        assertTrue(airlineCompanyMethods.getTotalPassengersCapacityOfAirliners() == 155);
    }

    @Test
    public void sortByFlightDistanceTest() {
        ArrayList<Airplane> sortedList = airlineCompanyMethods.sortByFlightDistance();
        assertTrue(sortedList.get(0).equals(new Turboprop("KING AIR", "C90GTI", 3, 1_539, 345, 4_581, 5)));
        assertTrue(sortedList.get(5).equals(new SmallAirplane("Cessna", "CJ1+", 11, 2400, 1234, 4853, 5)));
        assertTrue(sortedList.get(8).equals(new Turboprop("KING AIR", "350", 1, 2_850, 123, 6_849, 9)));
        assertTrue(sortedList.get(17).equals(new CargoVersionOfPassengerAircraft("McDonnell Douglas", "MD-11ER", 22, 13408, 3564, 286000)));
    }

    @Test
    public void getTotalWeightCapacity() {
        assertTrue(airlineCompanyMethods.getTotalWeightCapacity() == 434621);
    }

    @Test
    public void getAirplaneWithFuelConsumptionTest() {
        List<Airplane> airplanesArray = airlineCompanyMethods.getAirplaneWithFuelConsumption(345, 1231);
        assertTrue(airplanesArray.get(0).equals(new Turboprop("KING AIR", "C90GTI", 3, 1_539, 345, 4_581, 5)));
        assertTrue(airplanesArray.get(3).equals(new LightAirplane("Adam Aircraft", "А700", 6, 2_037, 678, 3_856, 6)));
        assertTrue(airplanesArray.get(6).equals(new SmallAirplane("Cessna", "Citation Bravo", 9, 3232, 1231, 6713, 7)));
    }
}
