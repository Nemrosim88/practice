package com.courses.se.advanced.piano;

import com.courses.se.advanced.piano.model.Key;
import com.courses.se.advanced.piano.model.Music;
import com.courses.se.advanced.piano.model.Piano;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/** PianoServiceTest created for PianoService class tests. */
public class PianoServiceTest {

    private PianoService ps = new PianoService(new Piano());

    @Test
    public void tuneThePianoTest() {
        ps.tuneThePiano();
        Key key = ps.getPiano().getKey()[0];
        assertTrue(key.equals(new Key(Key.Note.LA, Key.Octave.SUB_CONTRA)) && key.isWhite());
        key = ps.getPiano().getKey()[87];
        assertTrue(key.equals(new Key(Key.Note.DO, Key.Octave.FIVE_LINED)) && key.isWhite());
        key = ps.getPiano().getKey()[33];
        assertTrue(key.equals(new Key(Key.Note.FA_DIESIS, Key.Octave.SMALL)) && !key.isWhite());
        key = ps.getPiano().getKey()[71];
        assertTrue(key.equals(new Key(Key.Note.SOL_DIESIS, Key.Octave.THREE_LINED)) && !key.isWhite());
        key = ps.getPiano().getKey()[17];
        assertTrue(key.equals(new Key(Key.Note.RE, Key.Octave.GREAT)) && key.isWhite());
        key = ps.getPiano().getKey()[56];
        assertTrue(key.equals(new Key(Key.Note.FA, Key.Octave.TWO_LINED)) && key.isWhite());
    }

    @Test
    public void playThePianoTest() {
        assertTrue(ps.playThePiano(false, false, false).equals(new Music(true)));
        assertTrue(ps.playThePiano(true, true, true, 5).equals(new Music(false)));
        assertTrue(ps.playThePiano(false, false, true, 5, 8, 13, 51).equals(new Music(true)));
    }

    @Test
    public void pressTheKeyTest() {
        assertTrue(ps.pressTheKey(54).equals(new Music(true)));
        ps.getPiano().setKey(new Key[]{new Key(Key.Note.UNTUNED, Key.Octave.ONE_LINED)});
        assertTrue(ps.pressTheKey(0).equals(new Music(false)));
    }
}
