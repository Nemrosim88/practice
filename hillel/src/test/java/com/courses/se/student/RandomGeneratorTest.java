package com.courses.se.student;

import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.assertTrue;

/**
 * Class created for testing RandomGenerator class.
 */
public class RandomGeneratorTest {

  @Test
  public void getRandomIntTest() {
    for (int i = 0; i < 100; i++) {
      assertTrue(RandomGenerator.getRandomInt(-20, 20) >= -20 && RandomGenerator.getRandomInt(-20, 20) <= 20);
      assertTrue(RandomGenerator.getRandomYear() >= 1950 && RandomGenerator.getRandomYear() <= 1998);
      assertTrue(RandomGenerator.getRandomDay() >= 1 && RandomGenerator.getRandomDay() <= 28);
      assertTrue(RandomGenerator.getRandomMonth() >= 0 && RandomGenerator.getRandomMonth() <= 11);
      assertTrue(RandomGenerator.getRandomChar() >= 65 && RandomGenerator.getRandomChar() <= 70);
      assertTrue(RandomGenerator.getRandomPhoneNumber().matches("[+][3][8][0][4][4][0-9]{7}"));
      assertTrue(RandomGenerator.getRandomStreetName().matches("[a-zA-Z0-9., ]*"));
    }
  }

  @Test
  public void getRandomNameTest() {
    HashSet<String> namesList = new HashSet<>();
    for (int i = 0; i < 100; i++) {
      namesList.add(RandomGenerator.getRandomName());
    }

    assertTrue(!namesList.add("Andrey"));
    assertTrue(!namesList.add("Tony"));
    assertTrue(!namesList.add("Victor"));
    assertTrue(!namesList.add("Jason"));
  }

  @Test
  public void getRandomSecondNameTest() {
    HashSet<String> secondNameList = new HashSet<>();
    for (int i = 0; i < 100; i++) {
      secondNameList.add(RandomGenerator.getRandomSecondName());
    }

    assertTrue(!secondNameList.add("Petrovich"));
    assertTrue(!secondNameList.add("Viktorovich"));
    assertTrue(!secondNameList.add("Stepanovich"));
    assertTrue(!secondNameList.add("Ivanovich"));
  }

  @Test
  public void getRandomPatronymicTest() {
    HashSet<String> patronymicList = new HashSet<>();
    for (int i = 0; i < 100; i++) {
      patronymicList.add(RandomGenerator.getRandomPatronymic());
    }

    assertTrue(!patronymicList.add("Voloshin"));
    assertTrue(!patronymicList.add("Prasolov"));
    assertTrue(!patronymicList.add("Petrunchak"));
    assertTrue(!patronymicList.add("Homenko"));
  }

  @Test
  public void getRandomFacultyTest() {
    HashSet<String> facultyList = new HashSet<>();
    for (int i = 0; i < 100; i++) {
      facultyList.add(RandomGenerator.getRandomFaculty());
    }

    assertTrue(!facultyList.add("Faculty of Geography"));
    assertTrue(!facultyList.add("Faculty of History"));
  }

  @Test
  public void getRandomCourseTest() {
    HashSet<String> courseList = new HashSet<>();
    for (int i = 0; i < 100; i++) {
      courseList.add(RandomGenerator.getRandomCourse(RandomGenerator.getRandomFaculty()));
    }

    assertTrue(!courseList.add("Economic and social geography"));
    assertTrue(!courseList.add("Geography and tourism"));
    assertTrue(!courseList.add("Ancient and modern history of Ukraine"));
    assertTrue(!courseList.add("Art History"));
    assertTrue("Something wrong!".equals(RandomGenerator.getRandomCourse("Nothing here!")));
  }
}