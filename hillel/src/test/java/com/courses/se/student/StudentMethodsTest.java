package com.courses.se.student;

import com.courses.se.student.model.Student;
import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.assertTrue;

/**
 * Class created for testing of StudentMethods class.
 */
public class StudentMethodsTest {
  private static StudentMethods students = new StudentMethods();
  private static final Student[] ARRAY_OF_STUDENTS = StudentMethods.createNewArrayOfStudent(50);
  private static final String NAME_OF_FACULTY = "Faculty of Geography";

  @Test
  public void studentsSearchListByFacultyTest() {
    for (Student student : students.studentsSearchList(ARRAY_OF_STUDENTS, NAME_OF_FACULTY)) {
      assertTrue("Faculty of Geography".equals(student.getFaculty()));
    }
  }

  @Test
  public void studentsSearchListByCourseTest() {
    String course = "Ancient and modern history of Ukraine";
    for (Student student : students.studentsSearchList(ARRAY_OF_STUDENTS, NAME_OF_FACULTY, course)) {
      assertTrue(course.equals(student.getCourse()) && NAME_OF_FACULTY.equals(student.getFaculty()));
    }
  }

  @Test
  public void studentsSearchListByYearTest() {
    int year = 1984;
    for (Student student : students.studentsSearchList(ARRAY_OF_STUDENTS, year)) {
      assertTrue(student.getBirth().get(GregorianCalendar.YEAR) > year);
    }
  }

  @Test
  public void studentsSearchListByGroupTest() {
    for (Student student : students.studentsSearchList(ARRAY_OF_STUDENTS, 'A')) {
      assertTrue(student.getGroup() == 'A');
    }
  }
}
