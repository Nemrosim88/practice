package com.courses.se.util;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * TextTest.
 */
public class TextTest {
    Text textClass = new Text();

    @Test
    public void firstSolutionTest() {
        String firstText = "One, two three, four five six, seven eight, nine.";
        String firstResult = "nine, One two, three four five, six seven, eight.";
        String secondText = "fhgy jhdfu, FHG, ZXC, mnb qwe, sdf, lkj, YjU.";
        String secondResult = "YjU fhgy, jhdfu, FHG, ZXC mnb, qwe, sdf, lkj.";
        assertTrue(textClass.firstSolution(firstText).equals(firstResult));
        assertTrue(textClass.firstSolution(secondText).equals(secondResult));
    }

    @Test
    public void secondSolutionTest() {
        String text = "Oae, awo tyaee, four five six, seven eight, nine.";
        String[] result = {"seven", "eight", "tyaee", "nine", "five", "four", "six", "awo", "Oae"};
        assertArrayEquals(result, textClass.secondSolution(text));
    }

    @Test
    public void thirdSolutionTest() {
        String text = "Ooeee, awoa tyaete, four five six, sesvsen eighte, ninen.";
        String result = "Oeee, awo tyaee, four five six, seven eight, nie.";
        String secResult = "Ooe, woa tyate, four five six, sesvsen ighte, ien.";
        assertTrue(result.equals(textClass.thirdSolution(text, true)));
        assertTrue(secResult.equals(textClass.thirdSolution(text, false)));
    }

    @Test
    public void fourthSolutionTest() {
        String text = "Ooeee, awoa tyaete, four five six, sesvsen eighte, ninen.";
        String result = "Ooeee, awoa tyaete, four five HELLO WORLD!, sesvsen eighte, ninen.";
        assertTrue(result.equals(textClass.fourthSolution(text, 3, "HELLO WORLD!")));
    }
}
