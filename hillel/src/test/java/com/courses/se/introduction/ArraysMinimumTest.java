package com.courses.se.introduction;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Class created for testing of ArraysMinimum class.
 */
public class ArraysMinimumTest {

  @Test
  public void getPairNumbersTest() {
    List<Integer> resultList = ArraysMinimum.getPairNumbers(new int[]{-40, 13, 3, 0, 1, 10, 20, 31});
    Integer[] resultArray = new Integer[resultList.size()];
    resultArray = resultList.toArray(resultArray);
    assertArrayEquals(new Integer[]{-40, 0, 10, 20}, resultArray);
  }

  @Test
  public void getReversedArrayTest() {
    List<Integer> resultList = ArraysMinimum.getReversedArray(new int[]{-3, 0, 5, 10, 100});
    Integer[] resultArray = new Integer[resultList.size()];
    resultArray = resultList.toArray(resultArray);
    assertArrayEquals(new Integer[]{100, 10, 5, 0, -3}, resultArray);
  }

  @Test
  public void getFirstAndSecondPartTest() {
    int[][] resultArray = ArraysMinimum.getFirstAndSecondPart(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    assertArrayEquals(new int[][]{{1, 2, 3, 4, 5}, {6, 7, 8, 9, 10}}, resultArray);
  }

  @Test
  public void getFirstAndSecondPartsReversedTest() {
    int[][] resultArray = ArraysMinimum.getFirstAndSecondPartsReversed(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10});
    assertArrayEquals(new int[][]{{5, 4, 3, 2, 1}, {10, 9, 8, 7, 6}}, resultArray);
  }

  @Test
  public void getSumOfAllElementsTest() {
    int resultSum = ArraysMinimum.getSumOfAllElements(new int[]{-12, -12, 40, 31, 0, 0, 2, 7, 400});
    assertTrue(456 == resultSum);
  }

  @Test
  public void getAverageNumberTest() {
    assertTrue(4.8 == ArraysMinimum.getAverageNumber(new int[]{-6, 2, 3, 4, 5, 6, 7, 8, 9, 10}));
  }

  @Test
  public void getMinOfArrayTest() {
    assertTrue(-500 == ArraysMinimum.getMinOfArray(new int[]{-345, 2, 3, 4, 5, 6, 7, -500, 9, 10}));
  }

  @Test
  public void getMaxOfArrayTest() {
    assertTrue(456 == ArraysMinimum.getMaxOfArray(new int[]{-345, 2, 3, 4, 5, 456, 7, 8, 9, 10}));
  }

  @Test
  public void setNegativeNumbersToZeroTest() {
    int[] inputArray = {-345, 2, -1, 4, 5, -456, 7, 8, 9, 10};
    assertArrayEquals(new int[]{0, 2, 0, 4, 5, 0, 7, 8, 9, 10}, ArraysMinimum.setNegativeNumbersToZero(inputArray));
  }

  @Test
  public void getAmountOfEqualNumbersTest() {
    assertTrue(ArraysMinimum.getAmountOfEqualNumbers(new int[]{1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 9, 10}) == 5);
  }

  @Test
  public void reverseMinAndMaxTest() {
    int[] inputArray = {-345, 2, -1, 4, 5};
    ArraysMinimum.reverseMinAndMax(inputArray);
    assertArrayEquals(new int[]{5, 2, -1, 4, -345}, inputArray);
  }

  @Test
  public void reverseArrayTest() {
    int[] inputArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    assertArrayEquals(new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, ArraysMinimum.reverseArray(inputArray));
  }

  @Test
  public void getPartWithBiggestAverageTest() {
    List<Integer> firstList = ArraysMinimum.getPartWithBiggestAverage(new int[]{1, 1, 1, 2, 2, 2});
    List<Integer> secondList = ArraysMinimum.getPartWithBiggestAverage(new int[]{2, 2, 2, 1, 1, 1});
    List<Integer> thirdList = ArraysMinimum.getPartWithBiggestAverage(new int[]{3, 3, 3, 3, 3, 3});

    Integer[] firstResultArray = new Integer[firstList.size()];
    Integer[] secondResultArray = new Integer[secondList.size()];
    Integer[] thirdResultArray = new Integer[thirdList.size()];

    assertArrayEquals(new Integer[]{2, 2, 2}, firstList.toArray(firstResultArray));
    assertArrayEquals(new Integer[]{2, 2, 2}, secondList.toArray(secondResultArray));
    assertArrayEquals(new Integer[]{}, thirdList.toArray(thirdResultArray));
  }

  @Test
  public void getIndexesAndSum() {
    ArrayList<Integer> resultList = ArraysMinimum.getIndexesAndSum(new int[]{-2, 4, -46, 1, -56, -3, 10, 1, 0, 2});
    Integer[] resultArray = new Integer[resultList.size()];
    assertArrayEquals(new Integer[]{1, 3, 6, 7, 9, -107}, resultList.toArray(resultArray));
  }

  @Test
  public void getNumbersToZeroValue() {
    ArrayList<Integer> firstList = ArraysMinimum.getNumbersToZeroValue(new int[]{-3, -2, -1, 1, 0, 3});
    ArrayList<Integer> secondList = ArraysMinimum.getNumbersToZeroValue(new int[]{-3, -2, -1, 0});

    Integer[] firstResult = new Integer[firstList.size()];
    Integer[] secondResult = new Integer[secondList.size()];

    assertArrayEquals(new Integer[]{-3, -2, -1, 1}, firstList.toArray(firstResult));
    assertArrayEquals(new Integer[]{-3, -2, -1}, secondList.toArray(secondResult));
  }

  @Test
  public void changePlacesMaxAndSecondMax() {
    int[] inputArray = {-200, 2, 3, 4, -40000, 6, 7, 8, 9, 10};
    ArraysMinimum.changePlacesMaxAndSecondMax(inputArray);
    assertArrayEquals(new int[]{-200, 2, 3, 4, -40000, 6, 7, 8, 10, 9}, inputArray);
  }

  @Test
  public void getNumbersToMinValue() {
    ArrayList<Integer> resultList = ArraysMinimum.getNumbersToMinValue(new int[]{1, 2, 3, 4, 5, 6, -7});
    Integer[] resultArray = new Integer[resultList.size()];
    assertArrayEquals(new Integer[]{1, 2, 3, 4, 5, 6}, resultList.toArray(resultArray));
  }
}