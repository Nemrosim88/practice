package com.courses.se.introduction;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Class created for testing of ArraysVeryHard class.
 */
public class ArraysVeryHardTest {

  @Test
  public void moveToTheEndTest() {
    int[] inputArray = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    ArraysVeryHard.moveToTheEnd(inputArray, 1, 5);
    assertArrayEquals(new int[]{6, 7, 8, 9, 10, 1, 2, 3, 4, 5}, inputArray);
  }
}