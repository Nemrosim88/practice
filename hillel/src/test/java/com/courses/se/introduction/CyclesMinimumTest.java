package com.courses.se.introduction;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Class created for testing of CyclesMinimum class.
 */
public class CyclesMinimumTest {

  @Test
  public void getSequenceOfFourDigitNumbersTest() {
    List<Integer> actualResultList = CyclesMinimum.getSequenceOfFourDigitNumbers();
    assertTrue(actualResultList.get(0) == 1000
            && actualResultList.get(88) == 1264
            && actualResultList.get(2999) == 9997
            && actualResultList.size() == 3000);
  }

  @Test
  public void getFirstFiftyFiveElementsTest() {
    List<Integer> actualResultList = CyclesMinimum.getFirstFiftyFiveElements();
    assertTrue(actualResultList.get(0) == 1
            && actualResultList.get(36) == 73
            && actualResultList.get(54) == 109
            && actualResultList.size() == 55);
  }

  @Test
  public void getSequenceFromNinetyToOneTest() {
    List<Integer> actualResultList = CyclesMinimum.getSequenceFromNinetyToOne();
    assertTrue(actualResultList.get(0) == 90
            && actualResultList.get(5) == 65
            && !actualResultList.contains(-1));
  }

  @Test
  public void getSequenceOfTwentyTest() {
    List<Integer> actualResultList = CyclesMinimum.getSequenceOfTwenty();
    assertTrue(actualResultList.get(0) == 2
            && actualResultList.get(5) == 64
            && actualResultList.get(6) == 128
            && actualResultList.size() == 20);
  }

  @Test
  public void getFactorialOfNTest() {
    assertTrue(CyclesMinimum.getFactorialOfN(5) == 120
            && CyclesMinimum.getFactorialOfN(6) == 720);
  }

    @Test
    public void getFibonacciSequenceTest() {
        assertArrayEquals(new Long[]{-1L, -1L, -2L, -3L, -5L, -8L, -13L, -21L, -34L, -55L, -89L, -144L, -233L, -377L, -610L, -987L, -1597L, -2584L, -4181L, -6765L}, CyclesMinimum.getFibonacciSequence(-20));
        assertArrayEquals(new Long[]{1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L, 89L, 144L, 233L, 377L, 610L, 987L, 1597L, 2584L, 4181L, 6765L}, CyclesMinimum.getFibonacciSequence(20));
        assertArrayEquals(new Long[]{}, CyclesMinimum.getFibonacciSequence(0));
        assertArrayEquals(new Long[]{1L, 1L, 2L, 3L, 5L, 8L, 13L, 21L, 34L, 55L,}, CyclesMinimum.getFibonacciSequence(10));
        assertArrayEquals(new Long[]{-1L, -1L, -2L, -3L, -5L, -8L, -13L, -21L, -34L, -55L,}, CyclesMinimum.getFibonacciSequence(-10));
    }
}
