package com.courses.se.introduction;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Class created for testing of ArraysHard class.
 */
public class ArraysHardTest {

  @Test
  public void getSumOfValuesBetweenMinAdnMaxTest() {
    assertTrue(ArraysHard.getSumOfValuesBetweenMinAdnMax(new int[]{5, -20, 1, 2, 3, 4, 20}) == 10
            && ArraysHard.getSumOfValuesBetweenMinAdnMax(new int[]{5, 20, 6, 7, 8, 9, -20}) == 30);
  }

  @Test
  public void getHalfOfAnArrayTest() {
    assertArrayEquals(new Integer[]{1, -1, 1, 1,}, ArraysHard.getHalfOfAnArray(new int[]{1, -1, 1, 1, 2, 2, 2, 2,}));
    assertArrayEquals(new Integer[]{1, 1, -1, 1}, ArraysHard.getHalfOfAnArray(new int[]{2, 2, 2, 2, 1, 1, -1, 1}));
  }
}
