package com.courses.se.introduction;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Class created for testing IfElse class.
 */
public class IfElseTest {

  @Test
  public void getMaxOfThreeTest() {
    assertTrue(IfElse.getMaxOfThree(10, 20, 30) == 30);
    assertTrue(IfElse.getMaxOfThree(20, 30, 10) == 30);
    assertTrue(IfElse.getMaxOfThree(30, 30, 30) == 30);
  }

  @Test
  public void convertDecimalToBinaryTest() {
    assertTrue("00010011".equals(IfElse.convertDecimalToBinary(19))
            && "11111111".equals(IfElse.convertDecimalToBinary(255)));
  }

  @Test
  public void getDecimalFromBinaryTest() {
    assertTrue(IfElse.getDecimalFromBinary("00010011") == 19
            && IfElse.getDecimalFromBinary("11111111") == 255);
  }

  @Test
  public void secondAfterMaximumTest() {
    assertTrue(IfElse.secondAfterMaximum(new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}) == 9
            && IfElse.secondAfterMaximum(new int[]{-23, -32, 0, 345, 1000}) == 345);
  }
}