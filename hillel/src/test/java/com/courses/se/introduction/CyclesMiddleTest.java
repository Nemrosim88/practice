package com.courses.se.introduction;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

/**
 * Class created for testing of CyclesMiddle class.
 */
public class CyclesMiddleTest {

  @Test
  public void positiveDividers() {
    ArrayList<Integer> firstList = CyclesMiddle.positiveDividers(20);
    ArrayList<Integer> secondList = CyclesMiddle.positiveDividers(63);

    Integer[] firstResult = new Integer[firstList.size()];
    Integer[] secondResult = new Integer[secondList.size()];

    assertArrayEquals(new Integer[]{1, 2, 4, 5, 10, 20}, firstList.toArray(firstResult));
    assertArrayEquals(new Integer[]{1, 3, 7, 9, 21, 63}, secondList.toArray(secondResult));
  }

  @Test
  public void commonDividers() {
    ArrayList<Integer> firstList = CyclesMiddle.commonDividers(22,64);
    ArrayList<Integer> secondList = CyclesMiddle.commonDividers(28,44);

    Integer[] firstResult = new Integer[firstList.size()];
    Integer[] secondResult = new Integer[secondList.size()];

    assertArrayEquals(new Integer[]{1, 2}, firstList.toArray(firstResult));
    assertArrayEquals(new Integer[]{1, 2, 4}, secondList.toArray(secondResult));
  }
}