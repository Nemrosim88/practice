package com.courses.se.introduction;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Class created for testing of CyclesHard class.
 */
public class CyclesHardTest {

  @Test
  public void amountOfLuckyNumbers() {
    assertTrue(CyclesHard.amountOfLuckyNumbers(0, 999999) == 55252
            && CyclesHard.amountOfLuckyNumbers(0, 999) == 1);
  }

  @Test
  public void digitalClockTest() {
    assertTrue(CyclesHard.digitalClock() == 16);
  }

  @Test
  public void notLuckyNumbersTest() {
    assertTrue(CyclesHard.notLuckyNumbers(456) == 3999
            && CyclesHard.notLuckyNumbers(99999) == 19);
  }
}
