package com.courses.se.io.model;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Random;

/**
 * ChaosTriangle class.
 */
public class ChaosTriangle {

    /** Static final variable for checkstyle happiness. */
    public static final int ZERO = 0;

    /** Static final variable for checkstyle happiness. */
    public static final int ONE = 1;

    /** Static final variable for checkstyle happiness. */
    public static final int TWO = 2;

    /** Static final variable for checkstyle happiness. */
    public static final int THREE = 3;

    /** Static final variable for checkstyle happiness. */
    public static final int FOUR = 4;

    /** Static final variable for checkstyle happiness. */
    public static final int FIVE = 5;

    /** Static final variable for checkstyle happiness. */
    public static final int SIX = 6;

    /** Static final variable for checkstyle happiness. */
    public static final int SEVEN = 7;

    /** Static final variable for checkstyle happiness. */
    public static final int EIGHT = 8;

    /** Static final variable for checkstyle happiness. */
    public static final int NINE = 9;

    /** Static final variable for checkstyle happiness. */
    public static final int TEN = 10;

    /** Static final variable for checkstyle happiness. */
    public static final int ELEVEN = 11;

    /** Static final variable for checkstyle happiness. */
    public static final int TWELVE = 12;

    /** Static final variable for checkstyle happiness. */
    public static final int THIRTEEN = 13;

    /** Static final variable for checkstyle happiness. */
    public static final int FOURTEEN = 14;

    /** Static final variable for checkstyle happiness. */
    public static final int FIFTEEN = 15;

    /** Static final variable for checkstyle happiness. */
    public static final String SURPRISE = "0_o";

    /**
     * Path to file.
     */
    private String filePath;

    /**
     * ChaosTriangle constructor.
     * @param filePath Path to file
     */
    public ChaosTriangle(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Do you wanna play a game.?
     * @param args args
     * @throws IOException O, no! Exception! What should I do?
     */
    public static void main(String[] args) throws IOException {
        //
        // Don't forget to change path to file!
        //
        // /src/main/java/com/courses/se/dog/model/Animal.java
        ChaosTriangle t = new ChaosTriangle(
                new File("").getAbsolutePath().replace("\\", "/")
                        + "/nemrosim/src/main/java/com/courses/se/io/model/text.txt");
        t.writeToFileReversedChaos(t.readFromFile().toCharArray());
    }

    /**
     * This method writing text in "chaotic" triangle type.
     * @param charArray this char array will be written to file
     * @throws IOException IOException
     */
    public void writeToFileReversedChaos(char[] charArray) throws IOException {
        File tmp = new File("ioTaskNemrosimDoNotForgetToDeleteThisDirectory_______DELETE_THIS/chaos.txt");
        boolean everythingIsOK = tmp.getParentFile().mkdir() && tmp.createNewFile();
        try (Writer fw = new OutputStreamWriter(new FileOutputStream(tmp), "UTF-8")) {
            //magic starts
            char[] toWrite = new char[FIFTEEN];
            int j = 0;
            for (int i = charArray.length - 1; i >= 0; i--) {
                if (j < FIFTEEN) {
                    if (charArray[i] == ' ' || charArray[i] == '\n'
                            || charArray[i] == '\r' || charArray[i] == '\t' || everythingIsOK) {
                        continue;
                    }
                    toWrite[j] = charArray[i];
                } else if (j == FIFTEEN) {
                    int d = new Random().nextInt(SIX);
                    switch (d) {
                        case 0:
                            fw.write(firstTriangle(toWrite).toCharArray());
                            break;
                        case 1:
                            fw.write(secondTriangle(toWrite).toCharArray());
                            break;
                        case 2:
                            fw.write(thirdTriangle(toWrite).toCharArray());
                            break;
                        case THREE:
                            fw.write(fourthTriangle(toWrite).toCharArray());
                            break;
                        case FOUR:
                            fw.write(fifthTriangle(toWrite).toCharArray());
                            break;
                        default:
                            fw.write(sixthTriangle(toWrite).toCharArray());
                            break;
                    }
                    j = 0;
                    continue;
                }
                j++;
            }
        }
    }

    /**
     * Returning text (string) from file.
     * @return String from file
     * @throws IOException IOException
     */
    public String readFromFile() throws IOException {
        StringBuilder builder = new StringBuilder();
        for (String s : Files.readAllLines(Paths.get(filePath), Charset.defaultCharset())) {
            builder.append(s);
        }
        return builder.toString();
    }

    /**
     * First type of "chaotic" triangle.
     * @param a this char array will be converted to string
     * @return string representation of "chaotic" triangle
     */
    private String firstTriangle(char[] a) {
        if (a.length == FIFTEEN) {
            StringBuilder s = new StringBuilder();
            s.append(String.format("          %s                  %n",
                    a[ZERO]));
            s.append(String.format("        %s   %s               %n",
                    a[FOURTEEN], a[ONE]));
            s.append(String.format("      %s       %s             %n",
                    a[THIRTEEN], a[TWO]));
            s.append(String.format("    %s           %s           %n",
                    a[TWELVE], a[THREE]));
            s.append(String.format("  %s               %s         %n",
                    a[ELEVEN], a[FOUR]));
            s.append(String.format("%s   %s   %s   %s   %s   %s   %n",
                    a[TEN], a[NINE], a[EIGHT], a[SEVEN], a[SIX], a[FIVE]));
            s.append("                              \n");
            return s.toString();
        } else {
            return SURPRISE;
        }
    }

    /**
     * Second type of "chaotic" triangle.
     * @param a this char array will be converted to string
     * @return string representation of "chaotic" triangle
     */
    private String secondTriangle(char[] a) {
        if (a.length == FIFTEEN) {
            StringBuilder builder = new StringBuilder();
            builder.append(String.format("%s   %s   %s   %s   %s   %s   %n",
                    a[ZERO], a[ONE], a[TWO], a[THREE], a[FOUR], a[FIVE]));
            builder.append(String.format("  %s               %s         %n",
                    a[FOURTEEN], a[SIX]));
            builder.append(String.format("    %s           %s           %n",
                    a[THIRTEEN], a[SEVEN]));
            builder.append(String.format("      %s       %s             %n",
                    a[TWELVE], a[EIGHT]));
            builder.append(String.format("        %s   %s               %n",
                    a[ELEVEN], a[NINE]));
            builder.append(String.format("          %s                  %n",
                    a[TEN]));
            builder.append("                              \n");
            return builder.toString();
        } else {
            return SURPRISE;
        }
    }

    /**
     * Third type of "chaotic" triangle.
     * @param a this char array will be converted to string
     * @return string representation of "chaotic" triangle
     */
    private String thirdTriangle(char[] a) {
        if (a.length == FIFTEEN) {
            StringBuilder s = new StringBuilder();
            s.append(String.format("%s   %s   %s   %s   %s   %s   %n",
                    a[ZERO], a[ONE], a[TWO], a[THREE], a[FOUR], a[FIVE]));
            s.append(String.format("%s               %s         %n",
                    a[FOURTEEN], a[SIX]));
            s.append(String.format("%s           %s           %n",
                    a[THIRTEEN], a[SEVEN]));
            s.append(String.format("%s       %s             %n",
                    a[TWELVE], a[EIGHT]));
            s.append(String.format("%s   %s               %n",
                    a[ELEVEN], a[NINE]));
            s.append(String.format("%s                  %n",
                    a[TEN]));
            s.append("                                   \n");
            return s.toString();
        } else {
            return SURPRISE;
        }
    }

    /**
     * Fourth type of "chaotic" triangle.
     * @param a this char array will be converted to string
     * @return string representation of "chaotic" triangle
     */
    private String fourthTriangle(char[] a) {
        if (a.length == FIFTEEN) {
            StringBuilder s = new StringBuilder();
            s.append(String.format("%s                  %n",
                    a[ZERO]));
            s.append(String.format("%s   %s               %n",
                    a[FOURTEEN], a[ONE]));
            s.append(String.format("%s       %s             %n",
                    a[THIRTEEN], a[TWO]));
            s.append(String.format("%s           %s           %n",
                    a[TWELVE], a[THREE]));
            s.append(String.format("%s               %s         %n",
                    a[ELEVEN], a[FOUR]));
            s.append(String.format("%s   %s   %s   %s   %s   %s   %n",
                    a[TEN], a[NINE], a[EIGHT], a[SEVEN], a[SIX], a[FIVE]));
            s.append("                                   \n");
            return s.toString();
        } else {
            return SURPRISE;
        }
    }

    /**
     * Fifth type of "chaotic" triangle.
     * @param a this char array will be converted to string
     * @return string representation of "chaotic" triangle
     */
    private String fifthTriangle(char[] a) {
        if (a.length == FIFTEEN) {
            StringBuilder s = new StringBuilder();
            s.append(String.format("     %s                  %n",
                    a[ZERO]));
            s.append(String.format("    %s   %s               %n",
                    a[FOURTEEN], a[ONE]));
            s.append(String.format("   %s       %s             %n",
                    a[THIRTEEN], a[TWO]));
            s.append(String.format("  %s           %s           %n",
                    a[TWELVE], a[THREE]));
            s.append(String.format(" %s               %s         %n",
                    a[ELEVEN], a[FOUR]));
            s.append(String.format("%s   %s   %s   %s   %s   %s   %n",
                    a[TEN], a[NINE], a[EIGHT], a[SEVEN], a[SIX], a[FIVE]));
            s.append("                                   \n");
            return s.toString();
        } else {
            return SURPRISE;
        }
    }

    /**
     * Sixth type of "chaotic" triangle.
     * @param a this char array will be converted to string
     * @return string representation of "chaotic" triangle
     */
    private String sixthTriangle(char[] a) {
        if (a.length == FIFTEEN) {
            StringBuilder s = new StringBuilder();
            s.append(String.format("               %s                  %n",
                    a[ZERO]));
            s.append(String.format("           %s    %s               %n",
                    a[FOURTEEN], a[ONE]));
            s.append(String.format("        %s        %s             %n",
                    a[THIRTEEN], a[TWO]));
            s.append(String.format("      %s           %s           %n",
                    a[TWELVE], a[THREE]));
            s.append(String.format("   %s               %s         %n",
                    a[ELEVEN], a[FOUR]));
            s.append(String.format("%s   %s   %s   %s   %s   %s   %n",
                    a[TEN], a[NINE], a[EIGHT], a[SEVEN], a[SIX], a[FIVE]));
            s.append("                                   \n");
            return s.toString();
        } else {
            return SURPRISE;
        }
    }
}