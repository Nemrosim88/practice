package com.courses.se.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Text class (MIN-136 Solution).
 * 1. В предложении из n слов первое слово поставить на место второго,
 * второе — на место третьего и т. д., (n-1)-е слово — на место n-го, n-е слово поставить на место первого.
 * В исходном и преобразованном предложениях между словами должны
 * быть или один пробел, или знак препинания и один пробел.
 * 2. Все слова текста рассортировать в порядке убывания их длин,
 * при этом все слова одинаковой длины рассортировать в порядке возрастания в них количества гласных букв.
 * 3. Преобразовать каждое слово в тексте, удалив из него все следующие
 * (предыдущие) вхождения первой (последней) буквы этого слова.
 * 4. В некотором предложении текста слова заданной длины заменить указанной
 * подстрокой, длина которой может не совпадать с длиной слова.
 */
public class Text {

    /**
     * Solution for the first task.
     * @param text text to change
     * @return changed text
     */
    public String firstSolution(String text) {
        String[] wordArray = text.split(" |(, )|\\.");
        String[] symbolArray = text.split("[a-zA-Zа-яА-Я]+");
        StringBuilder result = new StringBuilder(wordArray[wordArray.length - 1]);
        for (int i = 0; i < wordArray.length - 1; i++) {
            result.append(symbolArray[i + 1] + wordArray[i]);
        }
        return result.append(symbolArray[wordArray.length]).toString();
    }

    /**
     * Solution for the second task.
     * @param text text to change
     * @return changed text
     */
    public String[] secondSolution(String text) {
        String[] wordArray = text.split(" |(, )|\\.");
        List<String> list = Arrays.stream(wordArray).collect(Collectors.toList());
        list.sort((one, two) -> {
            int result = one.length() - two.length();
            if (result != 0) {
                return result;
            } else {
                return one.split("[eyuioa]+").length - two.split("[eyuioa]+").length;
            }
        });
        Collections.reverse(list);
        return list.toArray(new String[list.size()]);
    }

    /**
     * Solution for the third task.
     * @param text        text to change
     * @param firstOrLast true - first, false - last letter of the word
     * @return changed string
     */
    public String thirdSolution(String text, boolean firstOrLast) {
        String[] wordArray = text.split(" |(, )|\\.");
        String[] symbolArray = text.split("[a-zA-Zа-яА-Я]+");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < wordArray.length; i++) {
            char[] wordLetters = wordArray[i].toCharArray();
            if (firstOrLast) {
                String letter = "" + wordLetters[0];
                sb.append(letter);
                for (int j = 1; j < wordLetters.length; j++) {
                    if (!("" + wordLetters[j]).equalsIgnoreCase(letter)) {
                        sb.append(wordLetters[j]);
                    }
                }
                sb.append(symbolArray[i + 1]);
            } else {
                String letter = "" + wordLetters[wordLetters.length - 1];
                for (int j = 0; j < wordLetters.length - 1; j++) {
                    if (!("" + wordLetters[j]).equalsIgnoreCase(letter)) {
                        sb.append(wordLetters[j]);
                    }
                }
                sb.append(letter);
                sb.append(symbolArray[i + 1]);
            }
        }
        return sb.toString();
    }

    /**
     * Solution for the fourth task.
     * @param text      text to change
     * @param length    length of the word to change
     * @param substring substring to input
     * @return changed text
     */
    public String fourthSolution(String text, int length, String substring) {
        String[] wordArray = text.split(" |(, )|\\.");
        String[] symbolArray = text.split("[a-zA-Zа-яА-Я]+");
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < wordArray.length; i++) {
            if (wordArray[i].length() != length) {
                sb.append(wordArray[i]);
            } else {
                sb.append(substring);
            }
            sb.append(symbolArray[i + 1]);
        }
        return sb.toString();
    }
}
