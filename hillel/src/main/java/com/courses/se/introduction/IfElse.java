package com.courses.se.introduction;

import java.util.Arrays;

/**
 * This class contains methods that are solutions to "Variables and if/else operations" task.
 * @author Nemrosim
 * @version 1.1
 */
final class IfElse {

    /** Private constructor. */
    private IfElse() {
    }

    /**
     * Returns max value of three input values.
     * @param one   first value
     * @param two   second value
     * @param three third value
     * @return returns maximum value of three input values
     */
    static int getMaxOfThree(int one, int two, int three) {
        return Arrays.stream(new int[]{one, two, three}).boxed().mapToInt(Integer::intValue).max().getAsInt();
    }

    /**
     * Returns a binary representation of an inputInteger.
     * <br>Example: "19: = "0001 0011".
     * @param inputInteger method converts this decimal number to binary
     * @return string representation of decimal value
     */
    static String convertDecimalToBinary(int inputInteger) {
        int integerFromInput = inputInteger;
        int[] arrayForByteNumber = {0, 0, 0, 0, 0, 0, 0, 0};
        int conditionForWhile = arrayForByteNumber.length - 1;
        while (integerFromInput != 0) {
            arrayForByteNumber[conditionForWhile] = integerFromInput % 2;
            integerFromInput /= 2;
            conditionForWhile--;
        }
        StringBuilder string = new StringBuilder();
        Arrays.stream(arrayForByteNumber).forEach(string::append);
        return string.toString();
    }

    /**
     * Returns a decimal representation of "binaryNumber".
     * @param binaryNumber Example: "00101100" or "0110"
     * @return decimal representation of binary value
     */
    static int getDecimalFromBinary(String binaryNumber) {

        StringBuilder stringBuilder = new StringBuilder(binaryNumber.trim());
        stringBuilder.reverse();
        int result = 0;
        for (int j = 0; j < stringBuilder.length(); j++) {
            if (stringBuilder.charAt(j) == '1') {
                result += Math.pow(2, j);
            }
        }
        return result;
    }

    /**
     * Returns second maximum number of an "inputArray".
     * @param inputArray array of integers
     * @return second maximal value in the array
     */
    static int secondAfterMaximum(int[] inputArray) {
        int indexOfMaxValue = 0;
        int maximalValue = Integer.MIN_VALUE;

        for (int j = 0; j < inputArray.length; j++) {
            if (inputArray[j] > maximalValue) {
                maximalValue = inputArray[j];
                indexOfMaxValue = j;
            }
        }

        maximalValue = Integer.MIN_VALUE;
        for (int j = 0; j < inputArray.length; j++) {
            if (j == indexOfMaxValue) {
                continue;
            }
            if (inputArray[j] > maximalValue) {
                maximalValue = inputArray[j];
            }
        }
        return maximalValue;
    }
}
