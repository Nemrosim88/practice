package com.courses.se.introduction;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * This class contains methods that are solutions to section "Minimum" of task "Cycles. Course for beginners".
 * @author Nemrosim
 * @version 1.1
 */
final class CyclesMinimum {

    /** Private constructor. */
    private CyclesMinimum() {
    }

    /**
     * Returns all four-digit numbers of sequence 1000 1003 1006 1009 etc.
     * @return ArrayList of sequence 1000 1003 1006 1009 etc.
     */
    static List<Integer> getSequenceOfFourDigitNumbers() {
        final int sequenceStart = 1000;
        final int sequenceEnd = 9999;
        final int increment = 3;
        return IntStream.rangeClosed(sequenceStart, sequenceEnd).boxed()
                .filter(i -> (i - sequenceStart) % increment == 0).collect(Collectors.toList());
    }

    /**
     * Returns 55 elements of sequence 1 3 5 7 9 11 etc.
     * @return ArrayList that contains 55 elements of sequence
     */
    static List<Integer> getFirstFiftyFiveElements() {
        int start = 1;
        final int end = 150;
        final int amount = 55;
        return IntStream.rangeClosed(start, end).boxed()
                .filter(i -> i % 2 != 0).limit(amount).collect(Collectors.toList());
    }

    /**
     * Returns only positive numbers of sequence 90 85 80 75 70 65 60 etc.
     * @return ArrayList that contains positive numbers of sequence
     */
    static ArrayList<Integer> getSequenceFromNinetyToOne() {
        ArrayList<Integer> resultList = new ArrayList<>();
        final int startOfSequnce = 90;
        final int decrement = 5;
        for (int j = startOfSequnce; j >= 0; j -= decrement) {
            resultList.add(j);
        }
        return resultList;
    }

    /**
     * Returns 20 elements of sequence 2 4 8 16 32 64 128 etc.
     * @return ArrayList that contains 20 elements of sequence
     */
    static ArrayList<Integer> getSequenceOfTwenty() {
        ArrayList<Integer> resultList = new ArrayList<>();
        int valueOfSequence = 2;
        final int amountOfElements = 20;
        for (int j = 0; j < amountOfElements; j++) {
            resultList.add(valueOfSequence);
            valueOfSequence *= 2;
        }
        return resultList;
    }

    /**
     * Returns factorial of natural number N.
     * @param naturalNumber N
     * @return factorial of natural number
     */
    static long getFactorialOfN(int naturalNumber) {
        long factorialOfN = 1;
        for (int j = 1; j < (naturalNumber + 1); j++) {
            factorialOfN *= j;
        }
        return factorialOfN;
    }

    /**
     * Method returns first 20 elements of "number" fibonacci sequence.
     * @param number fibonacci number
     * @return array[] that contains 20 elements of "number" fibonacci sequence.
     */
    static Long[] getFibonacciSequence(int number) {
        if (number >= 0) {
            return Stream.generate(new FibonacciPlus()).limit(number).toArray(Long[]::new);
        } else {
            return Stream.generate(new FibonacciMinus()).limit(Math.abs(number)).toArray(Long[]::new);
        }
    }

    /** Implemented Supplier for getFibonacciSequence method. */
    private static class FibonacciPlus implements Supplier<Long> {
        /** Variable for previous elements. */
        private long p = 0;
        /** Variable for current variable. */
        private long c = 1;

        @Override
        public Long get() {
            long next = c + p;
            p = c;
            c = next;
            return p;
        }
    }

    /** Implemented Supplier for getFibonacciSequence method. For negative values. */
    private static class FibonacciMinus implements Supplier<Long> {
        /** Variable for previous elements. */
        private long p = 0;
        /** Variable for current variable. */
        private long c = -1;

        @Override
        public Long get() {
            long next = c + p;
            p = c;
            c = next;
            return p;
        }
    }
}
