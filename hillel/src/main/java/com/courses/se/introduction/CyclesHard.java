package com.courses.se.introduction;

/**
 * This class contains methods that are solutions to section "hard" of task
 * "Cycles. Course for beginners".
 */
final class CyclesHard {

    /** Private constructor. */
    private CyclesHard() {
    }

    /**
     * Lucky Numbers.
     * Returns amount of lucky numbers in the range
     * from "start" parameter (min: 0) to "end" parameter(max: 999999).
     * @param start from what number should search starts
     * @param end   at what number where should search ends
     * @return amount of lucky numbers
     */
    static int amountOfLuckyNumbers(final int start, final int end) {
        int amountOfLuckyNumbers = 0;
        for (int ticketsNumber = start; ticketsNumber <= end; ticketsNumber++) {
            if (luckyOrNotNumber(ticketsNumber)) {
                amountOfLuckyNumbers++;
            }
        }
        return amountOfLuckyNumbers;
    }

    /**
     * Private method. Method returns false if it is not a lucky number,
     * else returns true.
     * @param number number of the ticket
     * @return true or false depending is it a lucky number or not
     */
    private static boolean luckyOrNotNumber(int number) {

        int firstHalfSumOfANumber = 0;
        int secondHalfSumOfANumber = 0;

        //if numbers = 236, this code will convert it to 000236
        char[] charArray = String.format("%06d", number).toCharArray();

        for (int i = 0; i < charArray.length; i++) {
            if (i < (charArray.length / 2)) {
                firstHalfSumOfANumber += Integer.parseInt(
                        Character.toString(charArray[i]));
            } else {
                secondHalfSumOfANumber += Integer.parseInt(
                        Character.toString(charArray[i]));
            }
        }
        return firstHalfSumOfANumber == secondHalfSumOfANumber;
    }

    /**
     * Digital Clock
     * <br>Method counts how many times a day, digital clock shows
     * symmetric time. Example. 02:20, 11:11 or 15:51
     * @return how many times a day, digital clock shows symmetric time
     */
    static int digitalClock() {

        int count = 0;
        final int startHour = 0;
        final int endHour = 23;
        final int startMinutes = 0;
        final int endMinutes = 59;

        for (int i = startHour; i <= endHour; i++) {
            for (int j = startMinutes; j <= endMinutes; j++) {
                if (String.format("%02d", i).equals(new StringBuilder(
                        String.format("%02d", j)).reverse().toString())) {
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * Not Lucky Numbers
     * There are 999999 numbers. Method returns count amount of
     * numbers that contains <b>inputNumber</b>.
     * @param inputNumber something
     * @return amount of not lucky numbers
     */
    static int notLuckyNumbers(int inputNumber) {
        final int maxValue = 999999;

        int countOfNotLuckyNumbers = 0;
        for (int j = 0; j <= maxValue; j++) {
            if (Integer.toString(j).contains(Integer.toString(inputNumber))) {
                countOfNotLuckyNumbers++;
            }
        }
        return countOfNotLuckyNumbers;
    }
}
