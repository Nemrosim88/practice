package com.courses.se.introduction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static java.util.stream.Collectors.toList;

/**
 * This class contains methods that are solutions to section "Minimum" of task "Arrays".
 * @author Nemrosim
 * @version 1.1
 */
final class ArraysMinimum {

    /** This is private constructor. */
    private ArraysMinimum() {
    }

    /**
     * Returns ArrayList with pair numbers of an "input".
     * @param input array of integers.
     * @return ArrayList with pair numbers
     */
    static List<Integer> getPairNumbers(int[] input) {
        return Arrays.stream(input).boxed().filter(v -> v % 2 == 0).collect(toList());
    }

    /**
     * Returns ArrayList with all elements of an "input" in the reversed order.
     * @param input array of integers.
     * @return ArrayList with reversed elements
     */
    static List<Integer> getReversedArray(int[] input) {
        List<Integer> list = Arrays.stream(input).boxed().collect(toList());
        Collections.reverse(list);
        return list;
    }

    /**
     * Returns two-dimensional array[][] where array[0][x] contains first part of an "input" and
     * array[1][x] contains second parts of an "input".
     * @param input array of integers.
     * @return two-dimensional array[][]
     */
    static int[][] getFirstAndSecondPart(int[] input) {
        int[][] resultArray = new int[2][];
        resultArray[0] = Arrays.stream(input).limit(input.length / 2).toArray();
        resultArray[1] = Arrays.stream(input).skip(input.length / 2).toArray();
        return resultArray;
    }

    /**
     * Returns two-dimensional Array[][] with first(resultArray[0][x])
     * and second parts( resultArray[1][x] ) of the one-dimensional "input" in the reversed order.
     * @param input array of integers.
     * @return two-dimensional Array[][]
     */
    static int[][] getFirstAndSecondPartsReversed(int[] input) {
        int[][] resultArray = getFirstAndSecondPart(input);
        resultArray[0] = getReversedArray(resultArray[0]).stream().mapToInt(Integer::intValue).toArray();
        resultArray[1] = getReversedArray(resultArray[1]).stream().mapToInt(Integer::intValue).toArray();
        return resultArray;
    }

    /**
     * Returns the sum of all elements in the "input".
     * @param input array of integers.
     * @return sum of all elements in the "input"
     */
    static int getSumOfAllElements(int[] input) {
        return Arrays.stream(input).boxed().reduce((f, s) -> f + s).orElse(0);
    }

    /**
     * Returns average value of all numbers in the "input".
     * @param input array of integers.
     * @return average value of all numbers in the "input"
     */
    static double getAverageNumber(int[] input) {
        return Arrays.stream(input).boxed().mapToInt(Integer::intValue).average().orElse(0);
    }

    /**
     * Returns minimal value of the "input".
     * @param input array of integers.
     * @return minimal value of the array
     */
    static int getMinOfArray(int[] input) {
        return Arrays.stream(input).reduce(Integer::min).orElse(0);
    }

    /**
     * Returns maximal value of the "input".
     * @param input array of integers.
     * @return maximal value of the array
     */
    static int getMaxOfArray(int[] input) {
        return Arrays.stream(input).reduce(Integer::max).orElse(0);
    }

    /**
     * Changes all negative numbers in the "input" to zero.
     * @param input array of integers.
     */
    static int[] setNegativeNumbersToZero(int[] input) {
        return Arrays.stream(input).map(v -> {
            if (v < 0) {
                return 0;
            }
            return v;
        }).toArray();
    }

    /**
     * Returns amount of number that equal to each other in the "input".
     * @param input array of integers.
     * @return amount of number that equal to each other in the "input"
     */
    static int getAmountOfEqualNumbers(int[] input) {
        int amount = 0;
        int value;
        for (int j = 0; j < input.length - 1; j++) {
            value = input[j];
            for (int k = j + 1; k < input.length; k++) {
                if (value == input[k]) {
                    amount++;
                }
            }
        }
        return amount;
    }

    /**
     * Changes places of minimal and maximal values in the array.
     * @param array array of integers.
     */
    static void reverseMinAndMax(int[] array) {
        int min = getMinOfArray(array);
        int max = getMaxOfArray(array);
        for (int j = 0; j < array.length; j++) {
            if (array[j] == min) {
                array[j] = max;
                continue;
            }
            if (array[j] == max) {
                array[j] = min;
            }
        }
    }

    /**
     * Reverses values of "input".
     * @param input array of integers.
     * @return reversed array
     */
    static int[] reverseArray(int[] input) {
        List<Integer> result = Arrays.stream(input).boxed().collect(toList());
        Collections.reverse(result);
        return result.stream().mapToInt(Integer::intValue).toArray();
    }

    /**
     * Returns that part of the "input" that has biggest average value of numbers.
     * @param input array of integers.
     * @return ArrayList with part values
     */
    static List<Integer> getPartWithBiggestAverage(int[] input) {
        final int halfLength = input.length / 2;
        double firstAverage = Arrays.stream(input)
                .boxed().limit(halfLength).mapToInt(Integer::intValue).average().orElse(0);
        double secondAverage = Arrays.stream(input)
                .boxed().skip(halfLength).mapToInt(Integer::intValue).average().orElse(0);
        if (firstAverage > secondAverage) {
            return Arrays.stream(input).limit(halfLength).boxed().collect(toList());
        } else if (secondAverage > firstAverage) {
            return Arrays.stream(input).skip(halfLength).boxed().collect(toList());
        } else {
            return new ArrayList<>(0);
        }
    }

    /**
     * Returns ArrayList with indexes of positive numbers and
     * in the end of ArrayList sum of negative ones.
     * @param input array of integers.
     * @return ArrayList with indexes of positive numbers and sum of negative ones
     */
    static ArrayList<Integer> getIndexesAndSum(int[] input) {
        ArrayList<Integer> resultList = new ArrayList<>();
        int sumOfNegativeValues = 0;
        for (int j = 0; j < input.length; j++) {
            if (input[j] > 0) {
                resultList.add(j);
            } else {
                sumOfNegativeValues += input[j];
            }
        }
        resultList.add(sumOfNegativeValues);
        return resultList;
    }

    /**
     * Returns ArrayList that contains values from
     * the beginning of an "input" to the firs Zero-value.
     * @param input array of integers.
     * @return ArrayList
     */
    static ArrayList<Integer> getNumbersToZeroValue(int[] input) {
        ArrayList<Integer> resultArray = new ArrayList<>();
        for (int j = 0; j < input.length; j++) {
            if (input[j] == 0) {
                for (int i = 0; i < j; i++) {
                    resultArray.add(input[i]);
                }
                j = input.length;
            }
        }
        return resultArray;
    }

    /**
     * Changes places of maximal and after-maximal values of "input".
     * @param input array of integers.
     */
    static void changePlacesMaxAndSecondMax(int[] input) {
        int firstMax = Integer.MIN_VALUE;
        int firstMaxIndex = 0;
        for (int j = 0; j < input.length; j++) {
            if (input[j] > firstMax) {
                firstMax = input[j];
                firstMaxIndex = j;
            }
        }

        int secondMax = Integer.MIN_VALUE;
        int secondMaxIndex = 0;

        for (int j = 0; j < input.length; j++) {
            if (j == firstMaxIndex) {
                continue;
            }
            if (input[j] > secondMax) {
                secondMax = input[j];
                secondMaxIndex = j;
            }
        }
        input[firstMaxIndex] = secondMax;
        input[secondMaxIndex] = firstMax;
    }

    /**
     * Returns ArrayList that contains values from
     * the beginning of an "input" to the minimal value.
     * @param input array of integers.
     * @return ArrayList that contains values from the beginning to the minimal value
     */
    static ArrayList<Integer> getNumbersToMinValue(int[] input) {
        ArrayList<Integer> resultList = new ArrayList<>();
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        for (int j = 0; j < input.length; j++) {
            if (input[j] < min) {
                min = input[j];
                minIndex = j;
            }
        }
        for (int j = 0; j < minIndex; j++) {
            resultList.add(input[j]);
        }
        return resultList;
    }
}
