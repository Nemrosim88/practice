package com.courses.se.introduction;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * This class contains methods that are solutions to section "middle" of task "Cycles. Course for beginners".
 * @author Nemrosim
 * @version 1.1
 */
final class CyclesMiddle {

    /** Private constructor. */
    private CyclesMiddle() {
    }

    /**
     * Method returns all positive divider of natural number.
     * @param naturalNumber natural number
     * @return all positive divider of natural number
     */
    static ArrayList<Integer> positiveDividers(int naturalNumber) {
        ArrayList<Integer> resultList = new ArrayList<>();
        for (int j = 1; j < naturalNumber + 1; j++) {
            if (naturalNumber % j == 0) {
                resultList.add(j);
            }
        }
        return resultList;
    }

    /**
     * Method returns common dividers for two natural numbers "first" and "second".
     * @param first  first value
     * @param second second value
     * @return common dividers for two natural numbers "first" and "second"
     */
    static ArrayList<Integer> commonDividers(int first, int second) {
        ArrayList<Integer> result = new ArrayList<>();
        positiveDividers(first).forEach(integer -> result.addAll(positiveDividers(second).stream()
                .filter(integer::equals).map(secondValueDivider -> integer)
                .collect(Collectors.toList())));
        return result;
    }
}
