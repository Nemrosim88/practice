package com.courses.se.introduction;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class contains methods that are solutions to section "Hard" of task "Arrays".
 * @author Nemrosim
 * @version 1.1
 */
final class ArraysHard {

    /** Private constructor. */
    private ArraysHard() {
    }

    /**
     * Returns sum of elements which are located between minimal and maximal values of "inputArray".
     * @param input array of integers.
     * @return sum of elements which are between minimal and maximal values of "inputArray"
     */
    static int getSumOfValuesBetweenMinAdnMax(int[] input) {
        int minIndex = 0;
        int maxIndex = 0;
        for (int i = 0; i < input.length; i++) {
            if (input[i] == ArraysMinimum.getMinOfArray(input)) {
                minIndex = i;
            } else if (input[i] == ArraysMinimum.getMaxOfArray(input)) {
                maxIndex = i;
            }
        }

        if (minIndex < maxIndex) {
            return Arrays.stream(Arrays.copyOfRange(input, minIndex, maxIndex + 1))
                    .boxed().mapToInt(Integer::intValue).sum();
        } else {
            return Arrays.stream(Arrays.copyOfRange(input, maxIndex, minIndex + 1))
                    .boxed().mapToInt(Integer::intValue).sum();
        }
    }

    /**
     * Returns that half of an "input" that contains minimal value.
     * @param input array of integers.
     * @return ArrayList with half of an "input" that contains minimal value
     */
    static Integer[] getHalfOfAnArray(int[] input) {
        int min = Integer.MAX_VALUE;
        int minIndex = 0;
        for (int j = 0; j < input.length; j++) {
            if (input[j] < min) {
                min = input[j];
                minIndex = j;
            }
        }
        ArrayList<Integer> resultList = new ArrayList<>();
        int half = input.length / 2;
        if (minIndex > half) {
            for (int j = half; j < input.length; j++) {
                resultList.add(input[j]);
            }
        } else {
            for (int j = 0; j < half; j++) {
                resultList.add(input[j]);
            }
        }

        Integer[] result = new Integer[resultList.size()];
        result = resultList.toArray(result);
        return result;
    }
}
