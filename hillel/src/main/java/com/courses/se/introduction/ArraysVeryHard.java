package com.courses.se.introduction;

/**
 * This class contains methods that are solutions to section "Vary Hard" of task "Arrays".
 * @author Nemrosim
 * @version 1.1
 */
final class ArraysVeryHard {

    /** Private constructor. */
    private ArraysVeryHard() {
    }

    /**
     * Moves values [from, to] of "inputArray" to the end of an array.
     * @param array array of integers.
     * @param from  from where
     * @param to    to what point
     */
    static void moveToTheEnd(int[] array, int from, int to) {

        int newMassSizeOne = (to - from) + 1;
        int newMassSizeTwo = array.length - to;

        int[] newMassOne = new int[newMassSizeOne];
        int[] newMassTwo = new int[newMassSizeTwo];

        int startOne = 0;
        int startTwo = 0;

        for (int j = from - 1; j < to; j++) {
            newMassOne[startOne++] = array[j];
        }

        for (int j = to; j < array.length; j++) {
            newMassTwo[startTwo++] = array[j];
        }

        int firstStart = from - 1;
        for (int newMassTwoEach : newMassTwo) {
            array[firstStart++] = newMassTwoEach;
        }

        int secondStart = (from - 1) + newMassTwo.length;
        for (int newMassTwoEach : newMassOne) {
            array[secondStart++] = newMassTwoEach;
        }
    }
}
