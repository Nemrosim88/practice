package com.courses.se.inner.model;

/**
 * Movie class contains String movieName value and inner class
 * that contains additional information about the Movie:
 * 1. Runtime of the movie.
 * 2. Genre of the movie.
 * 3. Director of the movie.
 * @author Nemrosim
 * @version 1.1
 */
public class Movie {

    /** Reference to object MovieAdditionalInfo containing additional movie info. */
    private MovieAdditionalInfo movieAdditionalInfo;

    /** Name of a movie. */
    private String movieName;

    /**
     * Constructor for Movie class.
     * @param name     name of a movie
     * @param runtime  runtime of a Movie
     * @param director director of a Movie
     * @param genre    genre of a Movie
     */
    public Movie(String name, int runtime, String director, String genre) {
        this.movieAdditionalInfo = new MovieAdditionalInfo(name, runtime, director, genre);
    }

    /**
     * MovieAdditionalInfo inner class contains additional information about the Movie:
     * 1. Runtime of a Movie;
     * 2. Genre of a Movie;
     * 3. Director of a Movie.
     */
    private final class MovieAdditionalInfo {

        /** Runtime of the movie (in minutes). */
        private int runtime;

        /** Genre of the movie. */
        private String genre;

        /** Director of the movie. */
        private String director;

        /**
         * Constructor for MovieAdditionalInfo class.
         * @param name     name of a Movie
         * @param runtime  runtime of a Movie
         * @param director director of a Movie
         * @param genre    genre of a Movie
         */
        private MovieAdditionalInfo(String name, int runtime, String director, String genre) {
            movieName = name;
            this.runtime = runtime;
            this.genre = genre;
            this.director = director;
        }

        /**
         * !!This is for PMD checker (because of error: Should be a _static_ inner class? ).
         * @return movie name
         */
        public String getMovieName() {
            return movieName;
        }
    }

    /**
     * Returns name of a movie.
     * @return name of a movie
     */
    public String getMovieName() {
        return movieName;
    }

    /**
     * Sets name of a movie.
     * @param movieName name of a movie
     */
    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    /**
     * Returns reference of MovieAdditionalInfo object variable.
     * @return reference of MovieAdditionalInfo object
     */
    public MovieAdditionalInfo getMovieAdditionalInfo() {
        return movieAdditionalInfo;
    }

    /**
     * Sets MovieAdditionalInfo reference of a Movie class to movieAdditionalInfo parameter.
     * @param movieAdditionalInfo movieAdditionalInfo reference
     */
    public void setMovieAdditionalInfo(MovieAdditionalInfo movieAdditionalInfo) {
        this.movieAdditionalInfo = movieAdditionalInfo;
    }

    /**
     * Returns runtime of a MovieAdditionalInfo.
     * @return runtime of a MovieAdditionalInfo
     */
    public int getRuntime() {
        return movieAdditionalInfo.runtime;
    }

    /**
     * Sets runtime (in minutes) of a MovieAdditionalInfo.
     * @param runtime runtime of a MovieAdditionalInfo
     */
    public void setRuntime(int runtime) {
        movieAdditionalInfo.runtime = runtime;
    }

    /**
     * Returns genre of a MovieAdditionalInfo.
     * @return genre of a MovieAdditionalInfo
     */
    public String getGenre() {
        return movieAdditionalInfo.genre;
    }

    /**
     * Sets genre of a MovieAdditionalInfo.
     * @param genre genre of a MovieAdditionalInfo
     */
    public void setGenre(String genre) {
        movieAdditionalInfo.genre = genre;
    }

    /**
     * Returns director of a MovieAdditionalInfo.
     * @return director of a MovieAdditionalInfo
     */
    public String getDirector() {
        return movieAdditionalInfo.director;
    }

    /**
     * Sets director of a Movie.
     * @param director director of a movie
     */
    public void setDirector(String director) {
        movieAdditionalInfo.director = director;
    }

    @Override
    public String toString() {
        return String.format("|Runtime = %-3d |Director = %-16s |Genre(one) = %-6s.",
                getRuntime(), getDirector(), getGenre());
    }
}
