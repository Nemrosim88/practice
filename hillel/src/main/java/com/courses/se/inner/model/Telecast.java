package com.courses.se.inner.model;

/**
 * Telecast class contains  inner class that contains information about:
 * 1. TV channel name of the telecast.
 * 2. TV program name.
 * @author Nemrosim
 * @version 1.1
 */
public class Telecast {

    /** Variable for reference to inner TelecastInfo object. */
    private TelecastInfo telecastInfo;

    /**
     * Constructor for Telecast class.
     * @param channelName name of a TV channel
     * @param programName name of a program
     */
    public Telecast(String channelName, String programName) {
        this.telecastInfo = new TelecastInfo(channelName, programName);
    }

    /** TelecastInfo inner class contains information about Telecast. */
    private final class TelecastInfo {

        /** Name of a TV chanel. */
        private String channelName;

        /** Name of a program. */
        private String programName;

        /**
         * Constructor for TelecastInfo class.
         * @param channelName name of a TV channel
         * @param programName name of a program
         */
        private TelecastInfo(String channelName, String programName) {
            this.channelName = channelName;
            this.programName = programName;
        }

        /**
         * !!This is for PMD checker (because of error: Should be a _static_ inner class?).
         * @return telecastInfo
         */
        public TelecastInfo getReference() {
            return telecastInfo;
        }
    }

    /**
     * Returns reference of inner TelecastInfo class.
     * @return reference of inner TelecastInfo class
     */
    public TelecastInfo getTelecastInfo() {
        return telecastInfo;
    }

    /**
     * Sets to variable telecastInfo reference of telecastInfo parameter.
     * @param telecastInfo TelecastInfo object
     */
    public void setTelecastInfo(TelecastInfo telecastInfo) {
        this.telecastInfo = telecastInfo;
    }

    /**
     * Returns name of a TV chanel.
     * @return name of a TV chanel
     */
    public String getChannelName() {
        return telecastInfo.channelName;
    }

    /**
     * Sets name of a TV chanel.
     * @param channelName name of a TV chanel
     */
    public void setChannelName(String channelName) {
        telecastInfo.channelName = channelName;
    }

    /**
     * Returns name of a TV program.
     * @return TV program name.
     */
    public String getProgramName() {
        return telecastInfo.programName;
    }

    /**
     * Sets name of a TV program.
     * @param programName TV program name
     */
    public void setProgramName(String programName) {
        telecastInfo.programName = programName;
    }

    @Override
    public String toString() {
        return "Telecast{" + "telecastInfo=" + telecastInfo + '}';
    }
}
