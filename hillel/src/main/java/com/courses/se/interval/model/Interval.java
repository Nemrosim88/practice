package com.courses.se.interval.model;

/**
 * Class represents Intervals like [a,b] or (a,b) or [a,b) or (a,b].
 * @author Nemrosim
 * @version 1.1
 */
public class Interval {

    /** Variable A of IntervalA. */
    private int variableA;

    /** Is this end of interval open. */
    private boolean isOpenLeft;

    /** Variable A of IntervalB. */
    private int variableB;

    /** Is this end of interval open. */
    private boolean isOpenRight;

    /**
     * Constructor for Interval.
     * @param variableA   left end of interval [A,B]
     * @param isOpenLeft  is this end open [A,B] or (A,B]
     * @param variableB   right end of interval [A,B]
     * @param isOpenRight is this end open [A,B) or [A,B)
     */
    public Interval(int variableA, boolean isOpenLeft, int variableB, boolean isOpenRight) {
        this.variableA = variableA;
        this.variableB = variableB;
        this.isOpenLeft = isOpenLeft;
        this.isOpenRight = isOpenRight;
    }

    /**
     * Returns variable A.
     * @return variable A
     */
    public int getVariableA() {
        return variableA;
    }

    /**
     * Sets variable A.
     * @param variableA variable A
     */
    public void setVariableA(int variableA) {
        this.variableA = variableA;
    }

    /**
     * Returns variable B.
     * @return variable B
     */
    public int getVariableB() {
        return variableB;
    }

    /**
     * Sets variable B.
     * @param variableB variable B
     */
    public void setVariableB(int variableB) {
        this.variableB = variableB;
    }

    /**
     * Sets variable B.
     * @return end of interval opened or closed
     */
    public boolean getOpenLeft() {
        return isOpenLeft;
    }

    /**
     * Left end of interval open or closed.
     * @param openLeft left end of interval
     */
    public void setOpenLeft(boolean openLeft) {
        isOpenLeft = openLeft;
    }

    /**
     * Returns boolean - end of interval opened or closed.
     * @return end of interval opened or closed
     */
    public boolean getOpenRight() {
        return isOpenRight;
    }

    /**
     * Right end of interval open or closed.
     * @param openRight right end of interval
     */
    public void setOpenRight(boolean openRight) {
        isOpenRight = openRight;
    }


    @Override
    public String toString() {
        return "Interval{"
                + "A=" + variableA
                + ", getOpenLeft=" + isOpenLeft
                + ", B=" + variableB
                + ", getOpenRight=" + isOpenRight
                + '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Interval interval = (Interval) o;
        return variableA == interval.variableA
                && isOpenLeft == interval.isOpenLeft
                && variableB == interval.variableB
                && isOpenRight == interval.isOpenRight;
    }

    @Override
    public int hashCode() {
        int result = variableA;
        int firstVariable = 0;
        int secondVariable = 0;
        if (isOpenLeft) {
            firstVariable = 1;
        }
        if (isOpenRight) {
            secondVariable = 1;
        }
        final int hash = 31;
        result = hash * result + firstVariable;
        result = hash * result + variableB;
        result = hash * result + secondVariable;
        return result;
    }
}
