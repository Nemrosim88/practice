package com.courses.se.interval;

import com.courses.se.interval.model.Interval;
import com.courses.se.student.RandomGenerator;


/**
 * Intervals methods:
 * 1. Check for crossing of two intervals (isIntervalsCrossing);
 * 2. Merges two intervals (mergeTwoIntervals);
 * 2. Random generation of Intervals (getArrayOfRandomIntervals);
 * 3. Gets max distance between Intervals (getDistanceBetweenTheEnds).
 * @author Nemrosim
 * @version 1.1
 */
final class IntervalMethods {

    /** Private method. */
    private IntervalMethods() {
    }

    /**
     * Checking for crossing of two intervals.
     * @param first  first Interval
     * @param second second Interval
     * @return these two intervals crossing or not
     */
    static boolean isIntervalsCrossing(Interval first, Interval second) {
        if (first.getVariableA() < second.getVariableA()) {
            return first.getVariableB() > second.getVariableA()
                    || first.getVariableB() == second.getVariableA()
                    && !first.getOpenRight()
                    && !second.getOpenLeft();
        } else if (second.getVariableA() < first.getVariableA()) {
            return second.getVariableB() > first.getVariableA()
                    || second.getVariableB() == first.getVariableA()
                    && !second.getOpenRight()
                    && !first.getOpenLeft();
        } else {
            boolean isAnyLeftClosed = !first.getOpenLeft() || !second.getOpenLeft();
            boolean isBothLOpen = first.getOpenLeft() && second.getOpenLeft();
            boolean isAnyRightClosed = !first.getOpenRight() || !second.getOpenRight();
            boolean isBothROpen = first.getOpenRight() && second.getOpenRight();
            return isAnyLeftClosed || isBothLOpen || isAnyRightClosed || isBothROpen;
        }
    }

    /**
     * Method merges two intervals into one.
     * @param first  first interval to merge
     * @param second second interval to merge
     * @return Merged Interval. If two intervals couldn't be merged method will return new Interval(0,true,0,true)
     */
    static Interval mergeTwoIntervals(Interval first, Interval second) {
        if (first.equals(second)) {
            return first;
        } else if (!isIntervalsCrossing(first, second)) {
            return null;
        }
        int variableA;
        int variableB;
        boolean isOpenLeft;
        boolean isOpenRight;

        if (first.getVariableA() == second.getVariableA()) {
            variableA = first.getVariableA();
            isOpenLeft = first.getOpenLeft() && second.getOpenLeft();
            if (first.getVariableB() == second.getVariableB()) {
                variableB = second.getVariableB();
                isOpenRight = first.getOpenRight() && second.getOpenRight();
            } else if (first.getVariableB() > second.getVariableB()) {
                variableB = first.getVariableB();
                isOpenRight = first.getOpenRight();
            } else {
                variableB = second.getVariableB();
                isOpenRight = second.getOpenRight();
            }

        } else if (first.getVariableA() < second.getVariableA()) {
            variableA = first.getVariableA();
            isOpenLeft = first.getOpenLeft();
            if (first.getVariableB() == second.getVariableA()) {
                variableB = second.getVariableB();
                isOpenRight = second.getOpenRight();
            } else if (first.getVariableB() == second.getVariableB()) {
                variableB = second.getVariableB();
                isOpenRight = first.getOpenRight() && second.getOpenRight();
            } else if (first.getVariableB() > second.getVariableB()) {
                variableB = first.getVariableB();
                isOpenRight = first.getOpenRight();
            } else {
                variableB = second.getVariableB();
                isOpenRight = second.getOpenRight();
            }

        } else {
            variableA = second.getVariableA();
            isOpenLeft = second.getOpenLeft();
            if (first.getVariableB() == second.getVariableB()) {
                variableB = second.getVariableB();
                isOpenRight = first.getOpenRight() && second.getOpenRight();
            } else if (first.getVariableB() > second.getVariableB()) {
                variableB = first.getVariableB();
                isOpenRight = first.getOpenRight();
            } else {
                variableB = second.getVariableB();
                isOpenRight = second.getOpenRight();
            }
        }
        return new Interval(variableA, isOpenLeft, variableB, isOpenRight);
    }

    /**
     * Returns an array[amountOf].length with randomly generated intervals.
     * @param amountOf amountOf intervals to generate
     * @return amountOf intervals from -1_000 to 1_000 values. Ex: [-926, 491]
     */
    static Interval[] getArrayOfRandomIntervals(int amountOf) {
        Interval[] arrayOfIntervals = new Interval[amountOf];
        final int oneThousand = 1_000;
        boolean isOpenLeft;
        boolean isOpenRight;
        for (int i = 0; i < arrayOfIntervals.length; i++) {
            isOpenLeft = RandomGenerator.getRandomInt(0, 1) == 1;
            isOpenRight = RandomGenerator.getRandomInt(0, 1) == 1;
            arrayOfIntervals[i] = new Interval(
                    RandomGenerator.getRandomInt(-oneThousand, oneThousand),
                    isOpenLeft,
                    RandomGenerator.getRandomInt(-oneThousand, oneThousand),
                    isOpenRight);
        }
        return arrayOfIntervals;
    }

    /**
     * Returns Interval that represents maximal distance between min interval end
     * and max interval end of all Intervals in array.
     * @param intervals array of Intervals
     * @return Interval [min, max]
     */
    static Interval getDistanceBetweenTheEnds(Interval[] intervals) {

        int leftEnd = Integer.MAX_VALUE;
        boolean isOpenLeft = false;
        int rightEnd = Integer.MIN_VALUE;
        boolean isOpenRight = false;

        for (Interval arrayOfInterval : intervals) {
            if (arrayOfInterval.getVariableA() < leftEnd) {

                leftEnd = arrayOfInterval.getVariableA();
                isOpenLeft = arrayOfInterval.getOpenLeft();

            } else if (arrayOfInterval.getVariableB() > rightEnd) {

                rightEnd = arrayOfInterval.getVariableB();
                isOpenRight = arrayOfInterval.getOpenRight();
            }
        }
        return new Interval(leftEnd, isOpenLeft, rightEnd, isOpenRight);
    }
}
