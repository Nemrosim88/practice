package com.courses.se.advanced.airline.model;

/**
 * Large type of airplane.
 * @author Nemrosim
 * @version 1.1
 */
public class LargeAirplane extends Airliner {

    /**
     * LargeAirplane constructor.
     * @param manufacture          manufacture of the large Airplane
     * @param model                model of the large Airplane
     * @param serialNumber         serialNumber of the large Airplane
     * @param flightRange          flightRange of the large Airplane
     * @param fuelConsumption      fuelConsumption of the large Airplane
     * @param maximumLiftingWeight maximumLiftingWeight of the large Airplane
     * @param passengerCapacity    passengerCapacity of the large Airplane
     */
    public LargeAirplane(String manufacture,
                         String model,
                         int serialNumber,
                         int flightRange,
                         int fuelConsumption,
                         int maximumLiftingWeight,
                         int passengerCapacity) {
        super(manufacture,
                model,
                serialNumber,
                flightRange,
                fuelConsumption,
                maximumLiftingWeight,
                passengerCapacity);
    }
}
