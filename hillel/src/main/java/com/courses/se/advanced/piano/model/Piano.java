package com.courses.se.advanced.piano.model;

/**
 * Piano class. Solution of MIN-81 task.
 * @author Nemrosim
 * @version 1.1
 */
public class Piano {

    /** Array of Key. */
    private Key[] key = getTunedKeys();

    /** First pedal of piano. */
    private PianoPedal firstPianoPedal;

    /** Second pedal of piano. */
    private PianoPedal secondPianoPedal;

    /** Third pedal of piano. */
    private PianoPedal thirdPianoPedal;

    /**
     * Gets cloned array of Keys.
     * @return array of keys
     */
    public Key[] getKey() {
        return key.clone();
    }

    /**
     * Sets cloned array of Keys.
     * @param key array of keys
     */
    public void setKey(Key[] key) {
        this.key = key.clone();
    }

    /**
     * Gets first pedal of piano.
     * @return first pedal of piano
     */
    public PianoPedal getFirstPianoPedal() {
        return firstPianoPedal;
    }

    /**
     * Sets first pedal of piano.
     * @param firstPianoPedal first pedal of piano
     */
    public void setFirstPianoPedal(PianoPedal firstPianoPedal) {
        this.firstPianoPedal = firstPianoPedal;
    }

    /**
     * Gets second pedal of piano.
     * @return second pedal of piano
     */
    public PianoPedal getSecondPianoPedal() {
        return secondPianoPedal;
    }

    /**
     * Sets second pedal of piano.
     * @param secondPianoPedal second pedal of piano
     */
    public void setSecondPianoPedal(PianoPedal secondPianoPedal) {
        this.secondPianoPedal = secondPianoPedal;
    }

    /**
     * Gets third pedal of piano.
     * @return third pedal of piano
     */
    public PianoPedal getThirdPianoPedal() {
        return thirdPianoPedal;
    }

    /**
     * Sets third pedal of piano.
     * @param thirdPianoPedal third pedal of piano
     */
    public void setThirdPianoPedal(PianoPedal thirdPianoPedal) {
        this.thirdPianoPedal = thirdPianoPedal;
    }

    /**
     * Private static method. Returns Key array of all tuned up keys.
     * @return key[88] of tuned keys
     */
    public static Key[] getTunedKeys() {
        Key.Note[] notes = Key.Note.values();
        Key.Note note;
        final int amountOfPianoKeys = 88;
        Key[] tunedKeys = new Key[amountOfPianoKeys];
        final int firstOctaveStart = 3;
        final int firstOctaveEnd = 14;
        final int secondOctaveStart = 15;
        final int secondOctaveEnd = 26;
        final int thirdOctaveStart = 27;
        final int thirdOctaveEnd = 38;
        final int fourthOctaveStart = 39;
        final int fourthOctaveEnd = 50;
        final int sixthOctaveStart = 51;
        final int sixthOctaveEnd = 62;
        final int seventhOctaveStart = 63;
        final int seventhOctaveEnd = 74;
        final int eighthOctaveStart = 75;
        final int eighthOctaveEnd = 86;
        final int lastValue = 87;
        final int neededValue = 9;

        for (int i = 0; i < tunedKeys.length; i++) {
            if (i <= 2) {
                note = notes[i + neededValue];
                tunedKeys[i] = new Key(note, Key.Octave.SUB_CONTRA);
            } else if (i <= firstOctaveEnd) {
                note = notes[i - firstOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.CONTRA);
            } else if (i <= secondOctaveEnd) {
                note = notes[i - secondOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.GREAT);
            } else if (i <= thirdOctaveEnd) {
                note = notes[i - thirdOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.SMALL);
            } else if (i <= fourthOctaveEnd) {
                note = notes[i - fourthOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.ONE_LINED);
            } else if (i <= sixthOctaveEnd) {
                note = notes[i - sixthOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.TWO_LINED);
            } else if (i <= seventhOctaveEnd) {
                note = notes[i - seventhOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.THREE_LINED);
            } else if (i <= eighthOctaveEnd) {
                note = notes[i - eighthOctaveStart];
                tunedKeys[i] = new Key(note, Key.Octave.FOUR_LINED);
            } else {
                note = notes[i - lastValue];
                tunedKeys[i] = new Key(note, Key.Octave.FIVE_LINED);
            }
        }
        return tunedKeys;
    }
}
