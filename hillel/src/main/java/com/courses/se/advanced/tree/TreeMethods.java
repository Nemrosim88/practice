package com.courses.se.advanced.tree;

import com.courses.se.advanced.tree.model.Branch;
import com.courses.se.advanced.tree.model.Leaf;
import com.courses.se.advanced.tree.model.Tree;

/**
 * TreeMethods class contains methods that operates Tree class.
 * @author Nemrosim
 * @version 1.1
 */
public class TreeMethods {

    /** Reference to the tree. */
    private Tree tree;

    /**
     * Tree methods constructor.
     * @param tree reference to the Tree object
     */
    public TreeMethods(Tree tree) {
        this.tree = tree;
    }

    /** To blossom. */
    public void toBlossom() {
        Leaf[][] leaves = new Leaf[tree.getBranches().length][];
        for (int i = 0; i < tree.getBranches().length; i++) {
            leaves[i] = new Leaf[tree.getBranches()[i].getLeaves().length];
            for (int j = 0; j < tree.getBranches()[i].getLeaves().length; j++) {
                leaves[i][j] = new Leaf(false, Leaf.Color.GREEN);
            }
        }
        Branch[] branches = new Branch[leaves.length];
        for (int i = 0; i < branches.length; i++) {
            branches[i] = new Branch(leaves[i]);
        }

        tree.setBranches(branches);
    }

    /** All leaves fall in. */
    public void fallIn() {
        for (int i = 0; i < tree.getBranches().length; i++) {
            for (int j = 0; j < tree.getBranches()[i].getLeaves().length; j++) {
                tree.getBranches()[i].setLeaves(new Leaf[0]);
            }
        }
    }

    /** Sets frosted to TRUE. */
    public void frosted() {
        tree.setFrost(true);
    }

    /** All leaves become YELLOW. */
    public void growYellow() {

        Leaf[][] leaves = new Leaf[tree.getBranches().length][];
        for (int i = 0; i < tree.getBranches().length; i++) {
            leaves[i] = new Leaf[tree.getBranches()[i].getLeaves().length];
            for (int j = 0; j < tree.getBranches()[i].getLeaves().length; j++) {
                leaves[i][j] = new Leaf(tree.getBranches()[i].getLeaves()[j].isSear(),
                        Leaf.Color.YELLOW);
            }
        }
        Branch[] branches = new Branch[leaves.length];
        for (int i = 0; i < branches.length; i++) {
            branches[i] = new Branch(leaves[i]);
        }

        tree.setBranches(branches);
    }
}
