package com.courses.se.advanced.piano.model;

/** PianoPedal class. */
public class PianoPedal {

    /** Is pedal pressed or not. */
    private boolean isPressed;

    /** PianoPedal class constructor. */
    public PianoPedal() {
        this.isPressed = false;
    }

    /**
     * Returns pedal statement (pressed or not).
     * @return pedal statement (true/false)
     */
    public boolean isPressed() {
        return isPressed;
    }

    /**
     * Sets pedal statement (pressed or not).
     * @param pressed pedal statement
     */
    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }
}
