package com.courses.se.advanced.airline;

import com.courses.se.advanced.airline.model.AirlineCompany;
import com.courses.se.advanced.airline.model.Airliner;
import com.courses.se.advanced.airline.model.Airplane;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/** AirlineCompanyMethods class. */
public class AirlineCompanyMethods {

    /** Reference to list of Airplanes in the airlineCompany. */
    private List<Airplane> airplanesList;

    /**
     * AirlineCompanyMethods constructor.
     * @param airlineCompany reference to the AirlineCompany object
     */
    public AirlineCompanyMethods(AirlineCompany airlineCompany) {
        this.airplanesList = airlineCompany.getAirplanesList();
    }

    /**
     * Gets total passengers capacity of Airliner airplanes in Airline Company.
     * @return sum of all airplanes passengers capacity
     */
    public int getTotalPassengersCapacityOfAirliners() {
        return airplanesList.stream().filter(air -> air instanceof Airliner)
                .mapToInt(air -> ((Airliner) air).getPassengerCapacity()).sum();
    }

    /**
     * Method returns int value of total weight capacity of all airplanes in airline company.
     * @return total weight capacity
     */
    public int getTotalWeightCapacity() {
        return airplanesList.stream().mapToInt(Airplane::getMaximumLiftingWeight).sum();
    }

    /**
     * Method sorts airplanes by flight distance.
     * @return sorted list of airplanes
     */
    public ArrayList<Airplane> sortByFlightDistance() {
        ArrayList<Airplane> sorted = new ArrayList<>(airplanesList);
        Collections.sort(sorted, (f, s) -> Integer.compare(f.getFlightRange(), s.getFlightRange()));
        return sorted;
    }

    /**
     * Returns array of Airplane with fuel consumption in range [from,to].
     * @param from fuel consumption. start of the range
     * @param to   fuel consumption. end of the range
     * @return array of Airplane
     */
    public List<Airplane> getAirplaneWithFuelConsumption(int from, int to) {
        return airplanesList.stream().filter(plane -> plane.getFuelConsumption() >= from
                && plane.getFuelConsumption() <= to).collect(Collectors.toList());
    }
}