package com.courses.se.advanced.tree.model;

/**
 * Tree class. Solution to MIN-80.
 * @author Nemrosim
 * @version 1.1
 */
public class Tree {

    /** Array of tree branches. */
    private Branch[] branches;

    /** Is tree frosted. */
    private boolean isFrost;

    /**
     * Tree constructor.
     * Leaf [amount of branches][amount of leaves on each branch]
     * @param leaves array of arrays of leaves
     */
    public Tree(Leaf[][] leaves) {
        this.branches = new Branch[leaves.length];
        for (int i = 0; i < branches.length; i++) {
            branches[i] = new Branch(leaves[i]);
        }
    }

    /**
     * Gets branches cloned branches.
     * @return branches
     */
    public Branch[] getBranches() {
        return branches.clone();
    }

    /**
     * Sets branches.
     * @param branches branches
     */
    public void setBranches(Branch[] branches) {
        this.branches = branches.clone();
    }

    /**
     * Is tree frosted.
     * @return is frost
     */
    public boolean isFrost() {
        return isFrost;
    }

    /**
     * Sets frost true or false.
     * @param frost frost true or false
     */
    public void setFrost(boolean frost) {
        isFrost = frost;
    }

}
