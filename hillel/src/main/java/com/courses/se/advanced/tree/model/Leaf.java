package com.courses.se.advanced.tree.model;

/**
 * Leaf class.
 * @author Nemrosim
 * @version 1.1
 */
public class Leaf {

    /** Possible colors of the leaf. */
    public enum Color {
        /** Colors. */
        YELLOW, GREEN, RED, ORANGE
    }

    /** Private variable for leaf color. */
    private Color color;

    /** Is leaf sear or not. */
    private boolean isSear;

    /**
     * Leaf constructor.
     * @param isSear    leaf sear or not
     * @param leafColor color of the leaf
     */
    public Leaf(boolean isSear, Color leafColor) {
        this.isSear = isSear;
        this.color = leafColor;
    }

    /**
     * Gets Color of the leaf.
     * @return leaf Color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Sets leaf Color.
     * @param color color of the leaf
     */
    public void setColor(Color color) {
        this.color = color;
    }

    /**
     * Returns state of the leaf. Is it sear or not?
     * @return is sear
     */
    public boolean isSear() {
        return isSear;
    }

    /**
     * Sets state of the leaf. Is it sear or not?
     * @param sear is sear
     */
    public void setSear(boolean sear) {
        isSear = sear;
    }


}
