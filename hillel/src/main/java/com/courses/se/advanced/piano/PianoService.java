package com.courses.se.advanced.piano;

import com.courses.se.advanced.piano.model.Key;
import com.courses.se.advanced.piano.model.Music;
import com.courses.se.advanced.piano.model.Piano;

/**
 * PianoService class.
 * @author Nemrosim
 * @version 1.1
 */
public class PianoService {

    /** Variable for reference to Piano class. */
    private Piano piano;

    /**
     * Default constructor for PianoService class.
     * @param piano reference to the Piano class.
     */
    public PianoService(Piano piano) {
        this.piano = piano;
    }

    /** Tunes the Piano. */
    public void tuneThePiano() {
        piano.setKey(Piano.getTunedKeys());
    }

    /**
     * Play That Funky Music, White Boy.
     * @param first  is pressed first pedal of the Piano
     * @param second is pressed second pedal of the Piano
     * @param third  is pressed third pedal of the Piano
     * @param i      number of a key of the Piano.
     * @return music
     */
    public Music playThePiano(boolean first, boolean second, boolean third, int... i) {

        if (i.length == 0 && !first && !second && !third) {
            return new Music(true); // silence is the best music
        }
        boolean isMusicNice = false;
        for (int anI : i) {
            isMusicNice = piano.getKey()[anI].getNote()
                    != Key.Note.UNTUNED && !first && !second && third;
            if (!isMusicNice) {
                return new Music(isMusicNice);
            }
        }
        return new Music(isMusicNice);
    }

    /**
     * Press one key on piano.
     * @param i any amount piano keys
     * @return good or bad music
     */
    public Music pressTheKey(int i) {
        return new Music(!piano.getKey()[i].getNote().equals(Key.Note.UNTUNED));
    }

    /**
     * Gets piano reference.
     * @return reference to Piano Object
     */
    public Piano getPiano() {
        return piano;
    }
}
