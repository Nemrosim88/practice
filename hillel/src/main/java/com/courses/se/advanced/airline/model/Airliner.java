package com.courses.se.advanced.airline.model;

/**
 * Abstract class of Airliner.
 * @author Nemrosim
 * @version 1.1
 */
public abstract class Airliner extends Airplane {

    /** Passenger capacity of Airliner. */
    private int passengerCapacity;

    /**
     * Airliner constructor.
     * @param manufacture          manufacture of the Airliner
     * @param model                model of the Airliner
     * @param serialNumber         serialNumber of the Airliner
     * @param flightRange          flightRange of the Airliner
     * @param fuelConsumption      fuelConsumption of the Airliner
     * @param maximumLiftingWeight maximumLiftingWeight of the Airliner
     * @param passengerCapacity    passengerCapacity of the Airliner
     */
    public Airliner(String manufacture,
                    String model,
                    int serialNumber,
                    int flightRange,
                    int fuelConsumption,
                    int maximumLiftingWeight,
                    int passengerCapacity) {
        super(manufacture, model, serialNumber, flightRange, fuelConsumption, maximumLiftingWeight);
        this.passengerCapacity = passengerCapacity;
    }

    /**
     * Returns passenger capacity of Airliner.
     * @return int value (passenger capacity of Airliner)
     */
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    /**
     * Sets passenger capacity of Airliner.
     * @param passengerCapacity passenger capacity of Airliner
     */
    public void setPassengerCapacity(int passengerCapacity) {
        this.passengerCapacity = passengerCapacity;
    }
}