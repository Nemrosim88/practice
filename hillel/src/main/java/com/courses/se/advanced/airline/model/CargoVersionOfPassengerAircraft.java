package com.courses.se.advanced.airline.model;

/**
 * Cargo Version Of Passenger Aircraft class.
 * @author Nemrosim
 * @version 1.1
 */
public class CargoVersionOfPassengerAircraft extends TransportAircraft {

    /**
     * Cargo Version Of Passenger Aircraft constructor.
     * @param manufacture          manufacture
     * @param model                model
     * @param serialNumber         serialNumber
     * @param flightRange          flightRange
     * @param fuelConsumption      fuelConsumption
     * @param maximumLiftingWeight maximumLiftingWeight
     */
    public CargoVersionOfPassengerAircraft(String manufacture,
                                           String model,
                                           int serialNumber,
                                           int flightRange,
                                           int fuelConsumption,
                                           int maximumLiftingWeight) {
        super(manufacture,
                model,
                serialNumber,
                flightRange,
                fuelConsumption,
                maximumLiftingWeight);
    }
}
