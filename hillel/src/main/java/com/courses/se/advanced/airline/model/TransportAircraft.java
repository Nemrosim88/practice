package com.courses.se.advanced.airline.model;

/**
 * TransportAircraft abstract class.
 * @author Nemrosim
 * @version 1.1
 */
public abstract class TransportAircraft extends Airplane {

    /**
     * Transport aircraft constructor.
     * @param manufacture          manufacture
     * @param model                model
     * @param serialNumber         serialNumber
     * @param flightRange          flightRange
     * @param fuelConsumption      fuelConsumption
     * @param maximumLiftingWeight maximumLiftingWeight
     */
    public TransportAircraft(String manufacture,
                             String model,
                             int serialNumber,
                             int flightRange,
                             int fuelConsumption,
                             int maximumLiftingWeight) {
        super(manufacture,
                model,
                serialNumber,
                flightRange,
                fuelConsumption,
                maximumLiftingWeight);
    }
}
