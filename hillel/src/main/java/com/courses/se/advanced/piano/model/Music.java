package com.courses.se.advanced.piano.model;

/**
 * Music class.
 * @author Nemrosim
 * @version 1.1
 */
public class Music {

    //No methods inside. This class was created for method "playThePiano".

    /** Is this music nice.? */
    private boolean isMusicNice;

    /**
     * Music constructor.
     * @param niceMusic true/false
     */
    public Music(boolean niceMusic) {
        this.isMusicNice = niceMusic;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Music music = (Music) obj;
        return isMusicNice == music.isMusicNice;
    }

    @Override
    public int hashCode() {
        if (isMusicNice) {
            return 1;
        } else {
            return 0;
        }
    }
}
