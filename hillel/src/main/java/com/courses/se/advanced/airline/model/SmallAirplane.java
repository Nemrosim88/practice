package com.courses.se.advanced.airline.model;

/**
 * Small type of airplane.
 * @author Nemrosim
 * @version 1.1
 */
public class SmallAirplane extends Airliner {

    /**
     * SmallAirplane constructor.
     * @param manufacture          manufacture of the SmallAirplane
     * @param model                model of the SmallAirplane
     * @param serialNumber         serialNumber of the SmallAirplane
     * @param flightRange          flightRange of the SmallAirplane
     * @param fuelConsumption      fuelConsumption of the SmallAirplane
     * @param maximumLiftingWeight maximumLiftingWeight of the SmallAirplane
     * @param passengerCapacity    passengerCapacity of the SmallAirplane
     */
    public SmallAirplane(String manufacture,
                         String model,
                         int serialNumber,
                         int flightRange,
                         int fuelConsumption,
                         int maximumLiftingWeight,
                         int passengerCapacity) {
        super(manufacture,
                model,
                serialNumber,
                flightRange,
                fuelConsumption,
                maximumLiftingWeight,
                passengerCapacity);
    }
}
