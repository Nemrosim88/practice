package com.courses.se.advanced.piano.model;

/**
 * Key class.
 * @author Nemrosim
 * @version 1.1
 */
public class Key {

    /** Enum of possible notes. */
    public enum Note {
        /** Possible notes. */
        DO, DO_DIESIS, RE, RE_DIESIS, MI, FA,
        /** Possible notes. */
        FA_DIESIS, SOL, SOL_DIESIS, LA, LA_DIESIS, SI, UNTUNED
    }

    /** Enum of possible octaves. */
    public enum Octave {
        /** Possible octaves of piano notes. */
        SUB_CONTRA, CONTRA, GREAT, SMALL, ONE_LINED,
        /** Possible octaves of piano notes. */
        TWO_LINED, THREE_LINED, FOUR_LINED, FIVE_LINED, UNTUNED
    }


    /** Note of the key. */
    private Note note;

    /** Octave of the key note. */
    private Octave octave;
    /** Variable for key color. Is key color white? */
    private boolean isWhite;

    /**
     * Key constructor.
     * @param note   note of the key
     * @param octave octave of the key note
     */
    public Key(Note note, Octave octave) {
        this.isWhite = note == Note.DO
                || note == Note.RE
                || note == Note.MI
                || note == Note.FA
                || note == Note.SOL
                || note == Note.LA
                || note == Note.SI;
        this.note = note;
        this.octave = octave;
    }

    /**
     * Gets note of the key.
     * @return note of the key
     */
    public Note getNote() {
        return note;
    }

    /**
     * Sets note of the key.
     * @param note note of the key
     */
    public void setNote(Note note) {
        this.note = note;
    }

    /**
     * Gets octave of the key note.
     * @return octave of the key note
     */
    public Octave getOctave() {
        return octave;
    }

    /**
     * Sets octave of the key note.
     * @param octave octave of the key note
     */
    public void setOctave(Octave octave) {
        this.octave = octave;
    }

    /**
     * Key color white or not (true/false)?.
     * @return white or not (true/false)
     */
    public boolean isWhite() {
        return isWhite;
    }

    /**
     * Sets color of the key. White or not (true/false).
     * @param white white or not (true/false)
     */
    public void setWhite(boolean white) {
        isWhite = white;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Key key = (Key) obj;

        return isWhite == key.isWhite
                && note == key.note
                && octave == key.octave;

    }

    @Override
    public int hashCode() {
        int result;
        if (note != null) {
            result = note.hashCode();
        } else {
            result = 0;
        }
        final int forHash = 31;
        if (octave != null) {
            result = forHash * result + octave.hashCode();
        } else {
            result = forHash * result;
        }
        if (isWhite) {
            result = forHash * result + 1;
        } else {
            result = forHash * result;
        }

        return result;
    }

    @Override
    public String toString() {
        return "{" + note + ", " + octave + ", " + isWhite + '}';
    }
}
