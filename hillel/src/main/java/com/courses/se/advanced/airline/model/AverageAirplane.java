package com.courses.se.advanced.airline.model;

/**
 * Average type of airplane.
 * @author Nemrosim
 * @version 1.1
 */
public class AverageAirplane extends Airliner {

    /**
     * AverageAirplane constructor.
     * @param manufacture          manufacture of the average Airplane
     * @param model                model of the average Airplane
     * @param serialNumber         serialNumber of the average Airplane
     * @param flightRange          flightRange of the average Airplane
     * @param fuelConsumption      fuelConsumption of the average Airplane
     * @param maximumLiftingWeight maximumLiftingWeight of the average Airplane
     * @param passengerCapacity    passengerCapacity of the average Airplane
     */
    public AverageAirplane(String manufacture,
                           String model,
                           int serialNumber,
                           int flightRange,
                           int fuelConsumption,
                           int maximumLiftingWeight,
                           int passengerCapacity) {
        super(manufacture,
                model,
                serialNumber,
                flightRange,
                fuelConsumption,
                maximumLiftingWeight,
                passengerCapacity);
    }
}
