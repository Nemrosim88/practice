package com.courses.se.advanced.airline.model;

import java.util.ArrayList;

/**
 * AirlineCompany class.
 * @author Nemrosim
 * @version 1.1
 */
public class AirlineCompany {

    /** Name of the company. */
    private String companyName;

    /** List of all airplanes in the AirlineCompany. */
    private ArrayList<Airplane> airplanesList;

    /**
     * AirlineCompany constructor.
     * @param name          name of the company
     * @param airplanesList list of airplanes
     */
    public AirlineCompany(String name, ArrayList<Airplane> airplanesList) {
        this.companyName = name;
        this.airplanesList = airplanesList;
    }

    /**
     * Gets name of the airline company.
     * @return name of the airline company (String)
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Gets name of the airline company.
     * @param companyName name of the airline company (String)
     */
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    /**
     * Gets airplanes list.
     * @return airplanes list
     */
    public ArrayList<Airplane> getAirplanesList() {
        return airplanesList;
    }

    /**
     * Sets airplanes list.
     * @param airplanesList airplanes list
     */
    public void setAirplanesList(ArrayList<Airplane> airplanesList) {
        this.airplanesList = airplanesList;
    }

}