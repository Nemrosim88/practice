package com.courses.se.advanced.airline.model;

/**
 * Abstract class of Airplane.
 * @author Nemrosim
 * @version 1.1
 */
public abstract class Airplane {

    /** Manufacture of the Airplane. */
    private String manufacture;

    /** Model of the Airplane. */
    private String model;

    /** Serial number of the Airplane. */
    private int serialNumber;

    /** Flight range of the Airplane. */
    private int flightRange;

    /** Fuel consumption of the Airplane. */
    private int fuelConsumption;

    /** Maximum lifting weight of the Airplane. */
    private int maximumLiftingWeight;

    /**
     * Airplane constructor.
     * @param manufacture          manufacture of the Airplane
     * @param model                model of the Airplane
     * @param serialNumber         serialNumber of the Airplane
     * @param flightRange          flightRange of the Airplane
     * @param fuelConsumption      fuelConsumption of the Airplane
     * @param maximumLiftingWeight maximumLiftingWeight of the Airplane
     */
    public Airplane(String manufacture,
                    String model,
                    int serialNumber,
                    int flightRange,
                    int fuelConsumption,
                    int maximumLiftingWeight) {
        this.manufacture = manufacture;
        this.model = model;
        this.serialNumber = serialNumber;
        this.flightRange = flightRange;
        this.fuelConsumption = fuelConsumption;
        this.maximumLiftingWeight = maximumLiftingWeight;
    }

    /**
     * Gets manufacture of the Airplane.
     * @return manufacture of the Airplane
     */
    public String getManufacture() {
        return manufacture;
    }

    /**
     * Sets manufacture of the Airplane.
     * @param manufacture manufacture of the Airplane
     */
    public void setManufacture(String manufacture) {
        this.manufacture = manufacture;
    }

    /**
     * Gets model of the Airplane.
     * @return model of the Airplane
     */
    public String getModel() {
        return model;
    }

    /**
     * Sets model of the Airplane.
     * @param model model of the Airplane
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * Gets serial number of the Airplane.
     * @return serial number of the Airplane
     */
    public int getSerialNumber() {
        return serialNumber;
    }

    /**
     * Sets serial number of the Airplane.
     * @param serialNumber serial number of the Airplane
     */
    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    /**
     * Gets flight range of the Airplane.
     * @return flight range of the Airplane
     */
    public int getFlightRange() {
        return flightRange;
    }

    /**
     * Sets flight range of the Airplane.
     * @param flightRange flight range of the Airplane
     */
    public void setFlightRange(int flightRange) {
        this.flightRange = flightRange;
    }

    /**
     * Gets fuel consumption of the Airplane.
     * @return fuel consumption of the Airplane
     */
    public int getFuelConsumption() {
        return fuelConsumption;
    }

    /**
     * Sets fuel consumption of the Airplane.
     * @param fuelConsumption fuel consumption of the Airplane
     */
    public void setFuelConsumption(int fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    /**
     * Gets maximum lifting weight of the Airplane.
     * @return maximum lifting weight of the Airplane
     */
    public int getMaximumLiftingWeight() {
        return maximumLiftingWeight;
    }

    /**
     * Sets maximum lifting weight of the Airplane.
     * @param maximumLiftingWeight maximum lifting weight of the Airplane
     */
    public void setMaximumLiftingWeight(int maximumLiftingWeight) {
        this.maximumLiftingWeight = maximumLiftingWeight;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }

        Airplane airplane = (Airplane) object;
        return !(serialNumber != airplane.serialNumber
                || flightRange != airplane.flightRange
                || fuelConsumption != airplane.fuelConsumption
                || maximumLiftingWeight != airplane.maximumLiftingWeight
                || !manufacture.equals(airplane.manufacture))
                && model.equals(airplane.model);

    }

    @Override
    public int hashCode() {
        int result = manufacture.hashCode();
        final int forHashCode = 31;
        result = forHashCode * result + model.hashCode();
        result = forHashCode * result + serialNumber;
        result = forHashCode * result + flightRange;
        result = forHashCode * result + fuelConsumption;
        result = forHashCode * result + maximumLiftingWeight;
        return result;
    }

    @Override
    public String toString() {
        return String.format("JetType:%-16s", this.getClass().getSimpleName())
                + String.format("Manufacture: %-16s ", manufacture)
                + String.format("Model: %-18s ", model)
                + String.format("SN: %-3s ", serialNumber)
                + String.format("FlightRange: %-5s ", flightRange)
                + String.format("FuelConsumption: %-4s ", fuelConsumption)
                + String.format("maximumLiftingWeight: %-5s", maximumLiftingWeight) + "\n";
    }
}