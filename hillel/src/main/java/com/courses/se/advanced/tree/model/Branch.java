package com.courses.se.advanced.tree.model;


/**
 * Branch class.
 * @author Nemrosim
 * @version 1.1
 */
public class Branch {

    /** Array of leaves on the branch. */
    private Leaf[] leaves;

    /**
     * Branch constructor.
     * @param leaves leaves
     */
    public Branch(Leaf[] leaves) {
        this.leaves = leaves.clone();
    }

    /**
     * Gets cloned array of leaves.
     * @return cloned array of leaves
     */
    public Leaf[] getLeaves() {
        return leaves.clone();
    }

    /**
     * Sets cloned array of leaves.
     * @param leaves array of leaves
     */
    public void setLeaves(Leaf[] leaves) {
        this.leaves = leaves.clone();
    }


}
