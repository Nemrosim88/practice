package com.courses.se.advanced.airline.model;

/**
 * Turboprop type of airplane.
 * @author Nemrosim
 * @version 1.1
 */
public class Turboprop extends Airliner {

    /**
     * Turboprop constructor.
     * @param manufacture          manufacture of the LightAirplane
     * @param model                model of the LightAirplane
     * @param serialNumber         serialNumber of the LightAirplane
     * @param flightRange          flightRange of the LightAirplane
     * @param fuelConsumption      fuelConsumption of the LightAirplane
     * @param maximumLiftingWeight maximumLiftingWeight of the LightAirplane
     * @param passengerCapacity    passengerCapacity of the LightAirplane
     */
    public Turboprop(String manufacture,
                     String model,
                     int serialNumber,
                     int flightRange,
                     int fuelConsumption,
                     int maximumLiftingWeight,
                     int passengerCapacity) {
        super(manufacture,
                model,
                serialNumber,
                flightRange,
                fuelConsumption,
                maximumLiftingWeight,
                passengerCapacity);
    }
}
