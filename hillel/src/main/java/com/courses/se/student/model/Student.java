package com.courses.se.student.model;

import java.util.GregorianCalendar;

/**
 * This class is a POJO class of Students.
 * @author Nemrosim
 * @version 1.1
 */
public class Student {

    /** Variable for date of birth. */
    private GregorianCalendar birth;

    /** Variable for phone number. */
    private String phoneNumber;

    /** Variable for faculty name. */
    private String faculty;

    /** Variable for course name. */
    private String course;

    /** Variable for group name. */
    private char group;

    /** Variable for ID. */
    private int id;

    /** Variable for Name. */
    private String namePlusSecondNamePlusPatronymic;

    /** Variable for Address. */
    private String address;

    /** Default constructor. */
    public Student() {
    }

    /**
     * Constructor.
     * @param id                               id
     * @param namePlusSecondNamePlusPatronymic name namePlusSecondNamePlusPatronymic
     * @param address                          address
     * @param birth                            date of birth
     * @param course                           course name
     * @param faculty                          faculty name
     * @param group                            group name
     * @param phoneNumber                      phone number
     */
    public Student(int id,
                   // it was made for eight parameters in constructor
                   String namePlusSecondNamePlusPatronymic,
                   String address,
                   GregorianCalendar birth,
                   String phoneNumber,
                   String faculty,
                   String course,
                   char group) {
        this.id = id;
        this.namePlusSecondNamePlusPatronymic = namePlusSecondNamePlusPatronymic;
        this.address = address;
        this.birth = birth;
        this.phoneNumber = phoneNumber;
        this.faculty = faculty;
        this.course = course;
        this.group = group;
    }

    /**
     * Gets ID of the student.
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets ID of the student.
     * @param id id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets name of the student.
     * @return name
     */
    public String getName() {
        return namePlusSecondNamePlusPatronymic;
    }

    /**
     * Sets name of the student.
     * @param namePlusSecondNamePlusPatronymic namePlusSecondNamePlusPatronymic
     */
    public void setName(String namePlusSecondNamePlusPatronymic) {
        this.namePlusSecondNamePlusPatronymic = namePlusSecondNamePlusPatronymic;
    }

    /**
     * Gets the address of the student.
     * @return address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets the address of the student.
     * @param address address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets the birth date of the student.
     * @return date of birth
     */
    public final GregorianCalendar getBirth() {
        return birth;
    }

    /**
     * Sets the birth date of the student.
     * @param birthDate birth date
     */
    public final void setBirth(GregorianCalendar birthDate) {
        this.birth = birthDate;
    }

    /**
     * Gets the phone number of the student.
     * @return phone number
     */
    public final String getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * Sets the phone number of the student.
     * @param phoneNumber phone number
     */
    public final void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * Gets the Faculty of the student.
     * @return Faculty
     */
    public final String getFaculty() {
        return faculty;
    }

    /**
     * Sets the Faculty of the student.
     * @param faculty Faculty
     */
    public final void setFaculty(final String faculty) {
        this.faculty = faculty;
    }

    /**
     * Gets the Course of the student.
     * @return Course
     */
    public final String getCourse() {
        return course;
    }

    /**
     * Sets the Course of the student.
     * @param course Course
     */
    public final void setCourse(String course) {
        this.course = course;
    }

    /**
     * Gets the Group of the student.
     * @return Group
     */
    public final char getGroup() {
        return group;
    }

    /**
     * Sets the Group of the student.
     * @param group Group
     */
    public final void setGroup(final char group) {
        this.group = group;
    }


    /**
     * Returns all information about the student.
     * @return string representation of Student
     */
    @Override
    public String toString() {
        return "Student{"
                + "birth=" + birth.getTime()
                + ", phoneNumber='" + phoneNumber + '\''
                + ", faculty='" + faculty + '\''
                + ", course='" + course + '\''
                + ", group=" + group
                + ", id=" + id
                + ", name='" + namePlusSecondNamePlusPatronymic + '\''
                + ", address='" + address + '\'' + '}' + "\n";
    }
}
