package com.courses.se.student;

import java.util.Random;

/**
 * This class contains static methods the main purpose of which is to
 * randomly generate names, second names, phone numbers, faculties etc.
 * @author Nemrosim
 * @version 1.1
 */
public final class RandomGenerator {

    /** Private constructor. */
    private RandomGenerator() {
    }

    /**
     * Generates random number [from, to].
     * @param from from
     * @param to   to
     * @return random int
     */
    public static int getRandomInt(int from, int to) {
        return new Random().nextInt(to + 1 - from) + from;
    }

    /**
     * Generates random year from 1950 to 1998.
     * @return random year
     */
    public static int getRandomYear() {
        final int yearFrom = 1950;
        final int yearTo = 1998;
        return getRandomInt(yearFrom, yearTo);
    }

    /**
     * Generates random day from 1 to 28.
     * @return random day
     */
    public static int getRandomDay() {
        final int dayFrom = 1;
        final int dayTo = 28;
        return getRandomInt(dayFrom, dayTo);
    }

    /**
     * Generates random month from 0 to 11.
     * @return random month
     */
    public static int getRandomMonth() {
        final int monthFrom = 0;
        final int monthTo = 11;
        return getRandomInt(monthFrom, monthTo);
    }

    /**
     * Generates random char from A to F.
     * @return random char
     */
    public static char getRandomChar() {
        final int charStart = 65; // this is 'A' in Unicode
        final int charEnd = 70; // this is 'F' in Unicode
        return (char) getRandomInt(charStart, charEnd);
    }

    /**
     * Generates random phoneNumber.
     * @return random phoneNumber
     */
    public static String getRandomPhoneNumber() {
        final int phoneNumberStart = 0;
        final int phoneNumberEnd = 9;
        StringBuilder phoneNumber = new StringBuilder("+38044");
        final int phoneNumberLength = 7;
        for (int i = 0; i < phoneNumberLength; i++) {
            phoneNumber.append(getRandomInt(phoneNumberStart, phoneNumberEnd));
        }
        return phoneNumber.toString();
    }

    /**
     * Generates random name.
     * @return random Name
     */
    public static String getRandomName() {
        String[] namesArray = {"Andrey", "Tony", "Victor", "Jason"};
        final int lastIndexOfArray = 3;
        return namesArray[getRandomInt(0, lastIndexOfArray)];
    }

    /**
     * Generates second Name.
     * @return random Name
     */
    public static String getRandomSecondName() {
        String[] secondNamesArray = {"Petrovich", "Viktorovich",
                "Stepanovich", "Ivanovich"};
        final int lastIndexOfArray = 3;
        return secondNamesArray[getRandomInt(0, lastIndexOfArray)];
    }

    /**
     * Generates random Name.
     * @return random Name
     */
    public  static String getRandomPatronymic() {
        String[] patronymicArray = {"Voloshin", "Prasolov",
                "Petrunchak", "Homenko"};
        final int lastIndexOfArray = 3;
        return patronymicArray[getRandomInt(0, lastIndexOfArray)];
    }


    /**
     * Generates random Street.
     * @return random street
     */
    public static String getRandomStreetName() {
        final int houseNumberStart = 1;
        final int houseNumberEnd = 32;

        final int roomNumberStart = 1;
        final int roomNumberEnd = 250;

        String[] streetNameArray = {"Shelkovichnaia", "Solomenskaia",
                "Klimenko", "Stroiteley"};
        final int lastIndexOfArray = 3;

        return String.format(
                "Ukraine, Kiev, st. %s, h.%d, room.%d",
                streetNameArray[getRandomInt(0, lastIndexOfArray)],
                getRandomInt(houseNumberStart, houseNumberEnd),
                getRandomInt(roomNumberStart, roomNumberEnd));
    }

    /**
     * Generates random Faculty.
     * @return random faculty
     */
    public static String getRandomFaculty() {
        String[] faculties = {"Faculty of Geography", "Faculty of History"};
        return faculties[getRandomInt(0, 1)];
    }

    /**
     * Generates random Course depending of The Faculty.
     * @param faculty faculty
     * @return random course depending of faculty
     */
    public static String getRandomCourse(String faculty) {

        String[][] courseArray = {
                {"Economic and social geography",
                        "Geography and tourism"},
                {"Ancient and modern history of Ukraine",
                        "Art History"}};

        final int forRandomStart = 0;
        final int forRandomEnd = 1;

        final int arrayIndexZero = 0;
        final int arrayIndexOne = 1;

        if ("Faculty of Geography".equals(faculty)) {
            return courseArray[arrayIndexZero][getRandomInt(forRandomStart,
                    forRandomEnd)];
        } else if ("Faculty of History".equals(faculty)) {
            return courseArray[arrayIndexOne][getRandomInt(forRandomStart,
                    forRandomEnd)];
        } else {
            return "Something wrong!";
        }
    }
}