package com.courses.se.student;

import com.courses.se.student.model.Student;

import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class contains methods for "Students" task.
 * @author Nemrosim
 * @version 1.1
 */
class StudentMethods {

    /**
     * Method returns new array[] of Students with "howManyStudents" length .
     * @param howManyStudents what will be the length of an array
     * @return new Student[howManyStudents]
     */
    static Student[] createNewArrayOfStudent(int howManyStudents) {

        Student[] studentsArray = new Student[howManyStudents];

        for (int index = 0; index < studentsArray.length; index++) {
            String faculty = RandomGenerator.getRandomFaculty();
            studentsArray[index] = new Student(
                    index,
                    RandomGenerator.getRandomName()
                            + " " + RandomGenerator.getRandomSecondName()
                            + " " + RandomGenerator.getRandomPatronymic(),
                    RandomGenerator.getRandomStreetName(),
                    new GregorianCalendar(RandomGenerator.getRandomYear(),
                            RandomGenerator.getRandomMonth(),
                            RandomGenerator.getRandomDay()),
                    RandomGenerator.getRandomPhoneNumber(),
                    faculty,
                    RandomGenerator.getRandomCourse(faculty),
                    RandomGenerator.getRandomChar());
        }
        return studentsArray;
    }

    /**
     * Returns Students that are studying on Faculty.
     * @param students Student[] students
     * @param faculty  faculty
     * @return array list with Students that are studying on "faculty"
     */
    List<Student> studentsSearchList(Student[] students, String faculty) {
        return Arrays.stream(students).filter(s -> s.getFaculty().equals(faculty)).collect(Collectors.toList());
    }

    /**
     * Returns Students that are studying on course of the faculty.
     * @param students array of Students
     * @param faculty  faculty
     * @param course   course
     * @return Students that are studying on course of the faculty
     */
    List<Student> studentsSearchList(Student[] students, String faculty, String course) {
        return Arrays.stream(students).filter(s -> s.getFaculty().equals(faculty) && s.getCourse().equals(course))
                .collect(Collectors.toList());
    }

    /**
     * Returns Students that were born after year parameter.
     * @param input array of Students
     * @param year  year of birth
     * @return Students that were born after "year" parameter.
     */
    List<Student> studentsSearchList(Student[] input, int year) {
        return Arrays.stream(input).filter(s -> s.getBirth()
                .get(GregorianCalendar.YEAR) > year).collect(Collectors.toList());
    }

    /**
     * Returns Students that are studying in specific group.
     * @param input array of Students
     * @param group name of the group
     * @return Students that are studying on course
     */
    final List<Student> studentsSearchList(Student[] input, char group) {
        return Arrays.stream(input).filter(s -> s.getGroup() == group).collect(Collectors.toList());
    }
}