package com.courses.se.third.model;

/**
 * This is a POJO class of "Straight On A Plane" task.
 * @author Nemrosim
 * @version 1.1
 */
public class StraightOnAPlane {

    /** variableA in formula. */
    private double variableA;

    /** variableB in formula. */
    private double variableB;

    /** variableC in formula. */
    private double variableC;

    /**
     * Constructor for class.
     * @param variableA sets variableA
     * @param variableB sets variableB
     * @param variableC sets variableC
     */
    public StraightOnAPlane(double variableA, double variableB, double variableC) {
        this.variableA = variableA;
        this.variableB = variableB;
        this.variableC = variableC;
    }

    /**
     * Gets variableA.
     * @return variableA
     */
    public double getVariableA() {
        return variableA;
    }

    /**
     * Sets variableA.
     * @param variableA sets variableA
     */
    public void setVariableA(final double variableA) {
        this.variableA = variableA;
    }

    /**
     * Gets variableB.
     * @return variableB
     */
    public double getVariableB() {
        return variableB;
    }

    /**
     * Sets variableB.
     * @param variableB sets variableB
     */
    public void setVariableB(final double variableB) {
        this.variableB = variableB;
    }

    /**
     * Gets variableC.
     * @return variableC
     */
    public double getVariableC() {
        return variableC;
    }

    /**
     * Sets variableC.
     * @param variableC sets variableC
     */
    public void setVariableC(final double variableC) {
        this.variableC = variableC;
    }

    @Override
    public String toString() {
        return "Straight{"
                + "A=" + variableA
                + ", B=" + variableB
                + ", C=" + variableC
                + '}' + "\n";
    }
}



