package com.courses.se.third;

import com.courses.se.student.RandomGenerator;
import com.courses.se.third.model.StraightOnAPlane;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Locale;


/**
 * This class has method that solves "Straight On A Plane" tasks.
 * ax + by + c = 0;
 * y = (-ax-c)/(-b);
 * y = -c/-b;
 * x = -c/a;
 * @author Nemrosim
 * @version 1.1
 */
final class StraightOnAPlaneMethods {

    /** Private constructor. */
    private StraightOnAPlaneMethods() {
    }

    /**
     * Returns double[][] straight crossing points with datum line X and Y.
     * @param straight instance of StraightOnAPlane
     * @return double[0][0] contains value of X1 crossing point
     *         double[0][1] contains value of Y1 crossing point
     *         double[1][0] contains value of X2 crossing point
     *         double[1][0] contains value of X2 crossing point
     */
    static double[][] getCrossingPoints(StraightOnAPlane straight) {
        double[][] resultArray = new double[2][2];
        double result = -straight.getVariableC() / straight.getVariableA();
        result = Double.parseDouble(String.format(Locale.ENGLISH, "%.2f", result));
        resultArray[0][0] = result;
        resultArray[0][1] = 0;
        result = (-straight.getVariableC()) / (-straight.getVariableB());
        result = Double.parseDouble(String.format(Locale.ENGLISH, "%.2f", result));
        resultArray[1][0] = 0;
        resultArray[1][1] = result;
        return resultArray;
    }


    /**
     * Returns crossing point of two straights.
     * @param firstStraight  instance of first straight
     * @param secondStraight instance of second straight
     * @return double[0] contains value of X crossing point
     *         double[1] contains value of Y crossing point
     *         double[].length = 0 if straights are parallel
     */
    static double[] getCrossingPoints(StraightOnAPlane firstStraight,
                                      StraightOnAPlane secondStraight) {
        double[] resultArray = new double[2];

        double denominator =
                firstStraight.getVariableA()
                        * secondStraight.getVariableB()
                        - secondStraight.getVariableA()
                        * firstStraight.getVariableB();
        double firstNumerator =
                firstStraight.getVariableC()
                        * secondStraight.getVariableB()
                        - secondStraight.getVariableC()
                        * firstStraight.getVariableB();
        double secondNumerator =
                firstStraight.getVariableA()
                        * secondStraight.getVariableC()
                        - secondStraight.getVariableA()
                        * firstStraight.getVariableC();

        double resultForX = -(firstNumerator) / denominator;
        double resultForY = (secondNumerator) / denominator;

        if (Double.isNaN(resultForY) || Double.isNaN(resultForX)) {
            return new double[0];
        } else {
            resultForX = Double.parseDouble(
                    String.format(Locale.ENGLISH, "%.2f", resultForX));
            resultForY = Double.parseDouble(
                    String.format(Locale.ENGLISH, "%.2f", resultForY));
            resultArray[0] = resultForX;
            resultArray[1] = resultForY;
            return resultArray;
        }
    }

    /**
     * Returns ArrayList of parallel straights of random generated Straight array.
     * All Straight located in pairs.
     * Example: index 0 = firs parallel straight
     * index 1 = second parallel straight
     * @return ArrayList of parallel straights
     */
    static ArrayList<StraightOnAPlane> getArrayOfParallelStraights() {

        HashSet<StraightOnAPlane> array = new HashSet<>();

        // creating array of random straights
        final int amount = 300;
        for (int i = 0; i < amount; i++) {
            final int randomFrom = -20;
            final int randomTo = 20;
            array.add(new StraightOnAPlane(
                    (double) RandomGenerator.getRandomInt(randomFrom, randomTo),
                    (double) RandomGenerator.getRandomInt(randomFrom, randomTo),
                    (double) RandomGenerator.getRandomInt(randomFrom, randomTo)));
        }

        Object[] result = array.toArray();

        ArrayList<StraightOnAPlane> resultArray = new ArrayList<>();
        //searching parallel straights
        for (int i = 0; i < result.length; i++) {
            for (int j = i + 1; j < result.length; j++) {
                if (getCrossingPoints((StraightOnAPlane) result[i],
                        (StraightOnAPlane) result[j]).length == 0) {
                    resultArray.add((StraightOnAPlane) result[i]);
                    resultArray.add((StraightOnAPlane) result[j]);
                }
            }
        }
        return resultArray;
    }
}