package com.courses.se.planets.model;

import java.util.List;

/**
 * Planet class.
 * @author Nemrosim
 * @version 1.1
 */
public class Planet {

    /** Name of a Planet. */
    private String name;

    /** States arrayList of the Planet. */
    private List<State> states;

    /**
     * Constructor of the Planet class. Sets to all Employees of the planet Planet parent variable.
     * @param name   name of the Planet
     * @param states states of the Planet
     */
    public Planet(String name, List<State> states) {
        this.name = name;
        this.states = states;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<State> getStates() {
        return states;
    }

    public void setStates(List<State> states) {
        this.states = states;
    }


    @Override
    public String toString() {
        return name;
    }
}
