package com.courses.se.planets.model;

import java.util.ArrayList;
import java.util.List;

/**
 * State of the Planet class.
 * @author Nemrosim
 * @version 1.1
 */
public class State {

    /** State name. */
    private String name;

    /** Companies of the State. */
    private List<Company> companies;

    /**
     * State constructor. Sets StateParent to all the companies.
     * @param name             State name
     * @param companyArrayList companies of the State
     */
    public State(String name, List<Company> companyArrayList) {
        this.name = name;
        this.companies = companyArrayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(ArrayList<Company> companies) {
        this.companies = companies;
    }
}
