package com.courses.se.planets;

import com.courses.se.planets.model.Employee;
import com.courses.se.planets.model.Planet;

import java.util.Collections;
import java.util.List;

import static java.util.Optional.ofNullable;

/**
 * PlanetMethods class for Planet methods.
 * @author Nemrosim
 * @version 1.1
 */
public final class PlanetMethods {

    /** Private constructor. */
    private PlanetMethods() {
    }

    /**
     * Methods returns the richest guy of the Planet.
     * @param planetArrayList Planet array list
     * @return richest Employee of all Planets
     */
    public static Employee employeeSearch(List<Planet> planetArrayList) {
        return planetArrayList.stream().flatMap(p -> p.getStates().stream())
                .flatMap(s -> ofNullable(s.getCompanies()).orElse(Collections.emptyList()).stream())
                .flatMap(c -> ofNullable(c.getDepartments()).orElse(Collections.emptyList()).stream())
                .flatMap(d -> d.getEmployees().stream())
                .max((e, compare) -> e.getSalary().compareTo(compare.getSalary())).orElse(null);
    }
}
