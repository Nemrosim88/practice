package com.courses.se.planets.model;

import java.util.List;

/**
 * Department class.
 * @author Nemrosim
 * @version 1.1
 */
public class Department {

    /** Name of the Department. */
    private String name;

    /** Employees of the Department. */
    private List<Employee> employees;

    /**
     * Constructor for the Department.
     * @param name      name of the Department
     * @param employees Employees of the Department
     */
    public Department(String name, List<Employee> employees) {
        this.name = name;
        this.employees = employees;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
