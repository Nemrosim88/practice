package com.courses.se.planets.model;

import java.util.List;

/**
 * Company class.
 * @author Nemrosim
 * @version 1.1
 */
public class Company {

    /** Name of the company. */
    private String name;

    /** Departments of the company. */
    private List<Department> departments;

    /** Employees of the company. (If no departments) */
    private List<Employee> employees;

    /**
     * Company constructor if company has departments.
     * @param name                Company name
     * @param departmentArrayList departments of the Company
     */
    public Company(String name, List<Department> departmentArrayList) {
        this.name = name;
        this.departments = departmentArrayList;
        this.employees = null;
    }

    /**
     * Company constructor if company has no departments.
     * @param employeeArrayList Employees of the Company.
     * @param name              name of the Company
     */
    public Company(List<Employee> employeeArrayList, String name) {
        this.name = name;
        this.departments = null;
        this.employees = employeeArrayList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Department> getDepartments() {
        return departments;
    }

    public void setDepartments(List<Department> departments) {
        this.departments = departments;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
