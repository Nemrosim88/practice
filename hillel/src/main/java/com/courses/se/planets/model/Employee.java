package com.courses.se.planets.model;

/**
 * Employee class.
 * @author Nemrosim
 * @version 1.1
 */
public class Employee {

    /** Name of theEmployee. */
    private String name;

    /** Salary of the Employee. */
    private Integer salary;

    /**
     * Constructor of the Employee class.
     * @param name   name of the Employee
     * @param salary salary of the Employee
     */
    public Employee(String name, int salary) {
        this.name = name;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public Integer getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return "Employee:"
                + "Name=> " + name + '\''
                + " Salary=> " + salary;
    }
}
