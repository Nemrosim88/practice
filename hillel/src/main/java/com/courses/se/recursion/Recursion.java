package com.courses.se.recursion;

/**
 * Recursion class.
 * Write a recursive method named searchList with the following header:
 * boolean searchList ( int[] a, int size, int num)
 * The method should accept as parameter the following values
 * with the specification mentioned below:
 * a. A list of integer values
 * b. Size of the array, size >=5
 * c. Number to be searched in the parameter num, num >=0
 * The method should return true if the number is contained within the array list
 * and false if the number is not in the array list.
 */
public class Recursion {

    /**
     * Solution to the task Method return true if array contains number "num" from 0 to "size".
     * @param array A list of integer values
     * @param size  size of the array, size >=5
     * @param num   Number to be searched in the parameter num, num >=0
     * @return true if the number is contained within the array list
     *         and false if the number is not in the array list,
     *         or array contains null
     */
    boolean searchList(int[] array, int size, int num) {
        try {
            return size != -1 && (array[size] == num || searchList(array, size - 1, num));
        } catch (NullPointerException e) {
            return false;
        }
    }
}
