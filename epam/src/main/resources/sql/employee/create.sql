CREATE TABLE "main"."employees" (
"id"  INTEGER NOT NULL,
"surname"  TEXT(20) NOT NULL,
"name"  TEXT(10) NOT NULL,
"position"  TEXT(20) NOT NULL,
"department_number"  INTEGER NOT NULL,
PRIMARY KEY ("id" ASC),
CONSTRAINT "department number" FOREIGN KEY ("department_number") REFERENCES "department" ("id")
)
;