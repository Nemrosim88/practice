CREATE TABLE "main"."department" (
  "id"           INTEGER  NOT NULL,
  "name"         TEXT(30) NOT NULL,
  "phone_number" TEXT(15) NOT NULL,
  PRIMARY KEY ("id" ASC)
);