CREATE TABLE "main"."task" (
  "id"  INTEGER NOT NULL,
  "description"  TEXT(50) NOT NULL,
  "employee_id"  INTEGER NOT NULL,
  PRIMARY KEY ("id"),
  CONSTRAINT "employee_id" FOREIGN KEY ("employee_id") REFERENCES "employees" ("id")
)
;