package epam.courses.task.seven;

import epam.courses.task.seven.model.Employee;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateDemo {


  public void start() {

    StandardServiceRegistry registry = new StandardServiceRegistryBuilder().configure().build();
    SessionFactory factory = new MetadataSources(registry).buildMetadata().buildSessionFactory();
    Session session = factory.openSession();
    List<Employee> list = session.createQuery("from epam.courses.task.seven.model.Employee", Employee.class).getResultList();



  }


}
