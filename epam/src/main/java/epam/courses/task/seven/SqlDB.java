package epam.courses.task.seven;

import epam.courses.task.seven.model.Employee;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Task 7 Part 1 Solution methods.
 */
public class SqlDB {

  private String dbName;
  private Connection connection;
  private Statement statement;
  private ResourceBundle resource = ResourceBundle.getBundle("database");
  private String path = resource.getString("path");
  private static Logger log = LogManager.getLogger(SqlDB.class.getName());

  /**
   * Конструктор для Connection and Statement.
   *
   * @param dbName имя базы что будет создана в папке ресурсов.
   */
  public SqlDB(String dbName) throws ClassNotFoundException, SQLException, IOException {
    this.dbName = dbName;

    if (Files.deleteIfExists(Paths.get(path + dbName))) {
      log.log(Level.WARN, "Previous dataBase was deleted!");
    }

    // Первый способ
    Class.forName("org.sqlite.JDBC");

    log("Class.forName");
    log("Connecting to database...");

    // Второй способ
//    Driver driver = new org.sqlite.JDBC();
//    DriverManager.registerDriver(driver);

    connection = DriverManager.getConnection("jdbc:sqlite:" + path + dbName);
    log("Connection -> " + !connection.isClosed());

    statement = connection.createStatement();
    log("Got the Statement...");
  }


  /**
   * Метод создает таблицы Отдел, Сотрудники, Задачи.
   *
   * <p>SQL запросы находятся в папке ресурсы/sql/...
   */
  public boolean createSQLiteDataBase() throws IOException, SQLException {

    boolean one = statement.execute(readQuery(path + "sql/department/create.sql"));
    log(" - > Department table created");

    boolean two = statement.execute(readQuery(path + "sql/employee/create.sql"));
    log(" - > Employee table created");

    boolean three = statement.execute(readQuery(path + "sql/task/create.sql"));
    log(" - > Task table created");

    boolean result = one && two && three;
    log(" - > All three tables created ->" + result);

    return result;
  }

  /**
   * Заполняет таблицы данными.
   */
  public void insertIntoSQLiteDataBase() throws IOException, SQLException {

    statement.executeUpdate(readQuery(path + "sql/department/insert.sql"));
    log(" - > Department table default main.java.data inserted");

    statement.executeUpdate(readQuery(path + "sql/employee/insert.sql"));
    log(" - > Employee table default main.java.data inserted");

    statement.executeUpdate(readQuery(path + "sql/task/insert.sql"));
    log(" - > Task table default main.java.data inserted");
  }

  /**
   * Метод для получения списка всех сотрудников в таблице Employee.
   */
  public List<Employee> getAllEmployees() {

    String query = "SELECT * FROM employees";
    ResultSet rs = null;
    List<Employee> list = new ArrayList<>();

    try {
      rs = statement.executeQuery(query);
      log(" - > ResultSet created for method getAllEmployees()");

      while (rs.next()) {
        String surname = rs.getString("surname");
        String name = rs.getString("name");
        String position = rs.getString("position");
        list.add(new Employee(surname, name, position));
        log(" - > One of ALL Employees added to result list");
      }
      return list;
    } catch (SQLException e) {
      log.log(Level.ERROR,
          e.getMessage() + "*** \n"
              + e.getSQLState());

    } finally {
      log("finally block");
      try {
        rs.close();
      } catch (SQLException e) {
        log.log(Level.ERROR, e.getMessage());
      }
      log(" - > ResultSet closed for method getAllEmployees()");
      log(" --- > RETURNING RESULT! \n");
      return list;
    }
  }

  /**
   * Метод для получения сотрудников определённого отдела.
   */
  public List<Employee> getEmployeesOfDepartment(String departmentName) throws SQLException {

    //language=SQL
    String query =
        "SELECT * FROM employees JOIN department ON employees.department_number = department.id "
            + "WHERE department.name = ?";
    PreparedStatement ps = connection.prepareStatement(query);
    ps.setString(1, departmentName);
    ResultSet rs = ps.executeQuery();
    log(" - > ResultSet created for method getEmployeesOfDepartment()");

    List<Employee> list = new ArrayList<>();

    while (rs.next()) {
      String surname = rs.getString("surname");
      String name = rs.getString("name");
      String position = rs.getString("position");
      list.add(new Employee(surname, name, position));
      log(" - > One of Department Employees added to result list");
    }

    rs.close();
    ps.close();
    log(" - > ResultSet closed for method getEmployeesOfDepartment()");
    log(" --- > RETURNING RESULT! \n");
    return list;
  }

  /**
   * Получить все задания.
   *
   * @return список всех заданий
   * @throws SQLException SQLException
   */
  public List<String> getAllTasksFromDB() throws SQLException {

    String query = "SELECT * FROM task";
    ResultSet rs = statement.executeQuery(query);
    log(" - > ResultSet created for method getAllTasksFromDB()");

    List<String> list = new ArrayList<>();

    while (rs.next()) {
      list.add(rs.getString("description"));
      log(" - > One of ALL Tasks in DB added to result list");
    }

    rs.close();
    log(" - > ResultSet closed for method getAllTasksFromDB()");
    log(" --- > RETURNING RESULT! \n");
    return list;
  }

  /**
   * Метод добавления задания определённому сотруднику.
   *
   * @param task Задание
   * @param e Сотрудник
   * @throws SQLException SQLException
   */
  public void addTaskToEmployee(String task, Employee e) throws SQLException {
    log(" - > Enter to addTaskToEmployee() method! \n");
//    connection.setAutoCommit(false);
//    Savepoint save = connection.setSavepoint("savepoint method addTaskToEmployee()");

    //language=SQL
    String searchQuery =
        "SELECT employees.id "
            + "FROM employees "
            + "WHERE employees.name = ? AND employees.surname = ? ";

    //language=SQL
    String query =
        "INSERT INTO task(description, employee_id) VALUES (?, (" + searchQuery + "))";

    PreparedStatement ps = connection.prepareStatement(query);

    ps.setString(1, task);
    ps.setString(2, e.getName());
    ps.setString(3, e.getSurname());

    ps.executeUpdate();

//    connection.commit();
//     connection.releaseSavepoint(save);
//    connection.setAutoCommit(true);
    log(" - > executeUpdate of addTaskToEmployee() method! \n");
  }

  /**
   * Метод возвращает все задания конкретного сотрудника.
   *
   * @param e сотрудник
   * @return список заданий
   * @throws SQLException SQLException
   */
  public List<String> getAllTasksOfEmployee(Employee e) throws SQLException {

    String query = "SELECT description "
        + "FROM task JOIN employees ON task.employee_id = employees.id "
        + "WHERE employees.name = ? AND employees.surname = ? AND employees.position= ?";
    PreparedStatement ps = connection.prepareStatement(query);
    ps.setString(1, e.getName());
    ps.setString(2, e.getSurname());
    ps.setString(3, e.getPosition());

    ResultSet rs = ps.executeQuery();
    log(" - > ResultSet created for method getAllTasksOfEmployee()");

    List<String> list = new ArrayList<>();

    while (rs.next()) {
      list.add(rs.getString("description"));
      log(" - > One of all Tasks of " + e + "added to result list");
    }

    rs.close();
    log(" - > ResultSet closed for method getAllTasksFromDB()");
    log(" --- > RETURNING RESULT! \n");
    return list;
  }

  /**
   * Метод для удаления сотрудника из базы.
   *
   * @param e Сотрудник
   * @throws SQLException SQLException
   */
  public void deleteEmployee(Employee e) throws SQLException {
    //language=SQL
    String query = "DELETE FROM employees WHERE name= ? AND surname= ? AND position= ?";
    PreparedStatement ps = connection.prepareStatement(query);
    ps.setString(1, e.getName());
    ps.setString(2, e.getSurname());
    ps.setString(3, e.getPosition());
    ps.execute();
    log(" --- > " + e + " was deleted from DB \n");
  }

  /**
   * Метод для закрытия ресурсов.
   */
  public void closeResources() throws SQLException, IOException {
    statement.close();
    log(" - > Statement closed");

    connection.close();
    log(" - > Connection closed");

//    if (Files.deleteIfExists(Paths.get(path + dbName))) {
//      log.log(Level.WARN, "DataBase " + path + dbName + " was deleted!");
//    }
  }

  private void log(String message) {
    log.log(Level.INFO, message);
  }

  /**
   * Приватный метод для чтения SQL запросов из файла.
   *
   * @param filePath путь к файлу
   * @return строки в файле
   */
  private static String readQuery(String filePath) throws IOException {
    StringBuilder sb = new StringBuilder();
    Files.readAllLines(Paths.get(filePath)).forEach(sb::append);
    return sb.toString();
  }

}
