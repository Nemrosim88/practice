package epam.courses.task.seven.model;

public class Department {

  private String name;
  private String phoneNumber;

  public Department(String name, String phoneNumber) {
    this.name = name;
    this.phoneNumber = phoneNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Department that = (Department) o;

    if (!name.equals(that.name)) {
      return false;
    }
    return phoneNumber.equals(that.phoneNumber);
  }

  @Override
  public int hashCode() {
    int result = name.hashCode();
    result = 31 * result + phoneNumber.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Department{"
        + "name='" + name + '\''
        + ", phoneNumber='" + phoneNumber + '\'' + '}';
  }
}
