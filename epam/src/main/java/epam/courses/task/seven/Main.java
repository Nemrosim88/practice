package epam.courses.task.seven;

import epam.courses.task.seven.model.Employee;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import org.apache.log4j.PropertyConfigurator;

public class Main {


  static final Comparator<Integer> IntegerComparator
      = new Comparator() {
    @Override
    public int compare(Object o1, Object o2) {
      return ((Integer) o1).compareTo((Integer) o2);
    }
  };

  public static void main(String args[]) {
    ArrayList<Integer> list = new ArrayList<>();
    list.add(4);
    list.add(1);
    list.add(3);
    list.add(2);
    Collections.sort(list, null);
    System.out.print(Collections.binarySearch(list, 3));
    Collections.sort(list, IntegerComparator);
    System.out.print(Collections.binarySearch(list, 3));
  }


  private static final Employee EMPLOYEE = new Employee("Филимонов", "Филип", "специалист");


  private static void start() throws SQLException, IOException, ClassNotFoundException {
    PropertyConfigurator.configure("epam/src/main/resources/log4j.properties");

    SqlDB db = new SqlDB("SQLite.db");
    db.createSQLiteDataBase();
    db.insertIntoSQLiteDataBase();
    db.getAllEmployees().forEach(System.out::println);
    db.getAllTasksFromDB().forEach(System.out::println);
    db.getEmployeesOfDepartment("Отдел оформления заказов").forEach(System.out::println);
    db.addTaskToEmployee("Hello World", EMPLOYEE);
    db.getAllTasksOfEmployee(EMPLOYEE).forEach(System.out::println);
    db.deleteEmployee(EMPLOYEE);
    db.closeResources();
  }

}
