package epam.courses.task.seven.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employee {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  Long id;
  @Column(name = "surname")
  private String surname;
  @Column(name = "name")
  private String name;
  @Column (name = "position")
  private String position;

  public Employee() {
  }

  public Employee(String surname, String name, String position) {
    setSurname(surname);
    setName(name);
    setPosition(position);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Employee employee = (Employee) o;

    if (!surname.equals(employee.surname)) {
      return false;
    }
    if (!name.equals(employee.name)) {
      return false;
    }
    return position.equals(employee.position);
  }

  @Override
  public int hashCode() {
    int result = surname.hashCode();
    result = 31 * result + name.hashCode();
    result = 31 * result + position.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "Employee {"
        + "surname ='" + surname + '\''
        + ", name ='" + name + '\''
        + ", position ='" + position + '\''
        + '}';
  }
}
