package epam.courses.task.two.one;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

/**
 * <h1>Solution class for task 2 - task2.pdf -> Part 1</h1>
 *
 * <p>Задание: Напишите консольное приложение, которое: - читает с клавиатуры произвольный текст,
 * состоящий из нескольких предложений и строк; - выполняет обработку введенного текста (удалите из
 * текста все слова указанной длины и начинающиеся на гласную букву).</p>
 */
public class ConsoleSoft {

  /**
   * Welcome text that will be printed to console after app starts.
   */
  private static final String WELCOME_TEXT = "Перед началом введите длинну слов, "
      + "которые нужно будет удалить. "
      + "\n Слова будут начинаться на гласную буквую";

  /**
   * Thankful text. Good to be polite.
   */
  private static final String THANKS = "Спасибо за выбор";

  /**
   * Private constructor. Utility class.
   */
  private ConsoleSoft() {
  }

  /**
   * Читает с клавиатуры произвольный текст, состоящий из нескольких предложений и строк; <br> 1.-
   * выполняет обработку введенного текста (изменяет этот же текст, не создает новый) <br> Удаляет
   * из текста все слова указанной длины и начинающиеся на гласную букву
   */
  static void replaceWords() {
    StringBuilder stringBuilder = new StringBuilder();
    try (InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr)) {
      System.out.println(WELCOME_TEXT);
      int a = Integer.valueOf(br.readLine());
      System.out.println(THANKS);

      String s;
      while (true) {
        s = br.readLine();
        if (s.isEmpty()) {
          break;
        } else {
          stringBuilder.append("\n");
          stringBuilder.append(s);
          s = "";
        }
      }
      final String regExp = String.format("[eyuiaoEYUIOAЙУЕЭОАЮИйуеэоаюия]{%d}", a);
      String resultString = Pattern.compile(regExp).matcher(stringBuilder).replaceAll("");
      System.out.println("RESULT: -> " + resultString);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
