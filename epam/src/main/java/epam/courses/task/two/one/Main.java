package epam.courses.task.two.one;


/**
 * Main class.
 */
public class Main {

  /**
   * Entry point.
   *
   * @param args arguments
   */
  public static void main(String[] args) {
    ConsoleSoft.replaceWords();
  }

}
