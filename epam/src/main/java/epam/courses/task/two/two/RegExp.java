package epam.courses.task.two.two;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <h1>Task2.pdf -> part 2 test</h1>
 *
 * <p> Задание: Напишите консольное приложение, которое:
 *
 * <br>- вводит регулярное выражение согласно варианту (таблица 2);
 *
 * <br>- вводит некоторую строку и определяет наличие в ней слова соответствующего регулярному
 * выражению.
 *
 * <br>Предусмотреть ввод произвольного количества (нескольких) строк.</p>
 */
public class RegExp {

  /**
   * Private constructor. Utility class.
   */
  private RegExp() {
  }

  /**
   * Method finds needed pattern in text, and returns verbose report about what were found.
   *
   * @param text where to find patterns
   * @return report - what and where was found
   */
  public static String matchText(String text) {
    String regExp = "\\(([A-Z]{1}[,]+)*[A-Z]\\)";
    Matcher m = Pattern.compile(regExp).matcher(text);
    StringBuilder sb = new StringBuilder();
    while (m.find()) {
      sb.append("Found\n")
          .append("Start index: ")
          .append(m.start())
          .append("; End index: ")
          .append(m.end())
          .append("-> ")
          .append(m.group())
          .append("\n");
    }
    return sb.toString();
  }
}
