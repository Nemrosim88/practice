package epam.courses.task.six;

/**
 *
 */
public class TemperatureConverter {

  /**
   * Конветрация Фар в Цельс.
   *
   * @param degreesFahrenheit
   * @return
   */
  public double convertFtoC(double degreesFahrenheit) {
    return (degreesFahrenheit - 32.0) / 9.0 * 5.0;
  }

  /**
   * @param degreesCelsius
   * @return
   */
  public double convertCtoF(double degreesCelsius) {
    return degreesCelsius / 5.0 * 9.0 + 32.0;
  }

  /**
   * @param degreesCelsius
   * @return
   */
  public double convertCtoK(double degreesCelsius) {
    return degreesCelsius + 273.2;
  }

  /**
   * @param kelvin
   * @return
   */
  public double convertKtoC(double kelvin) {
    return kelvin - 273.2;
  }

  /**
   * @param degreesFahrenheit
   * @return
   */
  public double convertFtoK(double degreesFahrenheit) {
    return (degreesFahrenheit + 459.7) / 9.0 * 5.0;
  }

  /**
   * @param kelvin
   * @return
   */
  public double convertKtoF(double kelvin) {
    return kelvin / 5.0 * 9.0 - 459.7;
  }
}
