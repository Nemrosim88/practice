package epam.courses.task.four.two;

import epam.courses.task.one.one.TaskOnePartOne;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionString {

  private String textToChangeWithReflection;

  /**
   * Change Text To Another One using reflection.
   */
  public static String changeTextToAnotherOne(String text, String newText) {
    try {
      Field filed = (String.class).getDeclaredField("value");
      filed.setAccessible(true);
      filed.set(text, newText.toCharArray());
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }
    return null;
  }

  public static void main(String[] args) {
    String one = "hello";
    String two = "world";
    changeTextToAnotherOne(one,two);
    System.out.println(one);
  }
}
