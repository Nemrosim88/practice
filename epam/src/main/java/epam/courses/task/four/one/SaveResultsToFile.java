package epam.courses.task.four.one;

import epam.courses.task.one.three.model.Subscriber;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

/**
 * Task4.pdf -> One
 */
public class SaveResultsToFile {

  /**
   * Method that will save text fo file.
   *
   * <p> File will be saved to epam\src\main\java\epam\courses\task\four\one\files\ dir.
   *
   * @param fileName how you want to name your file
   * @param list subscribers you want to save
   */
  public static void saveToFile(String fileName, List<Subscriber> list) throws IOException {

    String path = Paths.get("").toAbsolutePath().toString() + "\\" + fileName;

    File file = new File(path);
    if (!file.exists()) {
      file.createNewFile();
    }
    try (FileOutputStream out = new FileOutputStream(
        path); ObjectOutputStream o = new ObjectOutputStream(out)) {
      o.writeObject(list);
    }
  }

  /**
   * Method that will read from file
   *
   * @param fileName name of the file you want to read from
   * @return will return NULL if something went wrong
   */
  public static List<Subscriber> readFromFileOfSubscribers(String fileName)
      throws IOException, ClassNotFoundException {
    String path = Paths.get("").toAbsolutePath().toString() + "\\" + fileName;
    try (FileInputStream fis = new FileInputStream(new File(path));
        ObjectInputStream ois = new ObjectInputStream(fis)) {
      return (List<Subscriber>) ois.readObject();
    }
  }
}
