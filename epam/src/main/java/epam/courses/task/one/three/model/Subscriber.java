package epam.courses.task.one.three.model;

import com.sun.istack.internal.NotNull;
import java.io.Serializable;

public class Subscriber implements Serializable {

  /**
   * Field for Subscriber's name.
   */
  private String name;

  /**
   * Field for Subscriber's surname.
   */
  private String surname;

  /**
   * Field for Subscriber's middleName.
   */
  private String middleName;

  /**
   * Field for Subscriber's adress.
   */
  private String adress;

  /**
   * Field for Subscriber's time of long distance calls.
   */
  private int timeOfLongDistanceCalls;

  /**
   * Field for Subscriber's time of city calls.
   */
  private int timeOfCityCalls;

  /**
   * Constructor for Subscriber.
   *
   * @param name param for name
   * @param surname param for surname
   * @param middleName param for middleName
   * @param adress param for adress
   * @param timeOfLongDistanceCalls param for time of long distance calls
   * @param timeOfCityCalls param for time of city calls
   */
  public Subscriber(String name,
      String surname,
      String middleName,
      String adress,
      int timeOfLongDistanceCalls,
      int timeOfCityCalls) {
    this.name = name;
    this.surname = surname;
    this.middleName = middleName;
    this.adress = adress;
    this.timeOfLongDistanceCalls = timeOfLongDistanceCalls;
    this.timeOfCityCalls = timeOfCityCalls;
  }

  /**
   * Getter for 'name' value.
   *
   * @return возвращает переменную "name"
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for 'name' value.
   */
  public void setName(@NotNull String name) {
    this.name = name;
  }

  /**
   * Геттер для переменной "surname".
   *
   * @return возвращает переменную "surname"
   */
  public String getSurname() {
    return surname;
  }

  /**
   * @param surname
   */
  public void setSurname(@NotNull String surname) {
    this.surname = surname;
  }

  /**
   * Геттер для переменной "middleName".
   *
   * @return возвращает переменную "middleName"
   */
  public String getMiddleName() {
    return middleName;
  }

  /**
   * @param middleName
   */
  public void setMiddleName(@NotNull String middleName) {
    this.middleName = middleName;
  }

  /**
   * Геттер для переменной "adress".
   *
   * @return возвращает переменную "adress"
   */
  public String getAdress() {
    return adress;
  }

  /**
   * @param adress
   */
  public void setAdress(@NotNull String adress) {
    this.adress = adress;
  }

  /**
   * Геттер для переменной "timeOfLongDistanceCalls".
   *
   * @return возвращает переменную "timeOfLongDistanceCalls" - время междугородних разговоров
   */
  public int getTimeOfLongDistanceCalls() {
    return timeOfLongDistanceCalls;
  }

  /**
   * @param timeOfLongDistanceCalls
   */
  public void setTimeOfLongDistanceCalls(int timeOfLongDistanceCalls) {
    this.timeOfLongDistanceCalls = timeOfLongDistanceCalls;
  }

  /**
   * Геттер для переменной "timeOfCityCalls".
   *
   * @return возвращает переменную "timeOfCityCalls" - время метсных разговоров.
   */
  public int getTimeOfCityCalls() {
    return timeOfCityCalls;
  }

  /**
   * @param timeOfCityCalls
   */
  public void setTimeOfCityCalls(int timeOfCityCalls) {
    this.timeOfCityCalls = timeOfCityCalls;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    } else if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Subscriber that = (Subscriber) o;
    if (timeOfLongDistanceCalls != that.timeOfLongDistanceCalls) {
      return false;
    } else if (timeOfCityCalls != that.timeOfCityCalls) {
      return false;
    } else if (!name.equals(that.name)) {
      return false;
    } else if (!surname.equals(that.surname)) {
      return false;
    } else if (!middleName.equals(that.middleName)) {
      return false;
    }
    return adress.equals(that.adress);
  }

  @Override
  public int hashCode() {
    final int RANDOM_NUMBER = 31;
    int result = name.hashCode();
    result = RANDOM_NUMBER * result + surname.hashCode();
    result = RANDOM_NUMBER * result + middleName.hashCode();
    result = RANDOM_NUMBER * result + adress.hashCode();
    result = RANDOM_NUMBER * result + timeOfLongDistanceCalls;
    result = RANDOM_NUMBER * result + timeOfCityCalls;
    return result;
  }

  @Override
  public String toString() {
    return "Subscriber:"
        + "Name='" + name + "' "
        + "surname='" + surname + "' "
        + "middleName='" + middleName + "' "
        + "adress='" + adress + "' "
        + "timeOfLongDistanceCalls='" + timeOfLongDistanceCalls + "' "
        + "timeOfCityCalls='" + timeOfCityCalls + "'\n";
  }
}
