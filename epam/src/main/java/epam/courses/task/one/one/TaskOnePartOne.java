package epam.courses.task.one.one;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;


/**
 * TaskOnePartTwo class for pdf file - task1.pdf -> part 1
 *
 * @version 1.2
 */
public class TaskOnePartOne {

  /**
   * BINARY_NUMERAL_SYSTEM.
   */
  private static final int BINARY_NUMERAL_SYSTEM = 2;

  /**
   * OCTAL_NUMERAL_SYSTEM.
   */
  private static final int OCTAL_NUMERAL_SYSTEM = 8;

  /**
   * HEX_NUMERAL_SYSTEM.
   */
  private static final int HEX_NUMERAL_SYSTEM = 16;

  /**
   * Or it will be "magic number"
   */
  private static final int FOUR = 4;

  /**
   * Private constructor. Utility class.
   */
  private TaskOnePartOne() {
  }

  /**
   * <h2>Task 1.1</h2>
   *
   * <p>Задание: Напишите консольное приложение, которое определяет целое положительное число в
   * десятичной системе, затем преобразует и выводит его в двоичной/восьмиричной/шестнадцатиричной
   * системах счисления.</p>
   *
   * <p>[TaskOnePartTwo doc] Conversion method (that uses Integer.parseInt() method) from decimal to
   * binary/octal/hexadecimal numeral system.</p>
   *
   * @param inText value in decimal numeral system
   * @param numeralSystem value that indicates in which numeral system "inText" will be converted.
   * @return String value in another numeral system. If "inText" or "numeralSystem" where incorrect
   * than will be returned specific error message.
   */
  @Deprecated
  public static String convertToAnotherNumeralSystem(String inText, int numeralSystem)
      throws InvalidParameterException, NumberFormatException {
    // this code will throw NumberFormatException is inText is not a number
    StringUtils.isNumeric(inText);

    if (numeralSystem == BINARY_NUMERAL_SYSTEM
        || numeralSystem == OCTAL_NUMERAL_SYSTEM
        || numeralSystem == HEX_NUMERAL_SYSTEM) {
      return Integer.toString(Integer.parseInt(inText), numeralSystem);
    } else {
      throw new InvalidParameterException("Wrong numeral System");
    }
  }

  /**
   * <h2>TASK 1.1</h2>
   *
   * <p>Напишите консольное приложение, которое определяет целое положительное число в десятичной
   * системе, затем преобразует и выводит его в двоичной/восьмиричной/шестнадцатиричной системах
   * счисления.</p>
   *
   * <p>[TaskOnePartTwo doc] Converting decimal values to another numeral system in vintage way.</p>
   *
   * @param inputValue передаём значение
   * @param numeralSystem в какую систему будем переводить?
   * @return converted string representation
   */
  public static String convertToAnotherNumeralSystem(int inputValue, int numeralSystem)
      throws InvalidParameterException {
    // For binary operations----------------------------
    if (numeralSystem == BINARY_NUMERAL_SYSTEM) {
      int i = 0;
      final int pyramidHeight = 9;
      int[] tempArray = new int[pyramidHeight];
      while (inputValue > 0) {
        tempArray[i++] = inputValue % 2;
        inputValue /= 2;
      }

      StringBuilder stringBuilder = new StringBuilder();
      for (int intFromArray : tempArray) {
        stringBuilder.append(intFromArray);
      }

      return stringBuilder.reverse().toString();

      //For 8---------------------------------------
    } else if (numeralSystem == OCTAL_NUMERAL_SYSTEM) {
      int counter = 0;
      int result = 0;
      final int degree = 10;
      while (inputValue != 0) {
        int temp = (int) ((inputValue % OCTAL_NUMERAL_SYSTEM) * Math.pow(degree, counter));
        counter++;
        result += temp;
        inputValue /= OCTAL_NUMERAL_SYSTEM;
      }
      return "" + result;
      //For 16---------------------------------------
    } else if (numeralSystem == HEX_NUMERAL_SYSTEM) {
      final int halfByte = 0x0F;
      final char[] hexDigits = {
          '0', '1', '2', '3', '4', '5', '6', '7',
          '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
      };

      StringBuilder hexBuilder = new StringBuilder(OCTAL_NUMERAL_SYSTEM);
      hexBuilder.setLength(OCTAL_NUMERAL_SYSTEM);
      for (int i = OCTAL_NUMERAL_SYSTEM - 1; i >= 0; --i) {
        int j = inputValue & halfByte;
        hexBuilder.setCharAt(i, hexDigits[j]);
        inputValue >>= FOUR;
      }
      return hexBuilder.toString();
    } else {
      throw new InvalidParameterException();
    }
  }

  /**
   * <h2>TASK 1.2</h2>
   *
   * <p>Напишите консольное приложение, которое находит все совершенные числа в диапазоне от 1 по
   * некоторое заданное положительное число и выводит их (совершенное число – это число, которое
   * равно сумме всех своих делителей, кроме самого себя. Например, 6 = 1+2+3).</p>
   *
   * <p>[TaskOnePartTwo doc] Method returns sequence of perfect numbers if they less or equal of
   * "inputValue". <br> Example: input value = 600 method will return sequence: [6, 28, 496].</p>
   *
   * @param inputValue maximal number
   * @return if "inputValue" will be incorrect, method will return an empty ArrayList.
   */
  public static List<Integer> getPerfectNumber(String inputValue) throws NumberFormatException {
    // this code will throw NumberFormatException is inputValue is not a number
    StringUtils.isNumeric(inputValue);

    //convert String value to integer
    int inputConvertedToInt = Integer.parseInt(inputValue);

    //This list will be used for number that satisfy condition
    // (Math.pow(2, i)-1)== prime number
    List<Integer> primeList = new ArrayList<>();
    final int randomNumber = 1000;
    for (int i = 0; i <= randomNumber; i++) {
      int result = (int) (Math.pow(2, i) - 1);
      if (isPrime(result)) {
        primeList.add(i);
      }
    }

    //Result list in which result values will be placed.
    List<Integer> resultList = new ArrayList<>();

    for (Integer prime : primeList) {
      //formula for perfect number search
      int result = (int) (Math.pow(2, (prime - 1))
          * (Math.pow(2, prime) - 1));
      if (result <= inputConvertedToInt) {
        resultList.add(result);
      } else {
        break;
      }
    }
    return resultList;
  }

  /**
   * <h2>Task 1.3</h2>
   *
   * <p>Задание: Напишите консольное приложение, которое вызывает метод для построения пирамиды
   * высотой в диапазоне от 1 по 9.</p>
   *
   * <p>[TaskOnePartTwo doc] Method returns string representation of a pyramid.</p>
   *
   * @param height height of a pyramid from 1 to 9
   * @return string representation of a pyramid
   */
  public static String buildPyramid(int height) throws InvalidParameterException {
    final int maxPyramidHeight = 9;
    if (height > maxPyramidHeight || height < 1) {
      throw new InvalidParameterException("Value \"height\" must be 1 <= height t=> 9");
    }
    StringBuilder finalString = new StringBuilder();
    // Cycle for all the pyramid.
    // Will continue based on "height" of the pyramid.
    for (int k = 1; k < height + 1; k++) {
      //cycle for spaces
      StringBuilder spacesString = new StringBuilder();
      for (int i = 0; i < maxPyramidHeight - k; i++) {
        spacesString.append(" ");
      }
      //cycle for numbers
      StringBuilder numbersString = new StringBuilder();
      for (int i = 0; i < k; i++) {
        numbersString.append(i + 1);
      }
      //combining results
      spacesString.append(numbersString)
          .append(numbersString.reverse().deleteCharAt(0))
          .append("\n");

      finalString.append(spacesString);
    }
    return finalString.toString();
  }

  /**
   * <p>Private method for getPerfectNumber() method</p> <p> Prime - простое число:2, 3, 5, 7, 11,
   * 13, 17, 19, 23, 29, 31, 37, 41... Условия по которым можно определить простое ли число.</p>
   *
   * @param input input numbers.
   * @return boolean value. If the value is prime - true, if not - false.
   */
  private static boolean isPrime(long input) {
    //less than 2 cant't be prime
    final int two = 2;
    // 2 and 3 automatically is prime
    final int three = 3;

    if (input < two) {
      return false;
    } else if (input == two || input == three) {
      return true;
    } else if (input % two == 0 || input % three == 0) {
      return false;
    }

    final long sqrtN = (long) Math.sqrt(input) + 1;
    final long randomNumber = 6L;
    final long six = 6;
    for (long i = randomNumber; i <= sqrtN; i += six) {
      if (input % (i - 1) == 0 || input % (i + 1) == 0) {
        return false;
      }
    }
    return true;
  }
}
