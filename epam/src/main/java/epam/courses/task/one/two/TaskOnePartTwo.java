package epam.courses.task.one.two;

import java.security.InvalidParameterException;

/**
 * TaskOnePartTwo class for pdf file - task1.pdf -> part 2
 */
public class TaskOnePartTwo {

  /**
   * Private constructor. Utility class.
   */
  private TaskOnePartTwo() {
  }

  /**
   * <h2>Task 1.3</h2>
   *
   * <p>Задание:Напишите консольное приложение, которое вызывает метод для построения пирамиды
   * высотой в диапазоне от 1 по 9.</p>
   *
   * <p>[TaskOnePartTwo doc] Method for matrix shift.</p>
   *
   * @param shift shift parameter
   * @return returns modified(shifted) matrix
   */
  public static double[][] matrixShiftDown(double[][] matrix, int shift) {
    final int maxPyramidShift = 6;

    // Если значение сдвига матрицы введено неверно, то возвращается пустой массыв.
    if (shift < 1 || shift > maxPyramidShift) {
      return new double[0][0];
    }

    final int matrixSize = 6;
    //one action
    double[][] shiftedMatrix = new double[matrixSize][matrixSize];
    for (int i = 0; i < matrix.length - shift; i++) {
      System.arraycopy(matrix[i], 0, shiftedMatrix[i + shift], 0, matrix.length);
    }

    //two action
    for (int i = 0; i < shift; i++) {
      System.arraycopy(matrix[matrix.length - shift + i], 0, shiftedMatrix[i], 0, matrix.length);
    }

    return shiftedMatrix;
  }

  /**
   * <p>[RUS] Метод возвращает сгенерированный двумерный массив размером [n][n] со значениями от
   * 'min' до 'max'</p>
   *
   * <p>[TaskOnePartTwo doc] Random matrix [n][n] generator. (Addition to "matrix solution") This method
   * was created separately because of tests, so that I could test values and see changes. With
   * Math.random it can't be done.</p>
   *
   * @param n value of matrix height and length
   * @param min minimal generated number that will be put in the matrix
   * @param max maximal generated number that will be put in the matrix
   * @return array[n][n] will randomly generated values from 'min' to 'max'
   * @see Math
   */
  public static double[][] randomMatrixGenerator(int n, int min, int max) {
    if (n <= 0) {
      throw new InvalidParameterException("n value should be more than zero");
    }
    double[][] matrix = new double[n][n];
    // Randomly generating values for matrix
    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix.length; j++) {
        matrix[i][j] = Math.random() * (min) + Math.random() * (max);
      }
    }
    return matrix;
  }
}
