package epam.courses.task.one.three;

import epam.courses.task.one.three.model.Subscriber;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * <h2>Task 3.1</h2>
 */
public class SubscriberMethods {

  /**
   * List variable.
   */
  private List<Subscriber> list;

  /**
   * Constructor for SubscriberMethods, that needs list of Subscribers.
   *
   * @param list list of Subscribers
   * @throws NullPointerException if list = null
   */
  public SubscriberMethods(List<Subscriber> list) throws NullPointerException {
    if (list != null) {
      this.list = list;
    } else {
      throw new NullPointerException("You passed list that in NULL");
    }

  }

  /**
   * Кто самый говорливый? Если список = NULL, метод вернёт пустой список.
   *
   * @param minutesCalls значение времени городских переговоров (в минутах)
   */
  public List<Subscriber> getSubscribersWithCityCallsByMinutes(int minutesCalls) {
    List<Subscriber> result = new ArrayList<>();
    for (Subscriber subscriber : list) {
      if (subscriber.getTimeOfCityCalls() >= minutesCalls) {
        result.add(subscriber);
      }
    }
    return null;
  }

  /**
   * Возвращает абонентов которые использовали междугороднюю связь.
   *
   * @return список абонентов которые пользовались международной связью.
   */
  public List<Subscriber> getSubscribersThatUsedLongDistCalls() {
    List<Subscriber> resultList = new ArrayList<>();
    for (Subscriber subscriber : list) {
      if (subscriber.getTimeOfLongDistanceCalls() > 0) {
        resultList.add(subscriber);
      }
    }
    return null;
  }

  /**
   * Метод сортировки по фамилии (РЕВЕРС).
   */
  public void sortBySurnameReverse() {
    list.sort((o1, o2) -> o2.getSurname().compareTo(o1.getSurname()));
  }

  /**
   * Метод сортировки по фамилии.
   */
  public void sortBySurname() {
    list.sort(Comparator.comparing(Subscriber::getSurname));
  }

  /**
   * Getter for List of Subscribers.
   */
  public List<Subscriber> getList() {
    return list;
  }

  /**
   * Setter for List of Subscribers.
   */
  public void setList(List<Subscriber> list) {
    this.list = list;
  }
}
