package epam.courses.task.one.three;

import epam.courses.task.one.three.model.Subscriber;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Main class with main method that demonstrates solution to task 3.1 *
 */
public class Main {

  /**
   * Entry point.
   */
  public static void main(String[] args) {

    Subscriber[] mass = {new Subscriber("1", "Art", "Art", "Kiev", 100, 700),
        new Subscriber("2", "Bart", "Art", "Kiev", 30, 310),
        new Subscriber("3", "Cart", "Art", "Kiev", 0, 50),
        new Subscriber("4", "Dart", "Art", "Kiev", 270, 10),
        new Subscriber("5", "Efa", "Art", "Kiev", 0, 240),
        new Subscriber("6", "Fgtq", "Art", "Kiev", 32, 12),
        new Subscriber("7", "Far", "Art", "Kiev", 11, 40),};

    String menu = "ВАС ПРИВЕТСТВУЕТ СУПЕР ПРОГРАММА ПО РАБОТЕ С АБОНЕНТАМИ!"
        + "\n Предлагаем следующее меню:"
        + "\n1. Получить сведения об абонентах, у которых время городских переговоров превышает указанное;"
        + "\n2. Получить сведения об абонентах, которые пользовались междугородней связью;"
        + "\n3. Упорядочить абонентов в порядке обратном алфавитному согласно фамилии"
        + "\n4. Упорядочить абонентов в алфавитном порядке согласно фамилии"
        + "\nНажмите 5 если хотите завершить программу.";

    SubscriberMethods sm = new SubscriberMethods(Arrays.asList(mass));
    try (InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr)) {
      System.out.println(menu);
      for (Subscriber subscriber : mass) {
        System.out.print("***" + subscriber);
      }
      while (true) {
        int a = Integer.valueOf(br.readLine());
        if (a == 1) {
          System.out.println("Введите время городских переговоров.");
          int b = Integer.valueOf(br.readLine());
          System.out.println("Результат");
          for (Subscriber subscriber : sm.getSubscribersWithCityCallsByMinutes(b)) {
            System.out.print(subscriber);
          }
        } else if (a == 2) {
          System.out.println("ВОТ ОНИ!");
          for (Subscriber subscriber : sm.getSubscribersThatUsedLongDistCalls()) {
            System.out.print(subscriber);
          }

        } else if (a == 3) {
          System.out.println("ВОТ ОНИ!");
          sm.sortBySurnameReverse();
          for (Subscriber subscriber : sm.getList()) {
            System.out.print(subscriber);
          }
        } else if (a == 4) {
          System.out.println("ВОТ ОНИ!");
          sm.sortBySurname();
          for (Subscriber subscriber : sm.getList()) {
            System.out.print(subscriber);
          }
        } else if (a == 5) {
          System.out.println("Спасибо за выбор! Удачи");
          return;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
