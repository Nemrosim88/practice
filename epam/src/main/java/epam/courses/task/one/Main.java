package epam.courses.task.one;


import static java.lang.System.out;

import epam.courses.task.one.one.TaskOnePartOne;
import epam.courses.task.one.two.TaskOnePartTwo;

/**
 * Main class for console results.
 */
public final class Main {

  /**
   * This class was created because of the task. <br>All possible result you can see in TEST
   * folder.
   *
   * @param args arguments for starting point
   */
  public static void main(String[] args) {

    //All parameter can be read by
    // Scanner scanner = new Scanner(System.in);
    // scanner.nextLine();

    // ================================Task_1=====================================
    out.println("Task 1.Converting number 123 to binary/oct/hex ");
    final int binaryValue = 2;
    final int octalValue = 8;
    final int hexValue = 16;
    out.println(TaskOnePartOne.convertToAnotherNumeralSystem("123", binaryValue));
    out.println(TaskOnePartOne.convertToAnotherNumeralSystem("123", octalValue));
    out.println(TaskOnePartOne.convertToAnotherNumeralSystem("123", hexValue));

    // ================================Task_2=====================================
    out.println("\n-----Task 2-----\n-----Совершенные числа до 8000");
    out.println(TaskOnePartOne.getPerfectNumber("8000").toString());

    // ================================Task_3=====================================
    out.println("\n-----Task 3-----\n-----Высота пирамиды 5");
    final int PYRAMID_HEIGHT = 5;
    out.println(TaskOnePartOne.buildPyramid(PYRAMID_HEIGHT));

    // ================================ Task_4 =====================================
    out.println("\n-----Task 4-----\n------Исходная матрица");

    //==================== ФОРМИРУЕМ МАТРИЦУ ========
    double[][] inputMatrix = TaskOnePartTwo.randomMatrixGenerator(6, -20, 20);

    for (double[] anInputMatrix : inputMatrix) {
      for (double anAnInputMatrix : anInputMatrix) {
        out.println(String.format("%.2f ", anAnInputMatrix));
      }
      out.println("\n");
    }

    //===================== ВЫВОДИМ МАТРИЦУ ==========
    out.println("\n-------RESULT--------\n Сдвиг вниз на 4 позиции");
    final int MATRIX_SHIFT = 4;
    double[][] resultMatrix = TaskOnePartTwo.matrixShiftDown(inputMatrix, MATRIX_SHIFT);
    for (double[] column : resultMatrix) {
      for (double row : column) {
        out.println(String.format("%.2f ", row));
      }
      out.println("\n");
    }
  }
}
