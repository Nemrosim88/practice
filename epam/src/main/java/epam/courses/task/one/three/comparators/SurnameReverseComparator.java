package epam.courses.task.one.three.comparators;

import epam.courses.task.one.three.model.Subscriber;

import java.util.Comparator;

/**
 * Comparator that will be used for sorting by Subscriber's surnames in reversed way.
 */
public class SurnameReverseComparator implements Comparator<Subscriber> {

  @Override
  public int compare(Subscriber o1, Subscriber o2) {
    return o2.getSurname().compareTo(o1.getSurname());
  }
}
