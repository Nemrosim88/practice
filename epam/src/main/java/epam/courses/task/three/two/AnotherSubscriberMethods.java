package epam.courses.task.three.two;

import epam.courses.task.one.three.comparators.SurnameReverseComparator;
import epam.courses.task.one.three.model.Subscriber;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Task3.pdf -> Part 2 solution class.
 */
public class AnotherSubscriberMethods {

  /**
   * List variable.
   */
  private List<Subscriber> list;

  /**
   * Constructor for SubscriberMethods, that needs list of Subscribers.
   *
   * @param list list of Subscribers
   * @throws NullPointerException if list = null
   */
  public AnotherSubscriberMethods(List<Subscriber> list) throws NullPointerException {
    if (list == null) {
      throw new NullPointerException("Empty list");
    }
    this.list = list;
  }

  /**
   * One way to get needed Subscriber.
   *
   * @param minutesCalls значение времени городских переговоров (в минутах)
   */
  public List<Subscriber> getSubscribersWithCityCallsByMinutes(int minutesCalls) {
    return list.stream()
        .filter(subscriber -> subscriber.getTimeOfCityCalls() >= minutesCalls)
        .collect(Collectors.toList());
  }

  /**
   * Another way to get needed Subscriber. That Subscribers that used it.
   *
   * @return список абонентов которые пользовались международной связью.
   */
  public List<Subscriber> getSubscribersThatUsedLongDistCalls() {
    return list.stream().filter(subscriber -> subscriber.getTimeOfLongDistanceCalls() > 0).collect(
        Collectors.toList());
  }

  /**
   * Another way to get needed Subscriber. By long distance calls. By more.
   *
   * @param minutes minutes for long distance calls
   * @return list of Subscribers
   */
  public List<Subscriber> getSubscribersThatUsedMoreThanLongDistCalls(int minutes) {
    return list.stream().filter(subscriber -> subscriber.getTimeOfLongDistanceCalls() > minutes)
        .collect(
            Collectors.toList());
  }

  /**
   * Another way to get needed Subscriber. Surname Reversed.
   */
  public void sortBySurnameReverse() {
    Collections.sort(list, new SurnameReverseComparator());
  }

  /**
   * Метод сортировки по фамилии.
   */
  public void sortBySurname() {
    list.sort(Comparator.comparing(Subscriber::getSurname));
  }

  /**
   * Метод сортировки по фамилии. Применение анонимного класса.
   */
  public void sortBySurnameAnClass() {
    list.sort(new Comparator<Subscriber>() {
      @Override
      public int compare(Subscriber o1, Subscriber o2) {
        return o1.getSurname().compareTo(o2.getSurname());
      }
    });
  }

  public static void sortBySubscriberName(List<? extends Subscriber> list) {
    Collections
        .sort(list, (Comparator<Subscriber>) (o1, o2) -> o1.getName().compareTo(o2.getName()));

  }

  /**
   * Getter for List of Subscribers.
   */
  public List<Subscriber> getList() {
    return list;
  }

  /**
   * Setter for List of Subscribers.
   */
  public void setList(List<Subscriber> list) {
    this.list = list;
  }

}
