package epam.courses.task.three.one.model;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Class representing solution to task3.pds -> part 1.
 */
public class Continent {

  /**
   * Value of Continent's name.
   */
  private String name;

  /**
   * List of territorial distribution.
   */
  private ArrayList<TerritorialDistribution> territorialDistributionArrayList;

  /**
   * Constructor for continent.
   *
   * @param name name of the continent.
   * @throws NullPointerException will throw this exception if name = NULL.
   */
  public Continent(String name) throws NullPointerException {
    if (name == null) {
      throw new NullPointerException("Value name can't be NULL");
    }
    this.name = name;
    this.territorialDistributionArrayList = new ArrayList<>();
  }

  /**
   * Method returns states in time range between 'from' to 'to' values.
   *
   * @param from year from
   * @param to year to
   * @return returns List of States in time range between 'from' to 'to' values. Otherwise will
   * return NULL.
   */
  public List<State> getStatesInTimeRange(int from, int to) {
    if (!territorialDistributionArrayList.isEmpty()) {
      List<State> resultList = new ArrayList<>();
      for (TerritorialDistribution distribution : territorialDistributionArrayList) {
        for (State state : distribution.stateList) {
          if (state.getStartDate() >= from && state.getEndDate() <= to) {
            resultList.add(state);
          }
        }
      }
      return resultList;
    } else {
      return null;
    }
  }

  /**
   * Was wrote for demonstration and to see how it looks.
   *
   * @return list of states.
   */
  private List<State> generateStateList() {
    List<State> stateList = new ArrayList<>();
    stateList.add(new State(1918, "Австрия"));
    stateList.add(new State(1830, "Бельгия"));
    stateList.add(new State(1581, "Нидерланды"));
    stateList.add(new State(1479, "Испания"));
    stateList.add(new State(431, 843, "Франкское государство"));
    stateList.add(new State(895, 1000, "Венгерское княжество"));
    return stateList;
  }

  /**
   * Method for adding new State to Continent class.
   *
   * @param name name of new State.
   * @param timeFrom time from State exists
   * @param timeTo time 'to' State existed
   * @return true - if state was added, false - if it's not
   */
  public boolean addNewStateToContinent(String name, int timeFrom, int timeTo) {
    for (TerritorialDistribution territorialDistribution : territorialDistributionArrayList) {
      if (timeFrom >= territorialDistribution.startDate
          && timeTo <= territorialDistribution.endDate) {
        return territorialDistribution.stateList.add(new State(timeFrom, timeTo, name));
      }
    }
    return false;

  }

  /**
   * Getter for name of the Continent.
   *
   * @return name of the Continent.
   */
  public String getName() {
    return name;
  }

  /**
   * Setter for name of the Continent.
   *
   * @param name name of the Continent
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Getter for List of territorial distributions of the Continent.
   *
   * @return ArrayList with territorial distributions of the Continent
   */
  public ArrayList<TerritorialDistribution> getTerritorialDistributionArrayList() {
    return territorialDistributionArrayList;
  }

  /**
   * Setter of List of territorial distributions of the Continent.
   *
   * @param territorialDistributionArrayList ArrayList with territorial distributions of the
   * Continent
   */
  public void setTerritorialDistributionArrayList(
      ArrayList<TerritorialDistribution> territorialDistributionArrayList) {
    this.territorialDistributionArrayList = territorialDistributionArrayList;
  }

  /**
   * Inner class of Continent for territorial distribution.
   */
  private class TerritorialDistribution {

    /**
     * Name of territorial distribution.
     */
    private String name;

    /**
     * Start date of territorial distribution.
     */
    private int startDate;

    /**
     * End date of territorial distribution.
     */
    private int endDate;

    /**
     * List of States.
     */
    private ArrayList<State> stateList;

    /**
     * Constructor for territorial distribution. Time validation.
     *
     * @param name name of territorial distribution
     * @param stateList list of States. Time of territorial distribution
     */
    private TerritorialDistribution(String name, ArrayList<State> stateList) {
      int min = Integer.MAX_VALUE;
      int max = Integer.MIN_VALUE;
      for (State state : stateList) {
        int tempMin = state.getStartDate();
        int tempMax = state.getEndDate();
        if (tempMin < min) {
          min = tempMin;
        } else if (tempMax > max) {
          max = tempMax;
        }
      }
      this.name = name;
      this.startDate = min;
      this.endDate = max;
      this.stateList = stateList;
    }

    /**
     * Constructor for territorial distribution. Time validation.
     *
     * @param name name of territorial distribution
     * @param stateList list of States territorial distribution
     * @param timeFrom time of territorial distribution will be set by user
     * @param timeTo time of territorial distribution will be set by user
     */
    private TerritorialDistribution(String name, ArrayList<State> stateList, int timeFrom,
        int timeTo) {
      this.name = name;
      this.stateList = new ArrayList<>();
      for (State state : stateList) {
        if (state.getStartDate() >= timeFrom && state.getEndDate() <= timeTo) {
          this.stateList.add(state);
        }
      }
    }

    /**
     *
     * @param name
     * @param startDate
     * @param endDate
     * @param stateList
     */
    private TerritorialDistribution(String name, int startDate, int endDate,
        ArrayList<State> stateList) {
      this.name = name;
      this.startDate = startDate;
      this.endDate = endDate;
      this.stateList = stateList;
    }
  }

  /**
   * Inner class of Continent for State.
   */
  private class State {

    /**
     *
     */
    private boolean isStill;

    /**
     *
     */
    private int startDate;

    /**
     *
     */
    private int endDate;

    /**
     *
     */
    private String name;

    /**
     * Constructor for State that is no more exists.
     */
    private State(int startDate, int endDate, String name) {
      this.startDate = startDate;
      this.endDate = endDate;
      this.name = name;
      this.isStill = false;
    }

    /**
     * Constructor for State that is still exists.
     *
     * <br>End date will be generated automatically by LocalDate.class
     *
     * @param startDate when state started to exist
     * @param name name of the State
     */
    private State(int startDate, String name) {
      this.endDate = LocalDate.now().getYear();
      this.startDate = startDate;
      this.name = name;
      this.isStill = true;
    }

    /**
     * Getter for value isStill.
     *
     * @return true - if State still exists, false - if it is not
     */
    private boolean isStill() {
      return isStill;
    }

    /**
     * Setter for value isStill.
     */
    private void setStill(boolean still) {
      isStill = still;
    }

    /**
     * Getter for startDate.
     *
     * @return start date of the State
     */
    private int getStartDate() {
      return startDate;
    }

    /**
     * Setter for startDate.
     *
     * @param startDate start date of the State
     */
    private void setStartDate(int startDate) {
      this.startDate = startDate;
    }

    /**
     * Getter for end date.
     *
     * @return end date of the State
     */
    private int getEndDate() {
      return endDate;
    }

    /**
     * Setter for end date.
     *
     * @param endDate end date of the State
     */
    private void setEndDate(int endDate) {
      this.endDate = endDate;
    }

    /**
     * Getter for name of the State.
     *
     * @return name of the State
     */
    private String getName() {
      return name;
    }

    /**
     * Setter for name of the State.
     *
     * @param name name of the State
     */
    private void setName(String name) {
      this.name = name;
    }
  }
}
