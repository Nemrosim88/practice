package epam.courses.task.two.two;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Tests of RegExp class.
 */
public class RegExpTest {


  @Test
  public void matchTextTest() {
    assertTrue(RegExp
        .matchText("asdasd(D,D)asdasdasd(A,D) asdasdasasdasdasdasd\nasd (D,D)asdasd (O,O) asdasd")
        .equals("Found\n"
            + "Start index: 6; End index: 11-> (D,D)\n"
            + "Found\n"
            + "Start index: 20; End index: 25-> (A,D)\n"
            + "Found\n"
            + "Start index: 51; End index: 56-> (D,D)\n"
            + "Found\n"
            + "Start index: 63; End index: 68-> (O,O)\n"));
    assertTrue(RegExp
        .matchText("Мама(A,R)мыла (X,X)раму![c,Q] ывцSDSD,FEW(df,sd)(DF,W)(,DW)(34,54)")
        .equals("Found\n"
            + "Start index: 4; End index: 9-> (A,R)\n"
            + "Found\n"
            + "Start index: 14; End index: 19-> (X,X)\n"));
    assertTrue(RegExp
        .matchText("(A,B)(AA,B)(A,DD)(A.A)[U,U]{E,R}(DD,DD)(,)(F,)(,F)")
        .equals("Found\n"
            + "Start index: 0; End index: 5-> (A,B)\n"));
  }

}
