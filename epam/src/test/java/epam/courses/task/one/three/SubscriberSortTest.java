package epam.courses.task.one.three;

import static org.junit.Assert.assertArrayEquals;

import epam.courses.task.one.three.comparators.SurnameReverseComparator;
import epam.courses.task.one.three.model.Subscriber;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SubscriberSortTest {

  private static List<Subscriber> list = new ArrayList<>(7);

  static {
    list.add(new Subscriber("1", "Art", "Art", "Kiev", 100, 700));
    list.add(new Subscriber("2", "Bart", "Art", "Kiev", 30, 310));
    list.add(new Subscriber("3", "Cart", "Art", "Kiev", 0, 50));
    list.add(new Subscriber("4", "Dart", "Art", "Kiev", 270, 10));
    list.add(new Subscriber("5", "Efa", "Art", "Kiev", 0, 240));
    list.add(new Subscriber("6", "Fgtq", "Art", "Kiev", 32, 12));
    list.add(new Subscriber("7", "Far", "Art", "Kiev", 11, 40));
  }

  /**
   * Проверка правмльности сортировки фамилий в обратном порядке.
   */
  @Test
  public void surnameReverseSortTest() {
    //Сортируем список по фамилии
    Collections.sort(list, new SurnameReverseComparator());

    //Формируем массив с отсортированными фамилиями
    String[] inputArray = new String[7];
    for (int i = 0; i < list.size(); i++) {
      inputArray[i] = list.get(i).getSurname();
    }

    //Было:    Art    Bart   Cart   Dart    Efa     Fgtq    Far
    //Стало: "Fgtq", "Far", "Efa", "Dart", "Cart", "Bart", "Art"
    String[] resultArray = {"Fgtq", "Far", "Efa", "Dart", "Cart", "Bart", "Art"};

    //Проверяем
    assertArrayEquals(inputArray, resultArray);
  }

  /**
   * Проверка извлечения из списка, абонентов которые пользовались междугородней связью.
   */
  @Test
  public void getSubscribersThatUsedLongDistCallsTest() {
    List<Subscriber> methodList = new SubscriberMethods(list).getSubscribersThatUsedLongDistCalls();

    // Тот список который должен быть сформирован.
    // Это те люди которые использовали городскую связь.
    Subscriber[] resultArray = {
        new Subscriber("1", "Art", "Art", "Kiev", 100, 700),
        new Subscriber("2", "Bart", "Art", "Kiev", 30, 310),
        new Subscriber("4", "Dart", "Art", "Kiev", 270, 10),
        new Subscriber("6", "Fgtq", "Art", "Kiev", 32, 12),
        new Subscriber("7", "Far", "Art", "Kiev", 11, 40),
    };
    Subscriber[] inputArray = methodList.toArray(new Subscriber[methodList.size()]);

    assertArrayEquals(inputArray, resultArray);
  }

  /**
   * Извлечение абонентов которые по городскому номеру общались больше заданого значения.
   */
  @Test
  public void getCityCallsTest() {
    List<Subscriber> methodList = new SubscriberMethods(list)
        .getSubscribersWithCityCallsByMinutes(50);
    Subscriber[] subscribers = {
        new Subscriber("1", "Art", "Art", "Kiev", 100, 700),
        new Subscriber("2", "Bart", "Art", "Kiev", 30, 310),
        new Subscriber("3", "Cart", "Art", "Kiev", 0, 50),
        new Subscriber("5", "Efa", "Art", "Kiev", 0, 240),
    };
    Subscriber[] resultList = methodList.toArray(new Subscriber[methodList.size()]);
    assertArrayEquals(resultList, subscribers);
  }

  @Test(expected = NullPointerException.class)
  public void nullPointerTest(){
    new SubscriberMethods(null);
  }


}
