package epam.courses.task.one.one;

import epam.courses.task.one.two.TaskOnePartTwo;
import java.security.InvalidParameterException;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @version 1.1
 */
public class TaskOnePartOneTest {

  @Test
  public void convertToAnotherNumeralSystem_UsingIntegerToString_Hex() {
    // testing hex
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("26", 16), "1a");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("256", 16), "100");
  }

  @Test
  public void convertToAnotherNumeralSystem_UsingIntegerToString_Binary() {
    //testing binary
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("8", 2), "1000");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("१२", 2), "1100");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("345", 2), "101011001");
  }

  @Test
  public void convertToAnotherNumeralSystem_UsingIntegerToString_Octal() {
    //testing octal
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("14", 8), "16");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem("133", 8), "205");
  }


  @Test(expected = NumberFormatException.class)
  public void numberFormatExceptionInConvertToAnotherNumeralSystem() {
    try {
      TaskOnePartOne.convertToAnotherNumeralSystem("3.5", 2);
    } catch (NumberFormatException a) {
      try {
        TaskOnePartOne.convertToAnotherNumeralSystem("-12,56", 2);
      } catch (NumberFormatException b) {
        TaskOnePartOne.convertToAnotherNumeralSystem("Santa", 2);
      }
    }
  }

  @Test(expected = InvalidParameterException.class)
  public void invalidParameterExceptionInConvertToAnotherNumeralSystem() {
    try {
      TaskOnePartOne.convertToAnotherNumeralSystem("20", 333);
    } catch (InvalidParameterException e) {
      assertTrue(e.getMessage().equals("Wrong numeral System"));
      TaskOnePartOne.convertToAnotherNumeralSystem("20", 333);
    }
  }

  @Test
  public void convertToAnotherNumeralSystem_OldSchool_ToBinary() {
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(8, 2), "000001000");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(12, 2), "000001100");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(345, 2), "101011001");
  }

  @Test
  public void convertToAnotherNumeralSystem_OLDSCHOOL_ToOCTAL() {
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(14, 8), "16");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(133, 8), "205");
  }

  @Test
  public void convertToAnotherNumeralSystem_ToHEX_Test() {
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(26, 16), "0000001A");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(256, 16), "00000100");
    assertEquals(TaskOnePartOne.convertToAnotherNumeralSystem(3465299, 16), "0034E053");
  }

  @Test
  public void getPerfectNumberMethodTest() {
    assertEquals(TaskOnePartOne.getPerfectNumber("496"), Arrays.asList(6, 28, 496));
    assertEquals(TaskOnePartOne.getPerfectNumber("१२३"), Arrays.asList(6, 28));
    assertEquals(TaskOnePartOne.getPerfectNumber("2000"), Arrays.asList(6, 28, 496));
    assertEquals(
        TaskOnePartOne.getPerfectNumber("33550336"), Arrays.asList(6, 28, 496, 8128, 33_550_336));
  }

  @Test(expected = NumberFormatException.class)
  public void getExceptionInGetPerfectNumberMethodInputValueIsText() {
    TaskOnePartOne.getPerfectNumber("Hello World!");
  }

  @Test(expected = NumberFormatException.class)
  public void getExceptionInGetPerfectNumberMethodInputValueIsDouble() {
    TaskOnePartOne.getPerfectNumber("496,3");
  }

  @Test
  public void privateIsPrimeMethodTest() {
    assertTrue(getPrivateMethod_isPrime(3L));
    assertTrue(getPrivateMethod_isPrime(5L));
    assertTrue(getPrivateMethod_isPrime(7L));
    assertTrue(getPrivateMethod_isPrime(11L));
    assertTrue(getPrivateMethod_isPrime(13L));
  }

  @Test
  public void privateIsPrimeMethod_NotPrimeNumbersTest() {
    // MUST RETURN FALSE
    assertTrue(!getPrivateMethod_isPrime(14L));
    assertTrue(!getPrivateMethod_isPrime(15L));
    assertTrue(!getPrivateMethod_isPrime(16L));
  }

  @Test
  public void buildPyramidMethod_passingCorrectValuesTest() {
    String resultForFour = "        1\n"
        + "       121\n"
        + "      12321\n"
        + "     1234321\n";
    assertEquals(TaskOnePartOne.buildPyramid(4), resultForFour);

    String resultForSix =
        "        1\n"
            + "       121\n"
            + "      12321\n"
            + "     1234321\n"
            + "    123454321\n"
            + "   12345654321\n";
    assertEquals(TaskOnePartOne.buildPyramid(6), resultForSix);
  }

  @Test(expected = InvalidParameterException.class)
  public void buildPyramidMethod_passingWrongValueTest() {
    //passing wrong value to method test (Correct values are from 1 to 9)
    try {
      TaskOnePartOne.buildPyramid(0);
    } catch (IllegalArgumentException a) {
      try {
        TaskOnePartOne.buildPyramid(10);
      } catch (IllegalArgumentException b) {
        try {
          TaskOnePartOne.buildPyramid(Integer.MIN_VALUE);
        } catch (IllegalArgumentException c) {
          TaskOnePartOne.buildPyramid(Integer.MAX_VALUE);
        }
      }
    }
  }

  /**
   * Метод для вызова приватного статического метода isPrime из класса TaskOnePartOne
   *
   * @param params параметры которые передаются методу isPrime
   * @return возвращает Boolean полученный от работы отработанного метода isPrime
   * @see Method
   * @see Constructor
   * @see Method#invoke(Object, Object...)
   */
  private Boolean getPrivateMethod_isPrime(long params) {
    try {
      Method privateMethod = (TaskOnePartOne.class).getDeclaredMethod("isPrime", long.class);
      Constructor constructor = (TaskOnePartOne.class).getDeclaredConstructor();
      constructor.setAccessible(true);
      privateMethod.setAccessible(true);
      return (Boolean) privateMethod.invoke(constructor.newInstance(), params);
    } catch (NoSuchMethodException
        | IllegalAccessException
        | InstantiationException
        | InvocationTargetException e) {
      e.printStackTrace();
      return false;
    }
  }
}
