package epam.courses.task.one.two;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class TaskOnePartTwoTest {
  @Test
  public void matrixShiftDownMethodTest() {
    double[][] input = {
        {1, 2, 3, 4, 5, 6},
        {2, 3, 4, 5, 6, 7},
        {8, 7, 6, 5, 4, 4},
        {3, 6, 4, 8, 1, 4},
        {0, 6, 1, 8, 3, 5},
        {1, 8, 2, 0, 3, 2},};
    //Bad input test
    assertArrayEquals(TaskOnePartTwo.matrixShiftDown(input, 234), new double[0][0]);

    //one test, shift 3
    double[][] firstResult = {
        {3, 6, 4, 8, 1, 4},
        {0, 6, 1, 8, 3, 5},
        {1, 8, 2, 0, 3, 2},
        {1, 2, 3, 4, 5, 6},
        {2, 3, 4, 5, 6, 7},
        {8, 7, 6, 5, 4, 4},};
    assertArrayEquals(TaskOnePartTwo.matrixShiftDown(input, 3), firstResult);
    //two test, shift 5
    double[][] secondResult = {
        {2, 3, 4, 5, 6, 7},
        {8, 7, 6, 5, 4, 4},
        {3, 6, 4, 8, 1, 4},
        {0, 6, 1, 8, 3, 5},
        {1, 8, 2, 0, 3, 2},
        {1, 2, 3, 4, 5, 6},};
    assertArrayEquals(TaskOnePartTwo.matrixShiftDown(input, 5), secondResult);
    //taskThree test, shift 6. Will get original array
    double[][] thirdResult = {
        {1, 2, 3, 4, 5, 6},
        {2, 3, 4, 5, 6, 7},
        {8, 7, 6, 5, 4, 4},
        {3, 6, 4, 8, 1, 4},
        {0, 6, 1, 8, 3, 5},
        {1, 8, 2, 0, 3, 2},};
    assertArrayEquals(TaskOnePartTwo.matrixShiftDown(input, 6), thirdResult);
  }

}
