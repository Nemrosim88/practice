package epam.courses.task.six;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(value = Parameterized.class)
public class TemperatureConverterTest {

  private final double input;
  private final double valueForFtoC;
  private final double valueForCtoF;
  private final double valueForCtoK;
  private final double valueForKtoC;
  private final double valueForFtoK;
  private final double valueForKtoF;
  private TemperatureConverter TC;

  /**
   * Конструктор для параметризации.
   * Constructor for parametrization.
   *
   * @param input        переменная, получаемая методом на вход
   * @param valueForFtoC ожидаемый результат для метода convertFtoC();
   * @param valueForCtoF ожидаемый результат для метода convertCtoF();
   * @param valueForCtoK ожидаемый результат для метода convertCtoK();
   * @param valueForKtoC ожидаемый результат для метода convertKtoC();
   * @param valueForFtoK ожидаемый результат для метода convertFtoK();
   * @param valueForKtoF ожидаемый результат для метода convertKtoF();
   */
  public TemperatureConverterTest(double input,
                                  double valueForFtoC,
                                  double valueForCtoF,
                                  double valueForCtoK,
                                  double valueForKtoC,
                                  double valueForFtoK,
                                  double valueForKtoF) {
    this.input = input;
    this.valueForFtoC = valueForFtoC;
    this.valueForCtoF = valueForCtoF;
    this.valueForCtoK = valueForCtoK;
    this.valueForKtoC = valueForKtoC;
    this.valueForFtoK = valueForFtoK;
    this.valueForKtoF = valueForKtoF;
  }

  @Before
  public void getTc() {
    TC = new TemperatureConverter();
  }

  @Parameters
  public static Collection<Double[]> getTestParameters() {
    return Arrays.asList(new Double[][] {

        {0.0,        // input
            -17.777, // value For F to C +
            32.0,    // value For C to F +
            273.2,   // value For C to K + 273,15
            -273.2,  // value For K to C + -273,15
            255.388, // value For F to K + 255.372
            -459.7   // value For K to F + -459,67
        },
        {234.0,      // input
            112.222, // valueForFtoC +
            453.2,   // valueForCtoF +
            507.2,   // valueForCtoK +
            -39.199, // valueForKtoC + 39,15
            385.388, // valueForFtoK + 385,372
            -38.5    // valueForKtoF + 38,47
        },
        {-129.45,     // input
            -89.6944, // valueForFtoC
            -201.0099,// valueForCtoF
            143.75,   // valueForCtoK
            -402.65,  // valueForKtoC
            183.472,  // valueForFtoK
            -692.7099 // valueForKtoF
        },
        {10.0,       // input
            -12.222, // valueForFtoC
            50.0,    // valueForCtoF
            283.2,   // valueForCtoK
            -263.2,  // valueForKtoC
            260.944, // valueForFtoK
            -441.7   // valueForKtoF
        },
    });
  }


  @Test
  public void convertFtoC() throws Exception {
    assertEquals("Wrong result in convertFtoC()",
        TC.convertFtoC(input), valueForFtoC, 0.001);
  }

  @Test
  public void convertCtoF() throws Exception {
    assertEquals("Wrong result in convertCtoF()",
        TC.convertCtoF(input), valueForCtoF, 0.001);
  }

  @Test
  public void convertCtoK() throws Exception {
    assertEquals("Wrong result in convertCtoK()",
        TC.convertCtoK(input), valueForCtoK, 0.001);
  }

  @Test
  public void convertKtoC() throws Exception {
    assertEquals("Wrong result in convertKtoC()",
        TC.convertKtoC(input), valueForKtoC, 0.001);
  }

  @Test
  public void convertFtoK() throws Exception {
    assertEquals("Wrong result in convertFtoK()",
        TC.convertFtoK(input), valueForFtoK, 0.001);
  }

  @Test
  public void convertKtoF() throws Exception {
    assertEquals("Wrong result in convertKtoF()",
        TC.convertKtoF(input), valueForKtoF, 0.001);
  }

}