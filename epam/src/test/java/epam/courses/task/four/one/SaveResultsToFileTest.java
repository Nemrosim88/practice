package epam.courses.task.four.one;

import epam.courses.task.one.three.model.Subscriber;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;

public class SaveResultsToFileTest {

  /**
   * Массив студентов
   */
  private Subscriber[] array = {
      new Subscriber("1", "Art", "Art", "Kiev", 100, 700),
      new Subscriber("2", "Bart", "Art", "Kiev", 30, 310),
      new Subscriber("3", "Cart", "Art", "Kiev", 0, 50),
      new Subscriber("4", "Dart", "Art", "Kiev", 270, 10),
      new Subscriber("5", "Efa", "Art", "Kiev", 0, 240),
      new Subscriber("6", "Fgtq", "Art", "Kiev", 32, 12),
      new Subscriber("7", "Far", "Art", "Kiev", 11, 40),};

  /**
   * Список студентов
   */
  private List<Subscriber> list = Arrays.asList(array);

  /**
   * Сохранение списка абонентов в файл.
   *
   * @throws Exception
   */
  @Before
  public void saveToFile() throws Exception {
    SaveResultsToFile.saveToFile("subscribers.obj", list);
  }

  /**
   * Пробуем записать файл второй раз. Всё ок, ошибки не будет.
   *
   * @throws Exception
   */
  @Test
  public void saveToFileSecondChance() throws Exception {
    SaveResultsToFile.saveToFile("subscribers.obj", list);
  }

  /**
   * Чтение списка абонентов из файла.
   *
   * @throws Exception
   */
  @Test
  public void readFromFileOfSubscribers() throws Exception {
    List<Subscriber> subscribers = SaveResultsToFile.readFromFileOfSubscribers("subscribers.obj");
    Subscriber[] result = subscribers.toArray(new Subscriber[0]);
    assertArrayEquals("Потеряли студентов", array, result);
  }

  /**
   * Проверка на наличие ошибки, при попытке чтнения несуществующего файла.
   *
   * @throws Exception
   */
  @Test(expected = FileNotFoundException.class)
  public void readFromWrongFile() throws Exception {
    SaveResultsToFile.readFromFileOfSubscribers("wrong.file");
  }

  /**
   * Удаляет рабочие файлы.
   */
//  @After
  public void deleteFiles() {
    String path = Paths.get("").toAbsolutePath().toString() + "\\" + "subscribers.obj";
    File file = new File(path);
    if (file.exists()) {
      file.delete();
    }
  }

}