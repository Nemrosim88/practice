<%@ page import="business.User" %>
<%@page contentType="text/html" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Murach's Java Servlets and JSP</title>
    <link rel="stylesheet" href="styles/main.css" type="text/css"/>
</head>

<body>
<h1>Thanks for joining our main.java.email list</h1>

<p>Here is the information that you entered:</p>
<%--Один из вариантов--%>
<%
    User user = (User) request.getAttribute("user");
    if (user == null) {
        user = new User();
    }
%>
<label>Email:</label>

<%--Простой вариант Using EL --%>
<span>${user.email}</span><br>
<label>First Name:</label>

<%--Один из вариантов--%>
<span><%=user.getName()%></span><br>
<label>Last Name:</label>

<%--Ещё один из вариантов Standart JSP tags--%>
<jsp:useBean id="user" scope="request" class="business.User">
<span><jsp:getProperty name="user" property="surname"></span><br>

<p>To enter another main.java.email address, click on the Back
    button in your browser or the Return button shown
    below.</p>

<form action="" method="get">
    <input type="hidden" name="action" value="join">
    <input type="submit" value="Return">
</form>

</body>
</html>