<%--
  Created by IntelliJ IDEA.
  User: Nemrosim
  Date: 18.09.2017
  Time: 21:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Hospital page">
    <meta name="author" content="Nemrosim">
    <link rel="icon" href="img/logo_min.gif">

    <title>Hospital JSP course project</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/carousel.css" rel="stylesheet">
</head>
<body>


<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a class="navbar-brand" href="#">Carousel</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="custom.jsp">Custom.jsp<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="doctor.jsp">Link</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link disabled" href="#">Disabled</a>
            </li>
        </ul>

            <label style="color: red">${message}</label>

        <form class="form-inline mt-2 mt-md-0" action="login" method="post">
            <input type="hidden" name="loginForm" value="login">
            <c:choose>
                <c:when test="${login != null}">
                    <input class="form-control mr-sm-2" type="text" name="login" placeholder="${login}"
                           aria-label="Search">
                </c:when>
                <c:otherwise>
                    <input class="form-control mr-sm-2" type="text" name="login" placeholder="Login"
                           aria-label="Search">
                </c:otherwise>
            </c:choose>

            <input class="form-control mr-sm-2" type="text" name="password" placeholder="Password"
                   aria-label="Password">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">LOGIN</button>
        </form>
    </div>
</nav>


<div class="container" style="width: 1310px">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img class="first-slide" src="img/room_modOne.jpg" alt="First slide">
                <div class="container">
                    <div class="carousel-caption d-none d-md-block" style="margin-bottom: 40px">
                        <h1>Лучшие больничные палаты</h1>
                        <p>Уникальные smart-палаты оборудованы системами дистанционного управления,
                            действующими по принципу «умного дома».
                            Палаты сделаны по стандартам высококлассного пятизвездочного отеля с участием
                            профессиональных дизайнеров.</p>
                        <p><a class="btn btn-lg btn-primary" href="#rooms" role="button">Узнать больше</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="second-slide" src="img/operation_modTwo.jpg" alt="Second slide">
                <div class="container">
                    <div class="carousel-caption d-none d-md-block" style="margin-bottom: 40px">
                        <h1>Лучшие операционные палаты</h1>
                        <p>Новые помещения включают в себя технологии следующего поколения,
                            адаптированные под медицинские нужды: сенсорные панели, видеокамеры и другое
                            оборудование для сбора данных о пациенте и помощи хирургической бригаде.</p>
                        <p><a class="btn btn-lg btn-primary" href="#operation" role="button">Узнать больше</a></p>
                    </div>
                </div>
            </div>
            <div class="carousel-item">
                <img class="third-slide"
                     src="img/best-medical-equipment-min.jpg"
                     alt="Third slide">
                <div class="container">
                    <div class="carousel-caption d-none d-md-block" style="margin-bottom: 40px">
                        <h1>Комплексное оснащение лабораторий</h1>
                        <p>Широкий ассортимент нового медицинского оборудования ведущих мировых производителей.
                            За время деятельности накоплен большой опыт работы как с крупными зарубежными
                            и отечественными компаниями, так и с небольшими фирмами и частными предпринимателями.</p>
                        <p><a class="btn btn-lg btn-primary" href="#equipment" role="button">Browse gallery</a></p>
                    </div>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

    <div class="container marketing">

        <!-- Three columns of text below the carousel -->
        <div class="row">


            <div class="col-lg-4">
                <img class="rounded-circle"
                     src="img/doctor_House_min.jpg"
                     alt="Generic placeholder image" width="140" height="140">
                <h2>Доктор Грегори Хаус</h2>
                <p>Блестящий диагност со специализацией в двух основных
                    областях: заболевания почек (нефрология) и инфекционные болезни.</p>
                <p><a class="btn btn-secondary" href="https://ru.wikipedia.org/wiki/Доктор_Хаус" role="button">Узнать
                    больше</a></p>
            </div><!-- /.col-lg-4 -->


            <div class="col-lg-4">
                <img class="rounded-circle"
                     src="img/dr-patch-adams_min.jpg"
                     alt="Generic placeholder image" width="140" height="140">
                <h2>Докто Патч Адамс</h2>
                <p>Врач, общественный деятель, больничный клоун и писатель.
                    Более всего известен своей деятельностью в качестве больничного клоуна
                    и считается одним из создателей данного явления.</p>
                <p><a class="btn btn-secondary" href="https://ru.wikipedia.org/wiki/Патч_Адамс" role="button">Узнать
                    больше</a></p>
            </div><!-- /.col-lg-4 -->


            <div class="col-lg-4">
                <img class="rounded-circle"
                     src="img/doctor_strange-min.jpg"
                     alt="Generic placeholder image" width="140" height="140">
                <h2>Доктор Стивен Стрэндж</h2>
                <p>Блестящий, но эгоистичный хирург. Практик как мистических, так и боевых искусств.</p>
                <p><a class="btn btn-secondary" href="https://ru.wikipedia.org/wiki/Доктор_Стрэндж" role="button">Узнать
                    больше</a></p>
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->


        <!-- START THE FEATURETTES -->

        <hr id="rooms" class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">Тут должна быть информация о комнатах. <span
                        class="text-muted">It'll blow your mind.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Галерея</a></p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" src="img/rooms_500x500.jpg"
                     alt="Generic placeholder image">
            </div>
        </div>


        <hr id="operation" class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7 order-md-2">
                <h2 class="featurette-heading">Oh yeah, it's that good. <span
                        class="text-muted">See for yourself.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Галерея</a></p>
            </div>
            <div class="col-md-5 order-md-1">
                <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
                     alt="Generic placeholder image">
            </div>
        </div>


        <hr id="equipment" class="featurette-divider">

        <div class="row featurette">
            <div class="col-md-7">
                <h2 class="featurette-heading">And lastly, this one. <span class="text-muted">Checkmate.</span></h2>
                <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis
                    euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus,
                    tellus ac cursus commodo.</p>
                <p><a class="btn btn-lg btn-primary" href="#" role="button">Галерея</a></p>
            </div>
            <div class="col-md-5">
                <img class="featurette-image img-fluid mx-auto" data-src="holder.js/500x500/auto"
                     alt="Generic placeholder image">
            </div>
        </div>

        <hr class="featurette-divider">

        <!-- /END THE FEATURETTES -->


        <!-- FOOTER -->
        <footer>
            <p class="float-right"><a href="#">Back to top</a></p>
            <p>&copy; 2017 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
        </footer>

    </div><!-- /.container -->
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-3.2.1.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/jquery-3.2.1.min.js"><\/script>')</script>
<script src="js/popper/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
<script src="js/holder/holder.min.js"></script>

</body>
</html>

