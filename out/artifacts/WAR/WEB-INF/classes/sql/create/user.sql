CREATE TABLE "hospital".`user` (
  `id`         INT(11)                                      NOT NULL AUTO_INCREMENT,
  `surname`    VARCHAR(45)                                  NOT NULL,
  `name`       VARCHAR(45)                                  NOT NULL,
  `patronymic` VARCHAR(45)                                  NOT NULL,
  `dayOfBirth` DATE                                         NOT NULL,
  `role`       ENUM ('admin', 'doctor', 'staff', 'patient') NOT NULL,
  PRIMARY KEY (`id`),

)