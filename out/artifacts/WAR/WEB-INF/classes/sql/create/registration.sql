CREATE TABLE "hospital".`registration` (
  `userId`   INT(11)     NOT NULL,
  `login`    VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  CONSTRAINT `registrationUserId` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)