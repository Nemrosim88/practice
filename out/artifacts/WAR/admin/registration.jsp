<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Flat HTML5/CSS3 Login Form</title>

    <link rel="stylesheet"
          href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css"
          integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M"
          crossorigin="anonymous">
    <link rel="stylesheet" href="../css/style.css">


</head>

<body>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="login-page">
    <div class="form">
        <%-- action="register" -> <url-pattern>/register</url-pattern> in web.xml --%>
        <form class="register-form" action="register" method="post">

            <input type="hidden" name="register" value="add">

            <label>${message}</label>
            <c:choose>
                <c:when test="${nickname != null}">
                    <input type="text" name="nickname" placeholder="${nickname}" required/>
                </c:when>
                <c:otherwise>
                    <input type="text" name="nickname" placeholder="Логин" required/>
                </c:otherwise>
            </c:choose>
            <input type="password" name="password" placeholder="Пароль" required/>

            <input type="text" name="name" placeholder="Имя" required/>
            <input type="text" name="surname" placeholder="Фамилия" required/>
            <input type="text" name="patronymic" placeholder="Отчество" required/>

            <input type="text" name="passport" placeholder="Паспорт" required/>
            <input type="email" name="email" placeholder="Email адрес"/>
            <input type="text" name="phoneNumber" placeholder="Мобильный номер"/>
            <input type="text" name="additionalInformation" placeholder="Дополнительная информация"/>

            <p>
                <input type="radio" class="radio" name="radioForTable" value="patient" id="radioOne"
                       checked/>
                <label for="radioOne">Пациент</label>
                <input type="radio" class="radio" name="radioForTable" value="doctor"
                       id="radioTwo"/>
                <label for="radioTwo">Доктор</label>
                <input type="radio" class="radio" name="radioForTable" value="nurse"
                       id="radioThree"/>
                <label for="radioThree">Медсестра</label></p>

            <!--<input type="submit" value="create" class="buttonForm">-->

            <button>create</button>
        </form>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
        integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
        crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js"
        integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1"
        crossorigin="anonymous"></script>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="../js/index.js"></script>

</body>
</html>
