package apress.IteratorPattern.aggregate;
import iterator.*;

public interface ISubject 
{
	IIterator CreateIterator();
}
