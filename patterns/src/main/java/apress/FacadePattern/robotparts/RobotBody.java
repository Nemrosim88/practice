package apress.FacadePattern.robotparts;

public class RobotBody 
{
	public void CreateBody() 
	{		
		System.out.println("Body Creation done");
	}
}
