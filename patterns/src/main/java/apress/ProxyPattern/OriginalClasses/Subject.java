package apress.ProxyPattern.OriginalClasses;

public abstract class Subject
{
    public abstract void doSomeWork();
}
