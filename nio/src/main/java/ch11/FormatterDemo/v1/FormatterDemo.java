package ch11.FormatterDemo.v1;

import java.util.Formatter;

public class FormatterDemo {

  public static void main(String[] args) {
    Formatter formatter = new Formatter();
    formatter.format("1 -> %d\n", 123);
    System.out.println(formatter.toString());
    formatter.format("2 -> %x\n", 123);
    System.out.println(formatter.toString());
    formatter.format("3 -> %c\n", 'X');
    System.out.println(formatter.toString());
    formatter.format("4 -> %f\n", 0.1);
    System.out.println(formatter.toString());
    formatter.format("5 -> %s%n", "Hello, World");
    System.out.println(formatter.toString());
    formatter.format("6 -> %10.2f\n", 98.375);
    System.out.println(formatter.toString());
    formatter.format("7 -> %05d\n", 123);
    System.out.println(formatter.toString());
    formatter.format("8 -> %1$d %1$d\n", 123);
    System.out.println(formatter.toString());
    formatter.format("9 -> %d %d\n", 123, 45);
    System.out.println(formatter.toString());
    formatter.close();
  }
}