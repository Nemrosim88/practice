

/**
 * A simple calculator that we are going to test.
 *
 * @version $Id$
 */
public class Calculator {

  public int squareRoot(int i) {
    return (int) Math.sqrt(i);
  }

  public int sum(int a, int b) {
    return a + b;
  }
}
