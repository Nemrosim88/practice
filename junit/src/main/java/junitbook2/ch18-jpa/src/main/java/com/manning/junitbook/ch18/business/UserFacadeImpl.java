package com.manning.junitbook.ch18.business;

import java.util.List;

import com.manning.junitbook.ch18.dao.UserDao;
import com.manning.junitbook.ch18.model.Telephone;
import com.manning.junitbook.ch18.model.User;
import com.manning.junitbook.ch18.model.UserDto;

public class UserFacadeImpl implements UserFacade {

  private static final String TELEPHONE_STRING_FORMAT = "%s (%s)";
  private UserDao userDao;

  void setUserDao(UserDao userDao) {
    this.userDao = userDao;
  }

  UserDao getUserDao() {
    return userDao;
  }

  public UserDto getUserById(long id) {
    User user = userDao.getUserById(id);
    if (user == null) {
      return null;
    }
    UserDto dto = new UserDto();
    dto.setFirstName(user.getFirstName());
    dto.setLastName(user.getLastName());
    dto.setUsername(user.getUsername());
    List<String> telephoneDtos = dto.getTelephones();
    for (Telephone telephone : user.getTelephones()) {
      String telephoneDto = String
          .format(TELEPHONE_STRING_FORMAT, telephone.getNumber(), telephone.getType());
      telephoneDtos.add(telephoneDto);
    }
    return dto;
  }

}
