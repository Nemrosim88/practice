package com.manning.junitbook.ch18.business;

import com.manning.junitbook.ch18.model.UserDto;

public interface UserFacade {

  UserDto getUserById(long id);

}
