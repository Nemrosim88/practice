package com.manning.junitbook.ch18.dao;

import com.manning.junitbook.ch18.model.User;

public interface UserDao {

  void addUser(User user);

  User getUserById(long id);

  void deleteUser(long id);

}
