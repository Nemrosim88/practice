package com.manning.junitbook.ch18.business;

import static org.easymock.EasyMock.*;
import static com.manning.junitbook.ch18.model.EntitiesHelper.*;
import static junit.framework.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.manning.junitbook.ch18.dao.UserDao;
import com.manning.junitbook.ch18.model.User;
import com.manning.junitbook.ch18.model.UserDto;

public class UserFacadeImplTest {

  private UserFacadeImpl facade;
  private UserDao dao;

  @Before
  public void setFixtures() {
    facade = new UserFacadeImpl();
    dao = createMock(UserDao.class);
    facade.setUserDao(dao);
  }

  @Test
  public void testGetUserByIdUnkownId() {
    int id = 666;
    expect(dao.getUserById(id)).andReturn(null);
    replay(dao);
    UserDto dto = facade.getUserById(id);
    assertNull(dto);
//    verify(dao);   // not necessary
  }

  @Test
  public void testGetUserById() {
    int id = 666;
    User user = newUserWithTelephones();
    expect(dao.getUserById(id)).andReturn(user);
    replay(dao);
    UserDto dto = facade.getUserById(id);
    assertUser(dto);
//    verify(dao);   // not necessary
  }

}
