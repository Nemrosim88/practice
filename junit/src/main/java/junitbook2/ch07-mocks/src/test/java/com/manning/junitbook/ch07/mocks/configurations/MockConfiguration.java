package com.manning.junitbook.ch07.mocks.configurations;

/**
 * Mock implementation of the configuration interface.
 *
 * @version $Id: MockConfiguration.java 505 2009-08-16 17:58:38Z paranoid12 $
 */
public class MockConfiguration implements Configuration {

  /**
   * Sets the sql query.
   */
  public void setSQL(String sqlString) {
  }

  /**
   * Gets the sql query.
   */
  public String getSQL(String sqlString) {
    return null;
  }

}
