package com.manning.junitbook.ch07.mocks.web;

import java.io.InputStream;

/**
 * Mock implementation of the ConnectionFactory interface.
 *
 * @version $Id: MockConnectionFactory.java 505 2009-08-16 17:58:38Z paranoid12 $
 */
public class MockConnectionFactory implements ConnectionFactory {

  /**
   * The input stream for the connection.
   */
  private InputStream inputStream;

  /**
   * Set the input stream.
   */
  public void setData(InputStream stream) {
    this.inputStream = stream;
  }

  /**
   * Get the input stream.
   */
  public InputStream getData() throws Exception {
    return this.inputStream;
  }
}
