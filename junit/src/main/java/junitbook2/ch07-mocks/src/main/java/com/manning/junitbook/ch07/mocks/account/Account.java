package com.manning.junitbook.ch07.mocks.account;

/**
 * Account POJO to hold the bank account object.
 *
 * @version $Id: Account.java 503 2009-08-16 17:47:12Z paranoid12 $
 */
public class Account {

  /**
   * The id of the account.
   */
  private String accountId;

  /**
   * The balance of the account.
   */
  private long balance;

  /**
   * A constructor.
   */
  public Account(String accountId, long initialBalance) {
    this.accountId = accountId;
    this.balance = initialBalance;
  }

  /**
   * Withdraw the amount from the account.
   */
  public void debit(long amount) {
    this.balance -= amount;
  }

  /**
   * Add the amount of money in the account.
   */
  public void credit(long amount) {
    this.balance += amount;
  }

  /**
   * What's the balance of the account?
   */
  public long getBalance() {
    return this.balance;
  }
}
