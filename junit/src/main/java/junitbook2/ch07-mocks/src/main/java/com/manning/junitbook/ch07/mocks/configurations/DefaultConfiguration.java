package com.manning.junitbook.ch07.mocks.configurations;

public class DefaultConfiguration implements Configuration {

  /**
   * Constructor.
   */
  public DefaultConfiguration(String configurationName) {
  }

  /**
   * Getter method to get the sql that we want to execute.
   */
  public String getSQL(String sqlString) {
    return null;
  }
}
