package com.manning.junitbook.ch07.mocks.web;

import java.io.InputStream;

/**
 * A connection factory interface. Different connection factories that we have, must implement this
 * interface.
 *
 * @version $Id: ConnectionFactory.java 503 2009-08-16 17:47:12Z paranoid12 $
 */
public interface ConnectionFactory {

  /**
   * Read the main.java.data from the connection.
   */
  InputStream getData() throws Exception;
}