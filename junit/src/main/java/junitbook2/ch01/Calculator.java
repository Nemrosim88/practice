package junitbook2.ch01;

/**
 * A sample calculator that we are going to test.
 *
 * @version $Id$
 */
public class Calculator {
  public double add(double number1, double number2) {
    return number1 + number2;
  }
}
