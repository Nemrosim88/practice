package junitbook2.ch01;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Test-case for the calculator program.
 *
 * @version $Id$
 */
public class CalculatorTest {

  @Test
  public void add() {
    Calculator calculator = new Calculator();
    double result = calculator.add(10, 50);
    assertEquals(60, result, 0);
  }
}
