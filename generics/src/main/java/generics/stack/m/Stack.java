package generics.stack.m

interface Stack<E> {
    public boolean empty();
    public void push(E elt);
    public E pop();
}
