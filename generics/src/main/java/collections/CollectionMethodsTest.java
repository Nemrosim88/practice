package collections;

import java.util.*;

public class CollectionMethodsTest {

  public static void main(String[] args) {
    StaticTestData.mondayTasks.add(new PhoneTask("Ruth", "567 1234"));

    assert StaticTestData.mondayTasks.toString().equals(
        "[code logic, phone Mike, phone Ruth]");

    Collection<Task> allTasks = new ArrayList<Task>(StaticTestData.mondayTasks);
    allTasks.addAll(StaticTestData.tuesdayTasks);

    assert allTasks.toString().equals(
        "[code logic, phone Mike, phone Ruth, code db, code gui, phone Paul]");

    boolean wasPresent = StaticTestData.mondayTasks.remove(StaticTestData.mikePhone);
    assert wasPresent;
    assert StaticTestData.mondayTasks.toString().equals("[code logic, phone Ruth]");
    StaticTestData.mondayTasks.clear();
    assert StaticTestData.mondayTasks.toString().equals("[]");
    Collection<Task> tuesdayNonphoneTasks = new ArrayList<Task>(StaticTestData.tuesdayTasks);
    tuesdayNonphoneTasks.removeAll(StaticTestData.phoneTasks);
    assert tuesdayNonphoneTasks.toString().equals("[code db, code gui]");
    Collection<Task> phoneTuesdayTasks = new ArrayList<Task>(StaticTestData.tuesdayTasks);
    phoneTuesdayTasks.retainAll(StaticTestData.phoneTasks);
    assert phoneTuesdayTasks.toString().equals("[phone Paul]");
    Collection<PhoneTask> tuesdayPhoneTasks =
        new ArrayList<PhoneTask>(StaticTestData.phoneTasks);
    tuesdayPhoneTasks.retainAll(StaticTestData.tuesdayTasks);
    assert tuesdayPhoneTasks.toString().equals("[phone Paul]");
    assert tuesdayPhoneTasks.contains(StaticTestData.paulPhone);
    assert StaticTestData.tuesdayTasks.containsAll(tuesdayPhoneTasks);
    assert StaticTestData.mondayTasks.isEmpty();
    assert StaticTestData.mondayTasks.size() == 0;
// throws ConcurrentModificationException
    for (Task t : StaticTestData.tuesdayTasks) {
      if (t instanceof PhoneTask) {
        StaticTestData.tuesdayTasks.remove(t);
      }
    }
// throws ConcurrentModificationException
    for (Iterator<Task> it = StaticTestData.tuesdayTasks.iterator(); it.hasNext(); ) {
      Task t = it.next();
      if (t instanceof PhoneTask) {
        StaticTestData.tuesdayTasks.remove(t);
      }
    }
    for (Iterator<Task> it = StaticTestData.tuesdayTasks.iterator(); it.hasNext(); ) {
      Task t = it.next();
      if (t instanceof PhoneTask) {
        it.remove();
      }
    }
  }
}