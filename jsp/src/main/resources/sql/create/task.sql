CREATE TABLE hospital.task (
  id        INT(11)                                 NOT NULL AUTO_INCREMENT,
  doctorId  INT(11)                                 NOT NULL,
  patientId INT(11)                                 NOT NULL,
  info      VARCHAR(2000)                           NOT NULL,
  datetime  DATETIME                                NOT NULL,
  type      ENUM ('drug', 'procedure', 'operation') NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT taskDoctorId FOREIGN KEY (doctorId) REFERENCES doctor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT taskPatientId FOREIGN KEY (patientId) REFERENCES patient (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)