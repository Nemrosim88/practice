CREATE TABLE hospital.diagnosis (
  id             INT(11)  NOT NULL AUTO_INCREMENT,
  patientId      INT(11)  NOT NULL,
  doctorId       INT(11)  NOT NULL,
  info           VARCHAR(2000)     DEFAULT NULL,
  start          DATETIME NOT NULL,
  end            DATETIME          DEFAULT NULL,
  finalDiagnosis VARCHAR(1000)     DEFAULT NULL,
  PRIMARY KEY (id),
  CONSTRAINT diagnosisDoctorId FOREIGN KEY (doctorId) REFERENCES doctor (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT diagnosisPatientId FOREIGN KEY (patientId) REFERENCES patient (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)