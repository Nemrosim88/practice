CREATE TABLE hospital.credentials (
  userID      INT(11)    NOT NULL,
  passport    VARCHAR(8) NOT NULL,
  email       VARCHAR(45) DEFAULT NULL,
  phoneNumber VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (userID),
  CONSTRAINT credentialsUserId FOREIGN KEY (userID) REFERENCES user (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
)