package controller;

import business.Credentials;
import business.Registration;
import business.User;
import business.enums.Role;
import data.ConnectionPool;
import data.DBUtil;
import data.UserDB;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddNewUser extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    doPost(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String form = request.getParameter("formName");
    String surname = request.getParameter("surname");
    String name = request.getParameter("name");
    String patronymic = request.getParameter("patronymic");
    Date dayOfBirth = Date.valueOf(request.getParameter("dayOfBirth"));
    Role role = Role.ofString(request.getParameter("role"));
    String passport = request.getParameter("passport");
    String email = request.getParameter("email");
    String phoneNumber = request.getParameter("phoneNumber");
    String login = request.getParameter("login");
    String password = request.getParameter("password");

    User userFromRequest = new User();
    userFromRequest.setSurname(surname);
    userFromRequest.setName(name);
    userFromRequest.setPatronymic(patronymic);
    userFromRequest.setDayOfBirth(dayOfBirth);
    userFromRequest.setRole(role);

    Credentials credentials = new Credentials();
    credentials.setEmail(email);
    credentials.setPassport(passport);
    credentials.setPhoneNumber(phoneNumber);
    userFromRequest.setCredentials(credentials);

    Registration registration = new Registration();
    registration.setLogin(login);
    registration.setPassword(password);
    userFromRequest.setRegistration(registration);

    System.out.println(form);
    System.out.println(surname);
    System.out.println(name);
    System.out.println(patronymic);
    System.out.println(dayOfBirth);
    System.out.println(role);
    System.out.println(passport);
    System.out.println(email);
    System.out.println(phoneNumber);
    System.out.println(login);
    System.out.println(password);

    Connection connection = ConnectionPool.getInstance().getConnection();

    if (form.equals("addNewUser")) {

      if (UserDB.isLoginExists(connection, login)) {
        request.setAttribute("loginMessage", "Пользователь с таким логином уже существует");
        request.setAttribute("user", userFromRequest);
        ServletContext sc = this.getServletContext();
        sc.getRequestDispatcher("/admin.jsp").forward(request, response);
      } else if (UserDB.isPassportExists(connection, passport)) {
        request.setAttribute("passportMessage", "Пользователь с таким паспортом уже существует");
        request.setAttribute("user", userFromRequest);
        ServletContext sc = this.getServletContext();
        sc.getRequestDispatcher("/admin.jsp").forward(request, response);
      } else {
        Integer userId = UserDB.insert(connection, userFromRequest);
        System.out.println(userId);
        request.setAttribute("userId", userId);
        ServletContext sc = this.getServletContext();
        sc.getRequestDispatcher("/admin.jsp").forward(request, response);
      }

    } else {

      System.out.println("Что-то не так");
    }


  }
}
