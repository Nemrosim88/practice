package controller;

import business.User;
import business.enums.Role;
import data.ConnectionPool;
import data.DBUtil;
import data.UserDB;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Login extends HttpServlet {

  private static final String FORM_PARAM = "loginForm";
  private static final String LOGIN_PARAM = "login";
  private static final String PASSWORD_PARAM = "password";
  private static final String REDIRECT_ADMIN_PAGE = "/admin.jsp";
  private static final String STAY_PAGE = "/";
  private static final String MESSAGE = "Nickname or Password doesn't match ";

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    doPost(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String form = request.getParameter(FORM_PARAM);
    String login = request.getParameter(LOGIN_PARAM);
    String password = request.getParameter(PASSWORD_PARAM);

    Connection connection = ConnectionPool.getInstance().getConnection();

    try {
      if (form.equals(LOGIN_PARAM) && UserDB.login(connection, login, password)) {
        User user = UserDB.getUserByLogin(connection, login);

        if (Role.ADMIN == user.getRole()) {
          ServletContext sc = this.getServletContext();
          sc.getRequestDispatcher(REDIRECT_ADMIN_PAGE).forward(request, response);
        } else {
          request.setAttribute("OKmessage", "Добро пожаловать,");
          request.setAttribute("user", user);
          //request.setAttribute("list", UserDB.get);
          ServletContext sc = this.getServletContext();
          sc.getRequestDispatcher(STAY_PAGE).forward(request, response);
        }


      } else {
        ServletContext sc = this.getServletContext();
        request.setAttribute(LOGIN_PARAM, login);
        request.setAttribute("message", MESSAGE);
        sc.getRequestDispatcher(STAY_PAGE).forward(request, response);
      }
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      DBUtil.closeConnection(connection);
    }
  }
}
