package business;

import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Result {

  private int id;
  private Timestamp datetime;
  private String info;

  @Id
  @Column(name = "id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "datetime")
  public Timestamp getDatetime() {
    return datetime;
  }

  public void setDatetime(Timestamp datetime) {
    this.datetime = datetime;
  }

  @Basic
  @Column(name = "info")
  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Result result = (Result) o;

    if (id != result.id) {
      return false;
    }
    if (datetime != null ? !datetime.equals(result.datetime) : result.datetime != null) {
      return false;
    }
    if (info != null ? !info.equals(result.info) : result.info != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
    result = 31 * result + (info != null ? info.hashCode() : 0);
    return result;
  }
}
