package business;

import business.enums.Type;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Task {

  private int id;
  private String info;
  private Timestamp datetime;
  private Type type;

  @Id
  @Column(name = "id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "info")
  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  @Basic
  @Column(name = "datetime")
  public Timestamp getDatetime() {
    return datetime;
  }

  public void setDatetime(Timestamp datetime) {
    this.datetime = datetime;
  }

  @Basic
  @Column(name = "type")
  public Type getType() {
    return type;
  }

  public void setType(Type type) {
    this.type = type;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    Task task = (Task) o;

    if (id != task.id) {
      return false;
    }
    if (info != null ? !info.equals(task.info) : task.info != null) {
      return false;
    }
    if (datetime != null ? !datetime.equals(task.datetime) : task.datetime != null) {
      return false;
    }
    if (type != null ? !type.equals(task.type) : task.type != null) {
      return false;
    }

    return true;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (info != null ? info.hashCode() : 0);
    result = 31 * result + (datetime != null ? datetime.hashCode() : 0);
    result = 31 * result + (type != null ? type.hashCode() : 0);
    return result;
  }
}
