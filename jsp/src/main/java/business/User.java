package business;

import business.enums.Role;
import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

@Entity
public class User implements Serializable {

  private int id;
  private String surname;
  private String name;
  private String patronymic;
  private Date dayOfBirth;
  private Role role;
  @Transient
  private Credentials credentials;
  @Transient
  private Registration registration;

  public User() {
  }

  public User(int id, String surname, String name, String patronymic, Date dayOfBirth, Role role) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.patronymic = patronymic;
    this.dayOfBirth = dayOfBirth;
    this.role = role;
  }

  public User(int id, String surname, String name, String patronymic, Date dayOfBirth, Role role,
      Credentials credentials) {
    this.id = id;
    this.surname = surname;
    this.name = name;
    this.patronymic = patronymic;
    this.dayOfBirth = dayOfBirth;
    this.role = role;
    this.credentials = credentials;
  }

  public User(String surname, String name, String patronymic, Date dayOfBirth, Role role,
      Registration registration) {
    this.surname = surname;
    this.name = name;
    this.patronymic = patronymic;
    this.dayOfBirth = dayOfBirth;
    this.role = role;
    this.registration = registration;
  }

  @Id
  @Column(name = "id")
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Basic
  @Column(name = "surname")
  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Basic
  @Column(name = "name")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Basic
  @Column(name = "patronymic")
  public String getPatronymic() {
    return patronymic;
  }

  public void setPatronymic(String patronymic) {
    this.patronymic = patronymic;
  }

  @Basic
  @Column(name = "dayOfBirth")
  public Date getDayOfBirth() {
    return dayOfBirth;
  }

  public void setDayOfBirth(Date dayOfBirth) {
    this.dayOfBirth = dayOfBirth;
  }

  @Basic
  @Column(name = "role")
  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }


  public Credentials getCredentials() {
    return credentials;
  }

  public void setCredentials(Credentials credentials) {
    this.credentials = credentials;
  }

  public Registration getRegistration() {
    return registration;
  }

  public void setRegistration(Registration registration) {
    this.registration = registration;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    User user = (User) o;

    if (id != user.id) {
      return false;
    }
    if (surname != null ? !surname.equals(user.surname) : user.surname != null) {
      return false;
    }
    if (name != null ? !name.equals(user.name) : user.name != null) {
      return false;
    }
    if (patronymic != null ? !patronymic.equals(user.patronymic) : user.patronymic != null) {
      return false;
    }
    if (dayOfBirth != null ? !dayOfBirth.equals(user.dayOfBirth) : user.dayOfBirth != null) {
      return false;
    }
    if (role != user.role) {
      return false;
    }
    if (credentials != null ? !credentials.equals(user.credentials) : user.credentials != null) {
      return false;
    }
    return registration != null ? registration.equals(user.registration) : user.registration == null;
  }

  @Override
  public int hashCode() {
    int result = id;
    result = 31 * result + (surname != null ? surname.hashCode() : 0);
    result = 31 * result + (name != null ? name.hashCode() : 0);
    result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
    result = 31 * result + (dayOfBirth != null ? dayOfBirth.hashCode() : 0);
    result = 31 * result + (role != null ? role.hashCode() : 0);
    result = 31 * result + (credentials != null ? credentials.hashCode() : 0);
    result = 31 * result + (registration != null ? registration.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "User{" +
        "id=" + id +
        ", surname='" + surname + '\'' +
        ", name='" + name + '\'' +
        ", patronymic='" + patronymic + '\'' +
        ", dayOfBirth=" + dayOfBirth +
        ", role=" + role +
        ", credentials=" + credentials +
        ", registration=" + registration +
        '}'+"\n";
  }
}
