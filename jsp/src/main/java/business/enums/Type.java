package business.enums;

import java.io.Serializable;

public enum Type implements Serializable {

  DRUG(1), PROCEDURE(2), OPERATION(3);

  private int value;

  private Type(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }

  public static Type ofStatusCode(int value) {

    switch (value) {

      case 0:
        return DRUG;

      case 1:
        return PROCEDURE;

      case 2:
        return OPERATION;

      default:
        return null;

    }
  }
}
