package business.enums;

import java.io.Serializable;

public enum Position implements Serializable {

  NURSE(1), OPERATOR(2);

  private int value;

  private Position(int value) {
    this.value = value;
  }

  public static Position ofStatusCode(int value) {

    switch (value) {

      case 0:
        return NURSE;

      case 1:
        return OPERATOR;

      default:
        return null;

    }
  }
}
