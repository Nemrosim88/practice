package business.enums;

import java.io.Serializable;

public enum Role implements Serializable {

  ADMIN(1), DOCTOR(2), STAFF(3), PATIENT(4);

  int value;

  private Role(int value) {
    this.value = value;
  }

  public int getValue() {
    return this.value;
  }

  public static Role ofStatusCode(int value) {

    switch (value) {

      case 0:
        return ADMIN;

      case 1:
        return DOCTOR;

      case 2:
        return STAFF;

      case 3:
        return PATIENT;

      default:
        return null;

    }
  }

  public static Role ofString(String role) {
    switch (role) {

      case "admin":
        return ADMIN;

      case "doctor":
        return DOCTOR;

      case "staff":
        return STAFF;

      case "patient":
        return PATIENT;

      default:
        return null;


    }

  }
}

