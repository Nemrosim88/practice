package tags;

import business.User;
import business.enums.Role;
import data.ConnectionPool;
import data.UserDB;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * BodyTagSupport extends TagSupport
 */
public class ListTag extends BodyTagSupport {


  private String cssClass;
  private String firstColumnName;
  private String secondColumnName;
  private String thirdColumnName;
  private String fourthColumnName;
  private String fifthColumnName;
  private String role;

  public String getCssClass() {
    return cssClass;
  }

  public void setCssClass(String cssClass) {
    this.cssClass = cssClass;
  }

  public String getFirstColumnName() {
    return firstColumnName;
  }

  public void setFirstColumnName(String firstColumnName) {
    this.firstColumnName = firstColumnName;
  }

  public String getSecondColumnName() {
    return secondColumnName;
  }

  public void setSecondColumnName(String secondColumnName) {
    this.secondColumnName = secondColumnName;
  }

  public String getThirdColumnName() {
    return thirdColumnName;
  }

  public void setThirdColumnName(String thirdColumnName) {
    this.thirdColumnName = thirdColumnName;
  }

  public String getFourthColumnName() {
    return fourthColumnName;
  }

  public void setFourthColumnName(String fourthColumnName) {
    this.fourthColumnName = fourthColumnName;
  }

  public String getFifthColumnName() {
    return fifthColumnName;
  }

  public void setFifthColumnName(String fifthColumnName) {
    this.fifthColumnName = fifthColumnName;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  @Override
  public int doStartTag() throws JspException {
    try {
      JspWriter out = pageContext.getOut();
      if (cssClass != null && cssClass.length() != 0) {
        out.print("<table class=\"" + cssClass + "\" >");
        out.print("<thead>");
        out.print("<tr>");
        out.print("<th>" + firstColumnName + "</th>");
        out.print("<th>" + secondColumnName + "</th>");
        out.print("<th>" + thirdColumnName + "</th>");
        out.print("<th>" + fourthColumnName + "</th>");
        out.print("<th>" + fifthColumnName + "</th>");
        out.print("<th>" + role + "</th>");
        out.print("</tr>");
        out.print("</thead>");
        out.print("<tbody>");

        try {
          List<User> list = UserDB.getAllUsers(ConnectionPool.getInstance().getConnection(), "id");
          for (User user : list) {
            out.print("<tr> ");
            out.print("<th scope=\"row\" > " + getUserValue(user, firstColumnName) + " </td>");
            out.print("<td>" + getUserValue(user, secondColumnName) + "</td>");
            out.print("<td>" + getUserValue(user, thirdColumnName) + "</td>");
            out.print("<td>" + getUserValue(user, fourthColumnName) + "</td>");
            out.print("<td>" + getUserValue(user, fifthColumnName) + "</td>");
            out.print("<td>" + getUserValue(user, role) + "</td>");
            out.print("</tr>");
          }

        } catch (SQLException e) {
          e.printStackTrace();
        }
//            <tr >
//                <th scope = "row" > 2 </th >
//               ...
//            </tr >
        out.print("</tbody>");
        out.print("</table>");
      }
    } catch (IOException ioe) {
      System.out.println(ioe.getMessage());
    }
    return SKIP_BODY;
  }

  /**
   * TODO Написать нормальный коммент.
   *
   * @param user user
   * @param field field
   * @return return
   */
  private String getUserValue(User user, String field) {
    switch (field) {
      case "id":
        return "" + user.getId();
      case "name":
        return user.getName();
      case "surname":
        return user.getSurname();
      case "patronymic":
        return user.getPatronymic();
      case "dayOfBirth":
        return user.getDayOfBirth().toString();
      case "role":
        return user.getRole().toString();
      default:
        return "ОШИБКА";
    }
  }


  @Override
  public void setPageContext(PageContext pageContext) {
    super.setPageContext(pageContext);
  }


}
