package data;

public class Query {

  public static String getAll(String orderBy) {
    return "SELECT * FROM hospital.user ORDER BY " + orderBy;
  }

  public static String getUserById() {
    return "SELECT * FROM hospital.user WHERE id=?";
  }

  public static String getCredentialsByUserId() {
    return "SELECT * FROM hospital.credentials WHERE userID = ?";
  }

  public static String getRegistrationByUserId() {
    return "SELECT * FROM hospital.registration WHERE userID = ?";
  }

  public static String getAllUsersPlusCredentials(String orderBy) {
    return "SELECT * FROM hospital.user JOIN hospital.credentials ON hospital.credentials.userID = hospital.user.id "
        + "ORDER BY " + orderBy;
  }

  public static String getUserByLogin() {
    return "SELECT * FROM hospital.user JOIN hospital.registration ON hospital.registration.userId= hospital.user.id "
        + "WHERE login = ?";
  }

  public static String isLoginExists() {
    return "SELECT login FROM hospital.registration WHERE login = ?";
  }

  public static String isPassportExists() {
    return "SELECT passport FROM hospital.credentials WHERE passport = ?";
  }

  public static String getLoginAndPasswordWhereLoginAndPassword() {
    return "SELECT hospital.registration.login,hospital.registration.password "
        + "FROM hospital.registration WHERE login = ? AND password = ?";
  }

  /**
   * 1.surname. <br> 2.name <br> 3.patronymic <br> 4.dayOfBirth <br> 5.role
   */
  public static String insertUser() {
    return "INSERT INTO hospital.user (surname, name, patronymic, dayOfBirth, role) VALUES (?,?,?,?,?)";
  }

  /**
   * 1.userId. <br> 2.passport <br> 3.email <br> 4.phoneNumber
   */
  public static String insertCredentials() {
    return "INSERT INTO hospital.credentials (userID, passport, email, phoneNumber) VALUES (?,?,?,?)";
  }

  public static String insertRegistration() {
    return "INSERT INTO hospital.registration (userId, login, password) VALUES (?,?,?)";
  }


}
