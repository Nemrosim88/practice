package data;


import business.Credentials;
import business.Registration;
import business.enums.Role;
import business.User;
import com.mysql.cj.jdbc.Driver;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.apache.log4j.Logger;

public class UserDB {

  private static final String COLUMN_NAME = "name";
  private static final String COLUMN_SURNAME = "surname";
  private static final String COLUMN_PATRONYMIC = "patronymic";
  private static final String COLUMN_DAY_OF_BIRTH = "dayOfBirth";
  private static final String COLUMN_ROLE = "role";
  private static final String COLUMN_PASSPORT = "passport";
  private static final String COLUMN_EMAIL = "email";
  private static final String COLUMN_PHONE_NUMBER = "phoneNumber";
//  private static final Logger logger = Logger.getLogger(UserDB.class);

  //  @Resource (name = "jdbc/mysql")
  private static DataSource dataSource;

  private static String url = "jdbc:mysql://localhost:3306/";
  private static String dbSSL = "useSSL=false";
  private static String timezone = "useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";


  public static List<User> getAllUsers(Connection connection, String orderBy) throws SQLException {
    Statement statement = null;
    ResultSet resultSet = null;
    try {
      statement = connection.createStatement();
      resultSet = statement.executeQuery(Query.getAll(orderBy));
      List<User> list = new ArrayList<>();
      while (resultSet.next()) {
        list.add(getUser(resultSet));
      }
      return list;
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      throw e;
    } finally {
      closeAll(resultSet, statement);
    }
  }

  public static User getUserById(Connection connection, int id) throws SQLException {
    PreparedStatement ps = null;
    ResultSet resultSet = null;
    try {
      ps = connection.prepareStatement(Query.getUserById());
      ps.setInt(1, id);
      resultSet = ps.executeQuery();
      resultSet.next();
      User user = getUser(resultSet);
      return user;
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      throw e;
    } finally {
      closeAll(resultSet, ps);
    }
  }

  public static Credentials getCredentialsByUserId(Connection connection, int userId) throws SQLException {
    PreparedStatement ps = null;
    ResultSet resultSet = null;
    try {
      ps = connection.prepareStatement(Query.getCredentialsByUserId());
      ps.setInt(1, userId);
      resultSet = ps.executeQuery();
      resultSet.next();
      Credentials credentials = getCredentials(resultSet);
      return credentials;
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      throw e;
    } finally {
      closeAll(resultSet, ps);
    }
  }

  public static Registration getRegistrationByUserId(Connection connection, int userId) throws SQLException {
    PreparedStatement ps = null;
    ResultSet resultSet = null;
    try {
      ps = connection.prepareStatement(Query.getRegistrationByUserId());
      ps.setInt(1, userId);
      resultSet = ps.executeQuery();
      resultSet.next();
      Registration registration = getRegistration(resultSet);
      return registration;
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      throw e;
    } finally {
      closeAll(resultSet, ps);
    }
  }

  public static List<User> getAllUsersPlusCredentials(Connection conn, String orderBy) throws SQLException {

    Statement statement = null;
    ResultSet resultSet = null;

    try {
      statement = conn.createStatement();
      resultSet = statement.executeQuery(Query.getAllUsersPlusCredentials(orderBy));

      List<User> list = new ArrayList<>();

      while (resultSet.next()) {
        list.add(getUserPlusCredentials(resultSet));
      }

      return list;
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      throw e;
    } finally {
      closeAll(resultSet, statement);
    }
  }


  public static User getUserByLogin(Connection connection, String login) throws
      SQLException {
    PreparedStatement preparedStatement = null;
    ResultSet resultSet = null;

    try {
      String query = Query.getUserByLogin();
      preparedStatement = connection.prepareStatement(query);
      preparedStatement.setString(1, login);

      resultSet = preparedStatement.executeQuery();

      User user = null;
      while (resultSet.next()) {
        user = getUser(resultSet);
      }
      return user;

    } catch (SQLException e) {
//      logger.info(e.getMessage());
      throw e;
    } finally {
      closeAll(resultSet, preparedStatement);
    }
  }

  public static boolean login(Connection connection, String login, String password) {
    PreparedStatement ps = null;
    ResultSet rs = null;
    try {
      String query = Query.getLoginAndPasswordWhereLoginAndPassword();
      ps = connection.prepareStatement(query);
      ps.setString(1, login);
      ps.setString(2, password);
      rs = ps.executeQuery();
      return rs.next();
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      return false;
    } finally {
      closeAll(rs, ps);
    }
  }

  /**
   * Метод добавляет нового пользователя. <br>
   *
   * @param connection connection
   * @param user user
   * @return возвращает ID пользователя который был добавлен в базу данных
   */
  public static int insert(Connection connection, User user) {
    int userId;
    if (user.getCredentials() != null && user.getRegistration() != null) {
      userId = insertOnlyUser(connection, user);
      insertCredentials(connection, user.getCredentials(), userId);
      insertRegistration(connection, user.getRegistration(), userId);

    } else if (user.getCredentials() != null && user.getRegistration() == null) {
      userId = insertOnlyUser(connection, user);
      insertCredentials(connection, user.getCredentials(), userId);

    } else if (user.getRegistration() != null && user.getCredentials() == null) {
      userId = insertOnlyUser(connection, user);
      insertRegistration(connection, user.getRegistration(), userId);

    } else {
      userId = insertOnlyUser(connection, user);
    }
    return userId;
  }


  /**
   * Если пользователь добавлен удачно, то вернётся индекс который был ему присвоен.
   *
   * <p> Если что-то пошло не так - вернётся -1.
   *
   * <p> Connection не закрывается.
   *
   * @param connection connection
   * @param user user
   * @return generated id or -1 if something went wrong.
   */
  private static int insertOnlyUser(Connection connection, User user) {
    String query = Query.insertUser();
    PreparedStatement ps = null;
    ResultSet rs = null;
    int auto_id;
    try {
      connection.setAutoCommit(false);
      ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
      ps.setString(1, user.getSurname());
      ps.setString(2, user.getName());
      ps.setString(3, user.getPatronymic());
      ps.setDate(4, user.getDayOfBirth());
      ps.setString(5, user.getRole().toString());
      ps.executeUpdate();
      rs = ps.getGeneratedKeys();
      rs.next();
      auto_id = rs.getInt(1);
      connection.commit();
      ps.close();
      connection.setAutoCommit(true);
      return auto_id;
    } catch (SQLException e) {
//      logger.info(e.getMessage());
      return -1;
    } finally {
      if (ps != null) {
        DBUtil.closeStatement(ps);
      }
      if (rs != null) {
        DBUtil.closeResultSet(rs);
      }
    }
  }

  private static void insertCredentials(Connection connection, Credentials credentials, int userId) {
    String query = Query.insertCredentials();
    PreparedStatement ps = null;

    try {
      connection.setAutoCommit(false);
      ps = connection.prepareStatement(query);
      ps.setInt(1, userId);
      ps.setString(2, credentials.getPassport());
      ps.setString(3, credentials.getEmail());
      ps.setString(4, credentials.getPhoneNumber());
      ps.executeUpdate();
      connection.commit();
      ps.close();
      connection.setAutoCommit(true);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (ps != null) {
        DBUtil.closeStatement(ps);
      }
    }
  }

  private static void insertRegistration(Connection connection, Registration registration, int userId) {
    String query = Query.insertRegistration();
    PreparedStatement ps = null;

    try {
      connection.setAutoCommit(false);
      ps = connection.prepareStatement(query);
      ps.setInt(1, userId);
      ps.setString(2, registration.getLogin());
      ps.setString(3, registration.getPassword());
      ps.executeUpdate();
      connection.commit();
      ps.close();
      connection.setAutoCommit(true);
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      if (ps != null) {
        DBUtil.closeStatement(ps);
      }
    }
  }


  /**
   * Метод проверяет, есть ли в базе данных такой логин или нет.
   */
  public static boolean isLoginExists(Connection connection, String nickname) {

    PreparedStatement ps = null;
    ResultSet rs = null;
    String query = Query.isLoginExists();

    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, nickname);
      rs = ps.executeQuery();
      return rs.next();
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    } finally {
      closeAll(rs, ps);
    }
  }


  public static boolean isPassportExists(Connection connection, String passport) {
    PreparedStatement ps = null;
    ResultSet rs = null;
    String query = Query.isPassportExists();
    try {
      ps = connection.prepareStatement(query);
      ps.setString(1, passport);
      rs = ps.executeQuery();
      return rs.next();
    } catch (SQLException e) {
      e.printStackTrace();
      return false;
    } finally {
      closeAll(rs, ps);
    }
  }


  /**
   * Метод извлекает из ResultSet пользователя. Таблица user.
   *
   * @param resultSet resultSet
   * @return User
   */
  private static User getUser(ResultSet resultSet) throws SQLException {
    int i;
    String role;
    Role r;
    try {
      i = resultSet.getInt(COLUMN_ROLE);
      r = Role.ofStatusCode(i);
    } catch (Exception e) {
      role = resultSet.getString(COLUMN_ROLE);
      r = Role.ofString(role);
    }

    User user = new User(
        resultSet.getInt("id"),
        resultSet.getString(COLUMN_SURNAME),
        resultSet.getString(COLUMN_NAME),
        resultSet.getString(COLUMN_PATRONYMIC),
        resultSet.getDate(COLUMN_DAY_OF_BIRTH),
        r);
    return user;
  }

  /**
   * Метод извлекает из ResultSet пользователя. Таблица user.
   *
   * @param resultSet resultSet
   * @return User
   */
  private static Credentials getCredentials(ResultSet resultSet) throws SQLException {
    Credentials credentials = new Credentials(
        resultSet.getInt("userID"),
        resultSet.getString("passport"),
        resultSet.getString("email"),
        resultSet.getString("phoneNumber")
    );
    return credentials;
  }

  private static Registration getRegistration(ResultSet resultSet) throws SQLException {
    Registration credentials = new Registration(
        resultSet.getInt("userId"),
        resultSet.getString("login"),
        resultSet.getString("password")
    );
    return credentials;
  }

  private static User getUserPlusCredentials(ResultSet resultSet) throws SQLException {
    User user = getUser(resultSet);
    user.setCredentials(new Credentials(
        resultSet.getString(COLUMN_PASSPORT),
        resultSet.getString(COLUMN_EMAIL),
        resultSet.getString(COLUMN_PHONE_NUMBER)));
    return user;
  }

  /**
   * Метод для закрытия трёх: ResultSet, Statement, Connection.
   */
  private static void closeAll(ResultSet rs, Statement st) {
    DBUtil.closeResultSet(rs);
    DBUtil.closeStatement(st);
  }

  /**
   * Old fashion way.
   */
  @Deprecated
  public static long insertOLW(User user) throws SQLException {
    Driver driver = new com.mysql.cj.jdbc.Driver();
    DriverManager.registerDriver(driver);
    Connection connection = DriverManager
        .getConnection(url + "?" + dbSSL + "&" + timezone, "root", "12345");

    //language=MySQL
    String query = "INSERT INTO java.users (name, surname, email) VALUES (?,?,?)";
    PreparedStatement ps = connection.prepareStatement(query);

    ps.setString(1, user.getName());
    ps.setString(2, user.getSurname());
//    ps.setString(3, user.getEmail());

    ps.executeUpdate();

    ps.close();

    return 0;
  }
}
