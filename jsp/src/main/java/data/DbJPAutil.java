package data;


import business.User;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class DbJPAutil {


  public static void insert(User user) {
    EntityManager em = JPAUtil.getEmFactory().createEntityManager();
    EntityTransaction trans = em.getTransaction();
    trans.begin();
    try {
      em.persist(user);
      trans.commit();
    } catch (Exception e) {
      System.out.println(e);
      trans.rollback();
    } finally {
      em.close();
    }
  }

  public static void update(User user) {
    EntityManager em = JPAUtil.getEmFactory().createEntityManager();
    EntityTransaction trans = em.getTransaction();
    trans.begin();
    try {
      em.merge(user);
      trans.commit();
    } catch (Exception e) {
      System.out.println(e);
      trans.rollback();
    } finally {
      em.close();
    }
  }

  public static void delete(User user) {
    EntityManager em = JPAUtil.getEmFactory().createEntityManager();
    EntityTransaction trans = em.getTransaction();
    trans.begin();
    try {
      em.remove(em.merge(user));
      trans.commit();
    } catch (Exception e) {
      System.out.println(e);
      trans.rollback();
    } finally {
      em.close();
    }
  }

  public static User selectUser(String patronymic) {
    EntityManager em = JPAUtil.getEmFactory().createEntityManager();
    String qString = "SELECT u FROM User u WHERE u.patronymic = :email";
    TypedQuery<User> q = em.createQuery(qString, User.class);
    q.setParameter("email", patronymic);
    try {
      User user = q.getSingleResult();
      return user;
    } catch (NoResultException e) {
      return null;
    } finally {
      em.close();
    }
  }

  public static boolean emailExists(String email) {
    User u = selectUser(email);
    return u != null;
  }

}
