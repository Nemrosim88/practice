package data;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class DBUtil {


  public static void closeStatement(Statement s) {
    try {
      if (s != null) {
        s.close();
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  public static void closeResultSet(ResultSet rs) {
    try {
      if (rs != null) {
        rs.close();
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  public static void closeConnection(Connection connection) {
    try {
      if (connection != null) {
        connection.close();
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

  }

  public static String readQuery(String fileName) throws IOException {
    String path = "\\src\\main\\resources\\sql\\";
    String absolutePath = new File("").getAbsolutePath();
    StringBuilder sb = new StringBuilder();
    Files.readAllLines(Paths.get(absolutePath + path + fileName)).forEach(sb::append);
    return sb.toString();
  }
}
