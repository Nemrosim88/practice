package email;

import business.User;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;


@WebServlet(name = "EmailListServlet", urlPatterns = "/emailList")
public class EmailListServlet extends HttpServlet {

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {

    String url = "/index.jsp";

    // get current action
    String action = request.getParameter("action");
    if (action == null) {
      action = "join";  // default action
    }
    // perform action and set URL to appropriate page
    if (action.equals("join")) {
      url = "/index.jsp";    // the "join" page
    } else if (action.equals("add")) {
      // get parameters from the request
      String firstName = request.getParameter("firstName");
      String lastName = request.getParameter("lastName");
      String email = request.getParameter("main/java/email");

      // store main.java.data in User object
      User user = new User();

      // validate the parameters
      String message;
      if (firstName == null || lastName == null || email == null ||
          firstName.isEmpty() || lastName.isEmpty() || email.isEmpty()) {
        message = "Please fill out all three text boxes.";
        url = "/index.jsp";
      } else {
        message = "";
        url = "/thanks.jsp";
        String checkbox = request.getParameter("addToDb");

        // checkbox.equals("on") можно не добавлять
        if (checkbox != null && checkbox.equals("on")) {
//          try {
//            UserDB.insert(user);
//          } catch (SQLException e) {
//            e.printStackTrace();
//          }
        }
      }
      request.setAttribute("user", user);
      request.setAttribute("message", message);
    }

    // forward request and response objects to specified URL
    ServletContext sc = this.getServletContext();
    System.out.println(sc.getRealPath("/style/main.css"));

    // Отправляем параметры
    sc.getRequestDispatcher(url).forward(request, response);

  }

  @Override
  protected void doGet(HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {
    doPost(request, response);
  }
}