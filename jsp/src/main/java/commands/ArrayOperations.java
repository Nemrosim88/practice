package commands;

public class ArrayOperations {


  private int[] mass;
  public ArrayOperations(int[] mass) {
    this.mass = mass;
  }
  public void sum() {
    int sum = 0;
    for (int i : mass)
      sum += i;
    System.out.println(sum);
  }
  public void product() {
    int mult = 1;
    for (int i : mass)
      mult *= i;
    System.out.println(mult);
  }


}
