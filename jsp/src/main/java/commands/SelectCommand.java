package commands;

import java.sql.ResultSet;

public interface SelectCommand {
  public ResultSet get();
}
