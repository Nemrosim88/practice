<%@page contentType="text/html" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:import url="/includes/header.html"/>
<%@ page import="business.User" %>



<%--       request.setAttribute("message", message);      --%>
<c:if test="${message != null}">
    <p class="alert-heading"><i>${message}</i></p>
</c:if>

<form action="emailList" method="post">
    <input type="hidden" name="action" value="add">

    <label class="pad_top">Email:</label>
    <input type="email" name="email" value="${user.email}" required><br>

    <label class="pad_top">First Name:</label>
    <input type="text" name="firstName" value="${user.firstName}" required><br>

    <label class="pad_top">Last Name:</label>
    <input type="text" name="lastName" value="${user.lastName}" required><br>

    <label class="pad_top">Last Name:</label>
    <input type="text" name="lastName" value="${user.lastName}" required><br>

    <!--Пример использования checkbox-->
    <p><input type="checkbox" name="addToDb" checked>
        Yes, add this to DB</p>

    <!--Пример использования radio-->
    <p> List Demo:
        <input type="radio" name="radioDemo" value="main.java.email"> main.java.email
        <input type="radio" name="radioDemo" value="PostalMail"> postal mail
        <input type="radio" name="radioDemo" value="both"> both </p>

    <!--Пример использования radio-->
    <p> Checkbox Demo: <br>
        <input type="checkbox" name="one" value="main.java.email"> one <br>
        <input type="checkbox" name="two" value="PostalMail"> two <br>
        <input type="checkbox" name="three" value="both"> three </p>


    <label>&nbsp;</label>
    <input type="submit" value="Join Now" class="margin_left">
</form>

<c:import url="/includes/footer.jsp"/>