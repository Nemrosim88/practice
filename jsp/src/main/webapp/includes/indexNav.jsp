<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<a class="navbar-brand" href="#">Carousel</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
        aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarCollapse">
    <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
            <a class="nav-link" href="custom.jsp">Custom.jsp<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="doctor.jsp">Link</a>
        </li>
        <li class="nav-item active">
            <a class="nav-link disabled" href="#">Disabled</a>
        </li>
        <li class="nav-item active">
            <%
                HttpSession clientSession = request.getSession();
            %>
            <label>Ваш ID:&ensp;<%= clientSession.getId()%>
            </label>
        </li>
    </ul>


    <c:choose>
        <%--@elvariable id="OKmessage" type="java.lang.String"--%>
        <c:when test="${OKmessage != null}">
            <label style="color: forestgreen">${OKmessage}&ensp;</label>
            <%--@elvariable id="user" type="business.User"--%>
            <label style="color: forestgreen">${user.surname}&ensp;</label>
            <label style="color: forestgreen">${user.name}&ensp;</label>
            <label style="color: forestgreen">${user.patronymic}&ensp;</label>
            <form class="form-inline mt-2 mt-md-0" action="logOut" method="post">
                <input type="hidden" name="logOut" value="logOut">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">LOGOUT</button>
            </form>
        </c:when>


        <c:otherwise>

            <%--@elvariable id="message" type="java.lang.String"--%>
            <label style="color: red">${message}</label>
            <form class="form-inline mt-2 mt-md-0" action="login" method="post">
                <input type="hidden" name="loginForm" value="login">
                <c:choose>
                    <%--@elvariable id="login" type="java.lang.String"--%>
                    <c:when test="${login != null}">
                        <input class="form-control mr-sm-2" type="text" name="login" placeholder="${login}"
                               aria-label="Search">
                    </c:when>
                    <c:otherwise>
                        <input class="form-control mr-sm-2" type="text" name="login" placeholder="Login"
                               aria-label="Search">
                    </c:otherwise>
                </c:choose>

                <input class="form-control mr-sm-2" type="text" name="password" placeholder="Password"
                       aria-label="Password">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">LOGIN</button>
            </form>
        </c:otherwise>
    </c:choose>


</div>
