<%--@elvariable id="user" type="business.User"--%>
<%--@elvariable id="role" type="business.enums.Role"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<br>
<div class="container">
    <label><c:out value="${userId != null ? 'Новому пользователю присвоено ID: '+ userId : ''}"/></label>
    <form action="addNewUser" method="post">
        <input type="hidden" name="formName" value="addNewUser">
        <%--Поле для фамилии--%>
        <div class="form-group">
            <label for="surname">Фамилия</label>
            <input type="text" class="form-control" id="surname" name="surname"
                   placeholder="Петренко" value="${user.surname}" required>
        </div>

        <%--Поле для имени--%>
        <div class="form-group">
            <label for="name">Имя</label>
            <input type="text" class="form-control" id="name" name="name"
                   placeholder="Петр" value="${user.name}" required>
        </div>

        <%--Поле для отчества--%>
        <div class="form-group">
            <label for="patronymic">Отчество</label>
            <input type="text" class="form-control" id="patronymic" name="patronymic"
                   placeholder="Петрович" value="${user.patronymic}" required>
        </div>

        <%--Поле для даты рождения--%>
        <div class="form-group">
            <label for="dayOfBirth">Дата рождения</label>
            <input type="date" class="form-control" id="dayOfBirth" name="dayOfBirth"
                   placeholder="1970-06-06" value="${user.dayOfBirth}" required>
        </div>

        <%--Поле для роли--%>
        <div class="form-group">
            <label for="role">Выберите роль пользователя</label>
            <select class="form-control" id="role" name="role">
                <c:choose>
                    <c:when test="${user.role == null}">
                        <option>admin</option>
                        <option>doctor</option>
                        <option>staff</option>
                        <option>patient</option>
                    </c:when>
                    <c:when test="${user.role == 'ADMIN'}">
                        <option selected>admin</option>
                        <option>doctor</option>
                        <option>staff</option>
                        <option>patient</option>
                    </c:when>
                    <c:when test="${user.role == 'DOCTOR'}">
                        <option>admin</option>
                        <option selected>doctor</option>
                        <option>staff</option>
                        <option>patient</option>
                    </c:when>
                    <c:when test="${user.role == 'STAFF'}">
                        <option>admin</option>
                        <option>doctor</option>
                        <option selected>staff</option>
                        <option>patient</option>
                    </c:when>
                    <c:when test="${user.role == 'PATIENT'}">
                        <option>admin</option>
                        <option>doctor</option>
                        <option>staff</option>
                        <option selected>patient</option>
                    </c:when>
                </c:choose>
            </select>
        </div>

        <h2>Дополнительные сведения</h2>

        <%--Поле для номера паспорта--%>
        <div class="form-group">
            <label for="passport" style="color: <c:out value="${passportMessage != null ? 'red' : 'black'}"/>">
                <%--@elvariable id="passportMessage" type="java.lang.String"--%>
                <c:out value="${passportMessage != null ? passportMessage : 'Серия и номер паспорта'}"/>
            </label>
            <input type="text"
                   class="form-control"
                   id="passport"
                   name="passport"
                   placeholder="AA123456"
                   value="${user.credentials.passport}"
                   required>
        </div>

        <%--Поле для email--%>
        <div class="form-group">
            <label for="email">Почтовый адрес</label>
            <input type="email"
                   class="form-control"
                   id="email"
                   name="email"
                   placeholder="helloWorld@gmail.com"
                   value="${user.credentials.email}">
        </div>

        <%--Поле для номера телефона--%>
        <div class="form-group">
            <label for="phoneNumber">Номер телефона</label>
            <input type="tel"
                   class="form-control"
                   id="phoneNumber"
                   name="phoneNumber"
                   placeholder="+38(050)123-45-56"
                   value="${user.credentials.phoneNumber}">
        </div>

        <h2>Логин и пароль пользователя</h2>

        <%--Поле для логина--%>
        <div class="form-group">

            <label for="login" style="color: <c:out value="${loginMessage != null ? 'red' : 'black'}"/>">

                <%--@elvariable id="loginMessage" type="java.lang.String"--%>
                <c:out value="${loginMessage != null ? loginMessage : 'Логин'}"/>
            </label>
            <input type="text"
                   class="form-control"
                   id="login"
                   name="login"
                   placeholder="JamesBond007"
                   value="<c:out value="${user.registration.login != null ? user.registration.login : ''}"/>">
        </div>

        <%--Поле для пароля--%>
        <div class="form-group">
            <label for="password">Пароль</label>
            <input type="password"
                   class="form-control"
                   id="password"
                   name="password"
                   placeholder="12345">
        </div>

        <%--<div class="form-group">--%>
        <%--<label for="exampleFormControlTextarea1">Example textarea</label>--%>
        <%--<textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>--%>
        <%--</div>--%>

        <button type="submit" class="btn btn-primary btn-lg btn-block">Добавить нового пользователя</button>
    </form>

</div>
