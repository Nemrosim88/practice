package data;

import business.Credentials;
import business.Registration;
import business.User;
import business.enums.Role;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class TestUtils {

  /**
   * Метод возвращает всех пользователей без credentials and registration.
   */
  public static List<User> getListOfUsers() {
    List<User> r = new ArrayList<>();
    r.add(new User(1, "Викторов", "Виктор", "Викторович", Date.valueOf("1960-06-12"), Role.DOCTOR));
    r.add(new User(2, "Спиридонов", "Спиридон", "Спиридонович", Date.valueOf("1975-02-21"), Role.STAFF));
    r.add(new User(3, "Андреонов", "Андрей", "Андреевич", Date.valueOf("1981-03-08"), Role.STAFF));
    r.add(new User(4, "Андроновский", "Андрон", "Андронович", Date.valueOf("1984-04-04"), Role.DOCTOR));
    r.add(new User(5, "Арсенский", "Арсен", "Арсеньевич", Date.valueOf("1979-09-27"), Role.DOCTOR));
    r.add(new User(6, "Белинский", "Борис", "Борисович", Date.valueOf("1978-10-11"), Role.PATIENT));
    r.add(new User(7, "Блохин", "Болеслав", "Болеславович", Date.valueOf("1969-01-01"), Role.PATIENT));
    r.add(new User(8, "Бурда", "Бонифатий", "Бонифатьевич", Date.valueOf("1983-02-20"), Role.DOCTOR));
    r.add(new User(9, "Антонович", "Владимир", "Бонифатьевич", Date.valueOf("1989-04-27"), Role.ADMIN));
    r.add(new User(10, "Вадиморский", "Вадим", "Вадимович", Date.valueOf("1990-11-09"), Role.PATIENT));
    r.add(new User(11, "Василяк", "Василий", "Васильевич", Date.valueOf("1991-12-15"), Role.PATIENT));
    r.add(new User(12, "Василяк", "Василий", "Витальевич", Date.valueOf("1965-04-01"), Role.PATIENT));
    r.add(new User(13, "Володарский", "Володар", "Володарьевич", Date.valueOf("1984-02-02"), Role.STAFF));
    r.add(new User(14, "Гавриляк", "Гавриил", "Гавриилович", Date.valueOf("1982-01-13"), Role.STAFF));
    r.add(new User(15, "Геральдин", "Геральд", "Геральдович", Date.valueOf("1971-07-25"), Role.DOCTOR));
    r.add(new User(16, "Гордон", "Гордон", "Гордонович", Date.valueOf("1972-06-21"), Role.STAFF));
    r.add(new User(17, "Герасименко", "Герасим", "Герасимович", Date.valueOf("1960-03-17"), Role.DOCTOR));
    r.add(new User(18, "Дагмерия", "Дагмера", "Дмитриевна", Date.valueOf("1959-06-03"), Role.STAFF));
    r.add(new User(19, "Диановна", "Диана", "Артёмовна", Date.valueOf("1993-03-03"), Role.PATIENT));
    r.add(new User(20, "Долена", "Доля", "Геральдовна", Date.valueOf("1988-05-10"), Role.STAFF));
    r.add(new User(21, "Дитовская", "Дита", "Бонифатьевна", Date.valueOf("1978-10-09"), Role.STAFF));
    r.add(new User(22, "Джановна", "Джана", "Михайловна", Date.valueOf("1978-10-08"), Role.PATIENT));
    return r;

  }

  public static List<User> getListOfUsersPlusCredentials() {
    List<User> list = getListOfUsers();
    list.get(0).setCredentials(new Credentials("ПА234321", "victorov@gmail.com", "+38(063)111-22-33"));
    list.get(1).setCredentials(new Credentials("ЛО596857", "spiridonov@gmail.com", "+38(050)475-43-90"));
    list.get(2).setCredentials(new Credentials("ПР857968", "arsenskiy@ukr.net", "+38(050)475-43-91"));
    list.get(3).setCredentials(new Credentials("НА867596", "andronovkiy@mail.ru", "+38(050)475-43-92"));
    list.get(4).setCredentials(new Credentials("УК857465", "arsenskiy@gmail.com", "+38(050)475-43-93"));
    list.get(5).setCredentials(new Credentials("АА038473", "belinskiy@gmail.com", "+38(050)475-43-34"));
    list.get(6).setCredentials(new Credentials("КН647586", "blohin@mail.ru", "+38(050)475-45-67"));
    list.get(7).setCredentials(new Credentials("ОГ273648", "burda@ukr.net", "+38(050)475-12-12"));
    list.get(8).setCredentials(new Credentials("ПА194857", "antonovich@ukr.net", "+38(063)475-45-23"));
    list.get(9).setCredentials(new Credentials("НЕ109857", "vadimorskiy@gmail.com", "+38(050)475-43-95"));
    list.get(10).setCredentials(new Credentials("ЕА675840", "vasiliak@gmail.com", "+38(050)475-43-96"));
    list.get(11).setCredentials(new Credentials("ОО305631", "vasil.vasiliak@mail.ru", "+38(050)475-43-97"));
    list.get(12).setCredentials(new Credentials("СН300948", "volodar@gmail.com", "+38(050)475-43-98"));
    list.get(13).setCredentials(new Credentials("МС101937", "gavriliak@ukr.net", "+38(050)475-43-99"));
    list.get(14).setCredentials(new Credentials("УУ475860", "geraldin@gmail.com", "+38(050)475-43-00"));
    list.get(15).setCredentials(new Credentials("ЛО049576", "gordon@epam.ua", "+38(050)475-43-01"));
    list.get(16).setCredentials(new Credentials("СС294836", "gerasimenko@codefire.com", "+38(050)475-43-02"));
    list.get(17).setCredentials(new Credentials("ЗУ394584", "dagmeria@mail.ru", "+38(050)475-43-03"));
    list.get(18).setCredentials(new Credentials("УТ947563", "dianova@gmail.com", "+38(050)475-43-04"));
    list.get(19).setCredentials(new Credentials("УК947574", "dolena@ukr.net", "+38(050)475-43-05"));
    list.get(20).setCredentials(new Credentials("ЛЕ836283", "ditovskaia@gmail.com", "+38(050)475-43-06"));
    list.get(21).setCredentials(new Credentials("СИ947584", "djanovna@ukr.net", "+38(050)475-43-07"));
    return list;


  }

  public static User getUserOne() {
    User user = new User();
    user.setSurname("Петроновский");
    user.setName("Пётр");
    user.setPatronymic("Васильевич");
    user.setDayOfBirth(Date.valueOf("1988-05-10"));
    user.setRole(Role.DOCTOR);
    return user;
  }

  public static User getUserWithCredentials() {
    User user = new User();
    user.setSurname("Вяземский");
    user.setName("Андрей");
    user.setPatronymic("Антонович");
    user.setDayOfBirth(Date.valueOf("1978-03-06"));
    user.setRole(Role.DOCTOR);
    user.setCredentials(new Credentials("ОР987234", "kjh876asd@gmail.com", "+38(063)597-43-11"));
    return user;
  }

  public static User getUserWithRegistration() {
    User user = new User();
    user.setSurname("Вяземский");
    user.setName("Андрей");
    user.setPatronymic("Антонович");
    user.setDayOfBirth(Date.valueOf("1978-03-06"));
    user.setRole(Role.DOCTOR);
    user.setCredentials(new Credentials("ОР987234", "kjh876asd@gmail.com", "+38(063)597-43-11"));
    user.setRegistration(new Registration("asdfasdf", "123456"));
    return user;
  }

}
