package data;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


import business.User;
import business.enums.Role;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.ResourceBundle;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class DBTests {

  private static final String MARIA_DB_DRIVER = "MariaDBdriver";
  private static final String MARIA_URL = "MariaDBurl";
  private static final String H2_DB_DRIVER = "H2driver";
  private static final String PROP_NAME = "database";
  private static Connection connection;
  private static Statement statement;
  private static DataSource dataSource;
  private static final Logger logger = Logger.getLogger(DBTests.class);


  @BeforeClass
  public static void setUp() throws Exception {
    ResourceBundle resource = ResourceBundle.getBundle(PROP_NAME);
    Class.forName(resource.getString(H2_DB_DRIVER));
    logger.info("@BeforeClass, Зарегистрировали драйвер");
    dataSource = JdbcConnectionPool.create(
        resource.getString("H2url"),
        resource.getString("H2user"),
        resource.getString("H2password")
    );
    logger.info("@BeforeClass, Получили DataSource");
    connection = dataSource.getConnection();
//    connection = DriverManager.getConnection(resource.getString(MARIA_URL));
    logger.info("@BeforeClass, Получили connection");


  }

  @AfterClass
  public static void tearDown() throws Exception {
    logger.info("@AfterClass, connection.isClosed() -> " + connection.isClosed());
    logger.info("@AfterClass, statement.isClosed() -> " + statement.isClosed());
  }

  @Before
  public void setConnection() {
    try {

      //      connection = DriverManager.getConnection(MARIA_DB_URL);
      connection = dataSource.getConnection();
      statement = connection.createStatement();

      logger.info("@Before, после методов connection.isClosed() -> " + connection.isClosed());
      logger.info("@Before, после методов statement.isClosed() -> " + statement.isClosed());

      new CreateAndFill(new HospitalDB(), statement);
    } catch (SQLException e) {
      logger.info("@Before, Exception -> " + e.getMessage());
    }
  }

  @After
  public void closeConnection() {
    try {
      if (connection == null || connection.isClosed()) {
        connection = dataSource.getConnection();
        logger.info("@After, connection.isClosed() -> " + connection.isClosed());

      }
      if (statement == null || statement.isClosed()) {
        statement = connection.createStatement();
        logger.info("@After, statement.isClosed() -> " + statement.isClosed());
      }

      statement.execute("DROP SCHEMA hospital");
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      DBUtil.closeStatement(statement);
      DBUtil.closeConnection(connection);
    }
  }

  @Test(timeout = 1000)
  public void getAllUsersTest() throws Exception {
    assertThat(TestUtils.getListOfUsers(), is(UserDB.getAllUsers(connection, "id")));
    assertTrue(!connection.isClosed());
  }

  @Test(timeout = 500)
  public void loginExistsTest() throws Exception {
    assertTrue(UserDB.isLoginExists(connection, "login5"));
    assertTrue(!connection.isClosed());
  }

  @Test(expected = SQLException.class)
  public void getAllUsersException() throws SQLException {
    UserDB.getAllUsers(connection, "nomer");
    assertTrue(!connection.isClosed());
  }

  @Test
  public void getAllWithCredentials() throws Exception {
    assertThat(TestUtils.getListOfUsersPlusCredentials(), is(UserDB.getAllUsersPlusCredentials(connection, "id")));
    assertTrue(!connection.isClosed());
  }

  @Test(expected = SQLException.class)
  public void getAllPlusCredentialsExceptionTest() throws SQLException {
    UserDB.getAllUsersPlusCredentials(connection, "chicken");
    assertTrue(!connection.isClosed());
  }

  @Test
  public void getUserByLoginTest() throws SQLException {
    assertEquals(TestUtils.getListOfUsers().get(4), UserDB.getUserByLogin(connection, "login5"));
    assertTrue(!connection.isClosed());
  }

  @Test(expected = AssertionError.class)
  public void getUserByLoginExceptionTest() throws SQLException {
    assertEquals(TestUtils.getListOfUsers().get(4), UserDB.getUserByLogin(connection, "stepan"));
    assertTrue(!connection.isClosed());
  }

  @Test
  public void loginTest() throws Exception {
    assertTrue(UserDB.login(connection, "login7", "12345"));
    assertTrue(!connection.isClosed());
  }

  @Test(expected = AssertionError.class)
  public void loginExceptionTest() throws Exception {
    assertTrue(UserDB.login(connection, "chicken", "fry"));
    assertTrue(!connection.isClosed());
  }

  @Test
  public void loginFinallyTest() throws SQLException {
    assertTrue(UserDB.login(connection, "login7", "12345"));
    assertTrue(!connection.isClosed());
  }


  @Test
  public void insertUserTest() throws Exception {
    User inputUser = TestUtils.getUserOne();
    int userID = UserDB.insert(connection, inputUser);
    logger.info("insertUserTest(), int userID => " + userID);

    inputUser.setId(23);
    User outputUser = UserDB.getUserById(connection, userID);


    assertTrue(inputUser.equals(outputUser));
    assertTrue(outputUser.getRole() == Role.DOCTOR);
    assertTrue(outputUser.getCredentials() == null);
    assertTrue(outputUser.getRegistration() == null);
  }

  @Test
  public void insertUserWithCredentialsTest() throws Exception {
    User newUser = TestUtils.getUserWithCredentials();
    int userID = UserDB.insert(connection, newUser);


    newUser.setId(userID);
    newUser.getCredentials().setId(userID);

    User user = UserDB.getUserById(connection, userID);
    user.setCredentials(UserDB.getCredentialsByUserId(connection, userID));

    assertTrue(user.equals(newUser));
  }

  @Test
  public void insertUserWithALL_Test() throws Exception {
    User newUser = TestUtils.getUserWithRegistration();
    int userID = UserDB.insert(connection, newUser);


    newUser.setId(userID);
    newUser.getCredentials().setId(userID);
    newUser.getRegistration().setUserId(userID);


    User user = UserDB.getUserById(connection, userID);
    user.setRegistration(UserDB.getRegistrationByUserId(connection, userID));
    user.setCredentials(UserDB.getCredentialsByUserId(connection, userID));

    assertTrue(user.equals(newUser));
  }

}