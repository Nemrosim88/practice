package data;

import static data.DBUtil.*;

import java.io.IOException;
import java.sql.SQLException;
import java.sql.Statement;

public class HospitalDB implements DataBase {

  @Override
  public void createAndFill(Statement statement) {
    try {
      //      CREATE STATEMENTS
      statement.execute(readQuery("create\\schema.sql"));
      statement.execute(readQuery("create\\user.sql"));
      statement.execute(readQuery("create\\credentials.sql"));
      statement.execute(readQuery("create\\registration.sql"));
      statement.execute(readQuery("create\\doctor.sql"));
      statement.execute(readQuery("create\\patient.sql"));
      statement.execute(readQuery("create\\diagnosis.sql"));
      statement.execute(readQuery("create\\staff.sql"));
      statement.execute(readQuery("create\\task.sql"));
      statement.execute(readQuery("create\\result.sql"));
      //      INSERT STATEMENTS
      statement.execute(readQuery("insert\\user.sql"));
      statement.execute(readQuery("insert\\credentials.sql"));
      statement.execute(readQuery("insert\\registration.sql"));
      statement.execute(readQuery("insert\\patient.sql"));
      statement.execute(readQuery("insert\\doctor.sql"));
      statement.execute(readQuery("insert\\diagnosis.sql"));
      statement.execute(readQuery("insert\\staff.sql"));
      statement.execute(readQuery("insert\\task.sql"));
      statement.execute(readQuery("insert\\result.sql"));
    } catch (SQLException | IOException e) {
      e.printStackTrace();
    }
  }

}
