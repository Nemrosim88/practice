## README ##
**project version 1.1.5**

This is a combination of dif solutions to JavaSE task.
In project structure contains **nine** modules.
> concurrency

> epam

> generics

> hillel

> java8inAction

> junit

> nio

> patterns

> sql

Each solution class of **EPAM module** has its own Test class with same name, plus "Test" in the end of the name file.
> -- main.java.epam.courses.task.one.one.TaskOnePartOne

> -- test.java.epam.courses.task.one.one.TaskOnePartOneTest

Task classes (in epam module) were put in packages 
with same name as PDF files by this [link to Google Drive](https://drive.google.com/drive/folders/0B_hSnwQ-K47bRGZEcmoyU3FsZnc)

For example: 
> **task1.pdf** -> ua.epam.courses.task.one.*;

In **task1.pdf** three tasks. So, solution classes will be in:
> ua.epam.courses.task.one.one.*;

> ua.epam.courses.task.one.two.*;

> ua.epam.courses.task.one.three.*;

> ... and so on.


Thank you for attention and reading this text to the end.

- Artem Diashkin,
- nemrosim1988@gmail.com